#!/bin/bash

# Define the build directory
DIRECTORY="build"

# Check if the directory already exists
if ! [ -d $DIRECTORY ]; then

	# Create the directory if necessary
	mkdir $DIRECTORY
fi

# Go to the directory
cd $DIRECTORY

# Get the hostname
HOST=$(hostname)

# Define the build directory
BUILD_LOCATION_FILE="../src/BuildLocation.h"

# Check if hostname contains imr
if echo "$HOST" | grep -q imr; then

	# Modify the BuildLocation header file if necessary
	if grep -q "buildLocationCluster 1" $BUILD_LOCATION_FILE; then
		sed -i -e 's/buildLocationCluster 1/buildLocationCluster 0/g' $BUILD_LOCATION_FILE
	fi
	
	# Find the libraries and prepare for compilation
	cmake ../
else
	# Modify the BuildLocation header file if necessary
	if grep -q "buildLocationCluster 0" $BUILD_LOCATION_FILE; then
		sed -i -e 's/buildLocationCluster 0/buildLocationCluster 1/g' $BUILD_LOCATION_FILE
	fi
	
	# Use the MKL libraries
	source /cm/shared/apps/intel/bin-2017.01/mklvars.sh intel64
#	module add intel/compiler/64/15.0/2015.1.133
#	module add intel/mkl/64/11.2/2015.1.133
#	module add intel/impi/5.0.3.048
	
	# Find the libraries and prepare for compilation
	cmake -DQT_QMAKE_EXECUTABLE=/usr/bin/qmake-qt4 -DBOOST_ROOT=/usr/include/boost ../
	# -DCMAKE_CXX_COMPILER=mpiicpc
fi

# Compile MAFEM
make
