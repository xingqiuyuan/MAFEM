# Finds the CGAL library.

# This module defines
#  CGAL_INCLUDE_DIR, where to find CGAL/*.h.
#  CGAL_LIBRARIES, libraries to link against to use CGAL.
#  CGAL_FOUND, If false, do not try to use CGAL.
#
#  Also defined, but not for general use are
#  CGAL_LIBRARY, where to find the CGAL library.

FIND_PATH(CGAL_INCLUDE_DIR CGAL/Segment_tree_k.h
  /usr/local/include
  /usr/include
  /cm/shared/apps/cgal/4.5.1/include
  /usr/include/CGAL
)

SET(CGAL_NAMES ${CGAL_NAMES} CGAL)
FIND_LIBRARY(CGAL_LIBRARY
  NAMES ${CGAL_NAMES}
  PATHS /usr/lib /usr/local/lib
		/cm/shared/apps/cgal/4.5.1/lib/
)

IF(CGAL_INCLUDE_DIR)
  IF(CGAL_LIBRARY)
    SET(CGAL_FOUND "YES")
    SET(CGAL_LIBRARIES ${CGAL_LIBRARY})
  ENDIF(CGAL_LIBRARY)
ENDIF(CGAL_INCLUDE_DIR)
