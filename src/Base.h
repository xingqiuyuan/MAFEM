///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
/** 
 * \file Base.h 
 * \brief This header file includes the standard system headers used by the basic classes.
 * 
 * This header file is included by every .cpp file in the project to facilitate
 * the generation of precompiled headers. This feature is mainly used with the
 * Microsoft Visual C++ compiler.
 */

#ifndef __BASE_H
#define __BASE_H

/// Intel compiler compatibility work-around.
#ifdef __ICC
	// Replacement of unknown atomic function.
	#define __sync_fetch_and_add(ptr,addend) _InterlockedExchangeAdd(const_cast<void*>(reinterpret_cast<volatile void*>(ptr)), addend);
#endif

namespace Base {

/******************************************************************************
* Define the number type used for numerical computations.
*
* The Kernel uses FloatType as the default value type for its computations.
******************************************************************************/
// Use 64-bit double as default floating point type.
typedef double FloatType;
#define FLOATTYPE_DOUBLE	// This tells the program that we're using 64-bit floating point.
/// A small epsilon value for the FloatType.
#define FLOATTYPE_EPSILON	1e-9

/// Computes the square of a number.
template<typename T> inline T square(const T& f) { return f*f; }

/// The maximum value for floating point variables.
#define FLOATTYPE_MAX	(numeric_limits<FloatType>::max())
#define FLOATTYPE_PI	((FloatType)3.14159265358979323846)

#define MAFEM_INVALID_INDEX (std::numeric_limits<size_t>::max())

#include "BuildLocation.h"
}; // End of namespace Base

/// Include the standard system header like STL, BOOST library etc.
/******************************************************************************
* Standard Template Library (STL)
******************************************************************************/

// Microsoft VC 8 specifc
#ifndef _CRT_SECURE_NO_DEPRECATE
	#define _CRT_SECURE_NO_DEPRECATE 1	// Tell the compiler to stop complaining about using localtime etc...
#endif
#ifndef _SCL_SECURE_NO_DEPRECATE
	#define _SCL_SECURE_NO_DEPRECATE 1	// Stop complaining about std::copy etc...
#endif
#ifndef _CRT_NONSTDC_NO_DEPRECATE
	#define _CRT_NONSTDC_NO_DEPRECATE 1	// To stop complaining about old POSIX names.
#endif

#include <string>
#include <sstream>
#include <fstream>
#include <map>
#include <deque>
#include <stack>
#include <exception>
#include <iostream>
#include <ios>
#include <limits>
#include <queue>
#include <locale>
#include <vector>
#include <complex>
#include <algorithm>
#include <numeric>
#include <set>
#include <cstdlib>
#include <valarray>
#include <ctime>

// use the namespace of the STL
using namespace std;

/******************************************************************************
* C Run-Time Library
******************************************************************************/
#include <math.h>

/******************************************************************************
* Boost Library
******************************************************************************/
#include <boost/static_assert.hpp>
#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/shared_array.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/scoped_array.hpp>
#include <boost/intrusive_ptr.hpp>
#include <boost/get_pointer.hpp>
#include <boost/cstdint.hpp>
#include <boost/array.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/type_traits.hpp>
#include <boost/pool/pool.hpp>
#include <boost/pool/object_pool.hpp>
#include <boost/format.hpp>
#include <boost/multi_array.hpp>
#include <boost/progress.hpp>
#if 0	// This has to be skipped when compiling on the Cluster
	#include <boost/graph/adjacency_list.hpp>
	#include <boost/graph/cuthill_mckee_ordering.hpp>
	#include <boost/graph/properties.hpp>
	#include <boost/graph/bandwidth.hpp>
#endif

//#include <boost/archive/text_oarchive.hpp>
//#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/archive_exception.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/export.hpp>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/key_extractors.hpp>

#if 1	// This will speedup uBLAS routines even in debug mode.
	#define BOOST_UBLAS_INLINE inline
	#define BOOST_UBLAS_USE_FAST_SAME
	#define BOOST_UBLAS_CHECK_ENABLE 0
	#define BOOST_UBLAS_TYPE_CHECK 0
#endif

#include <boost/numeric/ublas/matrix_sparse.hpp>
#include <boost/numeric/ublas/symmetric.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/vector_expression.hpp>
#include <boost/numeric/ublas/matrix_expression.hpp>
#include <boost/numeric/ublas/triangular.hpp>
#include <boost/numeric/ublas/banded.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>

using namespace boost;
namespace ublas = boost::numeric::ublas;

/******************************************************************************
* QT Library
******************************************************************************/
#include <QtGlobal>
#include <QChar>
#include <QString>
#include <QVector>
#include <QTextStream>
#include <QIODevice>
#include <QFile>
#include <QDebug>
#include <QMetaType>
#include <QStringList>
#include <QPoint>
#include <QColor>
#include <QProcess>
#include <QDateTime>

/******************************************************************************
* GNU Scientific Library.
******************************************************************************/
#define HAVE_INLINE 1		// This activates inlining of GSL functions.
#include <gsl/gsl_math.h>
#include <gsl/gsl_multimin.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_deriv.h>

/******************************************************************************
* Boos serialization of Qt classes
******************************************************************************/
namespace boost {
namespace serialization {
	
template<class Archive, class U>
inline void save(Archive & ar, const QVector<U> &t, const unsigned int) { 
    boost::serialization::stl::save_collection<Archive, QVector<U> >(ar, t);
}

template<class Archive, class U>
inline void load(Archive & ar, QVector<U> &t, const unsigned int) {
    boost::serialization::stl::load_collection_impl<Archive, QVector<U>, 
    boost::serialization::stl::archive_input_seq<Archive, QVector<U> >,
        boost::serialization::stl::no_reserve_imp<QVector<U> > >(ar, t);
}

// split non-intrusive serialization function member into separate
// non intrusive save/load member functions
template<class Archive, class U>
inline void serialize(Archive & ar, QVector<U> & t, const unsigned int file_version) {
    boost::serialization::split_free(ar, t, file_version);
}

template<class Archive>
inline void save(Archive & ar, const QString& t, const unsigned int) { 
	std::string s = t.toStdString();
	ar << s;
}

template<class Archive>
inline void load(Archive & ar, QString& t, const unsigned int) {
	std::string s;
	ar >> s;
	t = QString::fromStdString(s);
}

// split non-intrusive serialization function member into separate
// non intrusive save/load member functions
template<class Archive>
inline void serialize(Archive & ar, QString& t, const unsigned int file_version) {
    boost::serialization::split_free(ar, t, file_version);
}

/*template <class Archive, class T, std::size_t N>
void serialize(Archive& ar, boost::array<T,N>& a, const unsigned int version)
{
	ar & make_nvp("elems",a.elems);
}*/

} // namespace serialization
} // namespace boost


/******************************************************************************
* Include the basic stuff.
******************************************************************************/
#include "utilities/Debugging.h"
#include "utilities/Logger.h"
#include "utilities/Flags.h"
// Make all classes of the Base namespace available by default.
namespace Base {};
using namespace Base;

#endif // __BASE_H
