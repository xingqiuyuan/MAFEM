///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "DisplacementBoundaryCondition.h"
#include <simulation/Simulation.h>
#include <mesh/Mesh.h>
#include <force/CalculationResult.h>

namespace MAFEM {
	
/******************************************************************************
* Constructor.
******************************************************************************/
DisplacementBoundaryCondition::DisplacementBoundaryCondition(Group* group) : SimulationResource(group->simulation()), _group(group) 
{
	// Register this BC with the global simulation object.
	//this->simulation()->_displacementBC.push_back(shared_ptr<DisplacementBoundaryCondition>(this));	
}

/******************************************************************************
* Sets the corresponding entries for all selected atoms in the force vector to zero.
******************************************************************************/
void FixAtoms::resetForces(Mesh& mesh, CalculationResult& output)
{
	Q_FOREACH(const RepAtom* repatom, mesh.repatoms())
		if(repatom->belongsToGroup(group()))
		{
			for( int dof = 0; dof < NDOF; ++dof)
				if(_fixed[dof])
				{
					output.reforces()[repatom->index()][dof] -= output.forces()[repatom->index()][dof];
					output.forces()[repatom->index()][dof] = 0;
				}
			_groupForce += output.reforces()[repatom->index()];
			_groupTorque += CrossProduct(repatom->deformedPos() - ORIGIN, output.reforces()[repatom->index()]);
			_displacement = (repatom->deformedPos() - repatom->pos());
			_position = repatom->deformedPos();
			_relaxstrain = (repatom->deformedPos() - repatom->pos()) / repatom->pos().Z ;
		}
}

void DirichletBC::resetForces(Mesh& mesh, CalculationResult& output)
{
	if(activate)
		Q_FOREACH(const RepAtom* repatom, mesh.repatoms())
			if(repatom->belongsToGroup(group()))
			{
				Vector3 disp = (_rotation * repatom->deformedPos() + _displacement) - repatom->deformedPos();
				for(int dof = 0; dof < NDOF; ++dof)
					if(abs(disp[dof]) > FLOATTYPE_EPSILON)
					{
						output.reforces()[repatom->index()][dof] -= output.forces()[repatom->index()][dof];
						output.forces()[repatom->index()][dof] = 0;
					}
				_groupForce += output.reforces()[repatom->index()];
				_groupTorque += CrossProduct(repatom->deformedPos() - ORIGIN, output.reforces()[repatom->index()]);
			}
}

void DirichletBC::updatePositions(Mesh& mesh)
{
	if(activate)
		if(!refinement)
			Q_FOREACH(RepAtom* repatom, mesh.repatoms())
				if(repatom->belongsToGroup(group()))
					repatom->setDeformedPos(_rotation * repatom->deformedPos() + _displacement);
}


}; // End of namespace MAFEM
