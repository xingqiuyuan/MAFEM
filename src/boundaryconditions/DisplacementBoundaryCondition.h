///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_DISPLACEMENT_BOUNDARY_CONDITION_H
#define __MAFEM_DISPLACEMENT_BOUNDARY_CONDITION_H

#include <MAFEM.h>
#include <simulation/SimulationResource.h>
#include "Group.h"
#include <Base.h>

namespace MAFEM {

class Mesh;					// defined in Mesh.h
class CalculationResult;	// defined in CalculationResult.h

/**
 * \brief Base class for all boundary conditions that
 *        directly control the position of repatoms.
 * 
 * \author Alexander Stukowski
 */
class DisplacementBoundaryCondition : public SimulationResource
{
protected:

	/// \brief Constructor.
	/// \param group The group of atoms that should be affected by this boundary condition.
	DisplacementBoundaryCondition(Group* group);
	
public:

	/// \brief Returns the group of atoms on which this boundary condition acts.
	Group* group() const { return _group; }
	
	/// \brief Initializes the boundary condition.
	/// \param mesh The mesh.
	///
	/// This method is called by the system before every force calculation pass.
	virtual void prepare(Mesh& mesh) {}
	
	/// \brief Lets the BC apply its displacement constraints to the selected atoms.
	/// \param mesh The mesh.
	///
	/// This method is invoked by the system at the beginning of each time step.
	virtual void updatePositions(Mesh& mesh) = 0;

	/// \brief Sets the corresponding entries for all selected atoms in the force vector to zero.
	/// \param mesh The mesh.
	/// \param output The force vector to be altered.
	///
	/// This method is invoked by the system after each force calculation.
	/// The displacement boundary condition should set the corresponding entries in the
	/// force vector to zero to fix the selected atoms to their constrained positions.
	virtual void resetForces(Mesh& mesh, CalculationResult& output) = 0;
	
	virtual bool isFixed(int dof) = 0;
	
private:

	/// The group of atoms to which this BC is applied.
	Group* _group;
};

typedef QVector< shared_ptr<DisplacementBoundaryCondition> > DisplacementBoundaryConditionList;

/**
 * \brief This boundary conditions fixes atoms to their current position.
 * 
 * \author Alexander Stukowski
 */
class FixAtoms : public DisplacementBoundaryCondition
{
public:

	/// \brief Initializes the boundary condition.
	/// \param mesh The mesh.
	virtual void prepare(Mesh& mesh)
	{
		// Reset the internal force measure.
		_groupForce = NULL_VECTOR;
		
		// Reset the internal torque measure.
		_groupTorque = NULL_VECTOR;
		
		_fixed[0] = _fixX;
		_fixed[1] = _fixY;
		_fixed[2] = _fixZ;
	}

	/// \brief Constructor.
	/// \param group The group of atoms that should be affected by this boundary condition.
	/// \param fixX Specifies whether the atoms' positions are fixed in the X direction.
	/// \param fixY Specifies whether the atoms' positions are fixed in the Y direction.
	/// \param fixZ Specifies whether the atoms' positions are fixed in the Z direction.
	FixAtoms(Group* group, bool fixX = true, bool fixY = true, bool fixZ = true) : DisplacementBoundaryCondition(group),
		_fixX(fixX), _fixY(fixY), _fixZ(fixZ) {}
	/// \brief Lets the BC apply its displacement constraints to the selected atoms.
	/// \param mesh The mesh.
	///
	/// This method is invoked by the system at the beginning of each time step.
	virtual void updatePositions(Mesh& mesh) {
		// Nothing to do. Let the atoms just stay at their current positions. 
	}

	/// \brief Sets the corresponding entries for all selected atoms in the force vector to zero.
	/// \param mesh The mesh.
	/// \param output The force vector to be altered.
	///
	/// This method is invoked by the system after each force calculation.
	/// The displacement boundary condition sets the corresponding entries in the
	/// force vector to zero to fix the selected atoms to their positions.
	virtual void resetForces(Mesh& mesh, CalculationResult& output);
	
	/// \brief Returns the boundary forces on a group of atoms.
	const Vector3& groupForce() const { return _groupForce; };
	
	/// \brief Returns the boundary torque on a group of atoms.
	const Vector3& groupTorque() const { return _groupTorque; };
	
	/// \brief Returns the displacement on a group of atoms.
	const Vector3& displacement() const { return _displacement; }
	
	/// \brief Returns the position on a group of atoms.
	const Point3& position() const { return _position; }
	
	/// \brief Returns the strain with relaxation included on a group of atoms.
	const Vector3& relaxstrain() const { return _relaxstrain; }
	
	bool isFixed(int dof) {return _fixed[dof]; };

private:

	/// Specifies whether the atoms' positions are fixed in the X direction.
	bool _fixX; 

	/// Specifies whether the atoms' positions are fixed in the Y direction.
	bool _fixY; 

	/// Specifies whether the atoms' positions are fixed in the Z direction.
	bool _fixZ;
	
	bool _fixed[3];
	
	/// The boundary forces on a group of atoms.
	Vector3 _groupForce;
	
	/// The boundary torque on a group of atoms.
	Vector3 _groupTorque;
	
	/// The displacement on group of atoms
	Vector3 _displacement;
	
	/// The displacement on group of atoms
	Point3 _position;
	
	/// The strain with relaxation on group of atoms
	Vector3 _relaxstrain;
};

class DirichletBC : public DisplacementBoundaryCondition
{
public:

	/// \brief Constructor.
	DirichletBC(Group* group, const Vector3& displacement, AffineTransformation& rotation) : DisplacementBoundaryCondition(group),
		_displacement(displacement), _rotation(rotation), activate(false) {}

	/// \brief Initializes the boundary condition.
	/// \param mesh The mesh.
	virtual void prepare(Mesh& mesh)
	{
		// Reset the internal force measure.
		_groupForce = NULL_VECTOR;
		
		// Reset the internal torque measure.
		_groupTorque = NULL_VECTOR;
		
		Vector3 vec(1);
		Vector3 disp = _rotation * vec + _displacement - vec;
		_fixed[0] = (bool)disp.X;
		_fixed[1] = (bool)disp.Y;
		_fixed[2] = (bool)disp.Z;
	}

	/// \brief Lets the BC apply its displacement constraints to the selected atoms.
	/// \param mesh The mesh.
	///
	/// This method is invoked by the system at the beginning of each time step.
	virtual void updatePositions(Mesh& mesh);

	/// \brief Sets the corresponding entries for all selected atoms in the force vector to zero.
	/// \param mesh The mesh.
	/// \param output The force vector to be altered.
	///
	/// This method is invoked by the system after each force calculation.
	/// The displacement boundary condition sets the corresponding entries in the
	/// force vector to zero to fix the selected atoms to their positions.
	virtual void resetForces(Mesh& mesh, CalculationResult& output);

	void setDisplacement(const Vector3& disp) {
		_displacement = disp;
		setRefinementCriterion();
		activate = true;
	}

	void setRotation(const AffineTransformation& rot) {
		_rotation = rot;
		setRefinementCriterion();
		activate = true;
	}

	void setRefinementCriterion(const bool& ref = false){
		refinement = ref;
		activate = true;
	}
	const Vector3& displacement() const { return _displacement; }
	
	const AffineTransformation& rotation() const { return _rotation; }

	const bool& refinementCriterion() const { return refinement; }
	
	/// \brief Returns the boundary forces on a group of atoms.
	const Vector3& groupForce() const { return _groupForce; };
	
	/// \brief Returns the boundary torque on a group of atoms.
	const Vector3& groupTorque() const { return _groupTorque; };
	
	bool isFixed(int dof) {return _fixed[dof]; };

private:


	/// The displacement vector.
	Vector3 _displacement;

	/// The rotation matrix.
	AffineTransformation& _rotation;

	/// activate boundary condition
	bool activate;

	/// check refinement crieterion
	bool refinement;
	
	/// The fixed dofs
	bool _fixed[3];
	
	/// The boundary forces on a group of atoms.
	Vector3 _groupForce;
	
	/// The boundary torque on a group of atoms.
	Vector3 _groupTorque;
};


}; // End of namespace MAFEM

#endif // __MAFEM_DISPLACEMENT_BOUNDARY_CONDITION_H
