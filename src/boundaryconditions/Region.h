///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_REGION_H
#define __MAFEM_REGION_H

#include <MAFEM.h>

namespace MAFEM {

/**
 * \brief Base class for all geometric regions.
 * 
 * \author Alexander Stukowski
 */
class Region
{
protected:

	/// \brief Constructs a new region object.
	Region() {}

public:

	/// \brief Performs a point in region test.
	/// \param point The point to be tested.
	/// \return Return \c true if the point is in the region or on its border.
	virtual bool contains(const Point3& point) const = 0;
	
	/// \brief Returns a text describing the region for logging purposes.
	virtual QString toString() const = 0;
};

/**
 * \brief A box shaped region.
 * 
 * \author Alexander Stukowski
 */
class BoxRegion : public Region
{
public:

	/// \brief Constructs a new box region object.
	BoxRegion(const Box3& box) : Region(), _box(box) {}

	/// \brief Constructs a new box region object.
	BoxRegion(const Point3& c1, const Point3& c2) : Region(), _box(c1,c2) {}

	/// \brief Constructs a new box region object.
	BoxRegion(FloatType minx, FloatType maxx, FloatType miny, FloatType maxy, FloatType minz, FloatType maxz) : Region(), _box(Point3(minx,miny,minz),Point3(maxx,maxy,maxz)) {}

	/// \brief Performs a point in region test.
	/// \param point The point to be tested.
	/// \return Return \c true if the point is in the region or on its border.
	virtual bool contains(const Point3& point) const { return _box.contains(point); }

	/// \brief Returns a text describing the region for logging purposes.
	virtual QString toString() const {
		return QString("Box %1").arg(_box.toString());
	}

private:

	Box3 _box;
};


}; // End of namespace MAFEM

#endif // __MAFEM_REGION_H
