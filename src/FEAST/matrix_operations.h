/*
 * matrix.h
 *
 *  Created on: Sep 14, 2015
 *      Author: Fabian Guecker
 *
 *  Matrix operations for matrices created with constant size on the stack
 *  e.g. double StackMatrix [3][3];
 *
 */

#ifndef MATRIX_OPERATIONS_H_
#define MATRIX_OPERATIONS_H_

#include <iostream>
using namespace std;

void PrintMatrix(double *A, unsigned int M, unsigned int N)
{
	cout << "\n";
	for(unsigned int i = 0 ; i < min(M, (unsigned int)50) ; ++i)
	{
		for(unsigned int  j = 0 ; j < min(N, (unsigned int)50) ; ++j)
		{
			cout << A[i*N + j] <<" ; ";
		}
		cout << "\n";
	}
	cout << "\n";
}

void PrintMatrix(unsigned int *A, unsigned int M, unsigned int N)
{
	cout << "\n";
	for(unsigned int i = 0 ; i < min(M, (unsigned int)50) ; ++i)
	{
		for(unsigned int  j = 0 ; j < min(N, (unsigned int)50) ; ++j)
		{
			cout << A[i*N + j] <<" ; ";
		}
		cout << "\n";
	}
	cout << "\n";
}


void matrix_copy(double *dest, double *A, unsigned int AM, unsigned int AN)
{
	unsigned int elements = AM*AN;
	for (unsigned int i = 0; i < elements; ++i)
		dest[i] = A[i];

}

void matrix_copy_AB(double *dest, double *A, unsigned int destM, unsigned int destN, unsigned int AM, unsigned int AN)
{
	for (unsigned int i = 0; (i < destM) && (i < AM); ++i)
		for (unsigned int j = 0; (j < destN) && (j < AN); ++j)
				dest[i+destN*j] = A[i+AN*j];

}

void matrix_mul_skalar(double *A,double skalar,unsigned int AM, unsigned int AN)
{
	unsigned int elements = AM*AN;
	for (unsigned int i = 0; i < elements; ++i)
		A[i] *= skalar;
}

void matrix_div_skalar(double *A,double skalar,unsigned int AM, unsigned int AN)
{
	unsigned int elements = AM*AN;
	for (unsigned int i = 0; i < elements; ++i)
		A[i] /= skalar;
}

void matrix_add(double *dest, double *A, unsigned int AM, unsigned int AN)
{
	unsigned int elements = AM*AN;
	for (unsigned int i = 0; i < elements; ++i)
		dest[i] += A[i];
}

void matrix_sub(double *dest, double *A, unsigned int AM, unsigned int AN)
{
	unsigned int elements = AM*AN;
	for (unsigned int i = 0; i < elements; ++i)
		dest[i] -= A[i];
}

void matrix_mul_T_T(double *dest,double *A, double *B,unsigned int AM, unsigned int AN, unsigned int BM, unsigned int BN)
{
	unsigned int l,i,j,k;

	if(AM == BN)
	{
		l = AM;
	}
	else
	{
		cerr << "Dimension Error! Matrix multiply N x N \n";
		return;
	}

	for (i = 0; i < AN; ++i)
	{
		unsigned int iBM = i*BM;
		for (j = 0; j < BM; ++j)
			{
				double Value = 0;
				unsigned int jl = j * l;
				for (k = 0; k < l; ++k)
				{
					Value += A[k*AN + i] * B[jl + k];
				}
				dest[iBM + j] = Value;
			}

	 }
}

void matrix_mul_N_N(double *dest,double *A, double *B,unsigned int AM, unsigned int AN, unsigned int BM, unsigned int BN)
{
	unsigned int l,i,j,k;

	if(AN == BM)
	{
		l = AN;
	}
	else
	{
		cerr << "Dimension Error! Matrix multiply N x N \n";
		return;
	}

	for (i = 0; i < AM; ++i)
	{
		for (j = 0; j < BN; ++j)
			{
				double Value = 0;
				unsigned int il = i*l;
				for (k = 0; k < l; ++k)
				{
					Value += A[il + k] * B[k*BN + j];
				}
				dest[i*BN + j] = Value;
			}

	 }
}

void matrix_transpose(double *dest,double *A, unsigned int AM, unsigned int AN)
{
	for(unsigned int i=0; i < AM; ++i)
		for(unsigned int j=0; j < AN; ++j)

			dest[j*AM + i] = A[i*AN + j];

}

void matrix_solve_2x2xn(double *x,double *A,double *b, unsigned int N)
{
	  // Calculate Jacobideterminant
	  double detJ =  A[0] * A[3] - A[2] * A[1];

	  // Check if Jacobideterminant is near zero, if yes return(exit the function) to prevent a division by zero
	  if(detJ < 2.2e-15)
	  {
		  cerr << "Jacobideterminant equal or less than zero! \n";
		  return;
	  }

	  // Calculate the inverse of JT
	  double A_inverse[2][2] = {{A[3] / detJ, -A[1] / detJ},
			  	  	  	  	  	{-A[2] / detJ,  A[0] / detJ}};


	  // Solve the equation
	  matrix_mul_N_N(x, &A_inverse[0][0], b,2,2,2,N);
}
#endif
