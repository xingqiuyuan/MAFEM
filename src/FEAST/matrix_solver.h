/*
 * matrix_solver.h
 *
 *  Created on: Sep 23, 2015
 *      Author: fabian
 */

#ifndef MATRIX_SOLVER_H_
#define MATRIX_SOLVER_H_

#include <cmath>


double matrix_solve_gauss(double** A,unsigned int NumberOfUnknowns,unsigned int Rhs )
{
	double det = 1;
	unsigned int max_equation = NumberOfUnknowns;
	unsigned int max_coeff = NumberOfUnknowns + Rhs;
    for (unsigned int i = 0; i < max_equation; ++i)
    {
        // Search for maximum in this column
        double maxEl = fabs( A[i][i] );
        int maxRow = i;
        for (unsigned int k=i+1; k<max_equation; ++k)
        {
            if (fabs( A[k][i] ) > maxEl)
            {
                maxEl = fabs( A[k][i] );
                maxRow = k;
            }
        }

        // Swap maximum row with current row (column by column)
        if(i != maxRow)
        {
        	for (unsigned int k=i; k<max_coeff;++k)
        	{
            	double tmp = A[maxRow][k];
            	A[maxRow][k] = A[i][k];
            	A[i][k] = tmp;
        	}
        	det *= -1;
        }

        // Make sure we found an equation with
        // a non-zero ith coefficient.
        double coeff_i_i = A[i][i];

        if (coeff_i_i != 0)
        {
			for (unsigned int j = i; j < max_coeff; ++j)
			{
				A[i][ j] /= coeff_i_i;
			}

			det *= coeff_i_i;

			// Use this equation value to zero out
			// the other equations' ith coefficients.
			for (unsigned int j = 0; j< max_equation; ++j)
			{
				// Skip the ith equation.
				if (j != i)
				{
					// Zero the jth equation's ith coefficient.
					double coef_j_i = A[j][ i];
					for (unsigned int d = 0; d < max_coeff; ++d)
					{
						A[j][ d] -= A[i][ d] * coef_j_i;
					}
				}
			}
        }
    }
    return det;
}

double matrix_solve(double *x,double *A, double *b, unsigned int size, unsigned int rhs)
{
	double A_concat[size][size+rhs];
	double *pA_concat[size];

	for(unsigned int i=0; i<size; i++)
	{
		for(unsigned int j=0; j<size; j++)
			A_concat[i][j] = A[i*size + j];

		for(unsigned int j=0; j<rhs; j++)
			A_concat[i][size+j] = b[i*rhs + j];

		pA_concat[i] = &A_concat[i][0];
	}

	double det = matrix_solve_gauss(pA_concat, size, rhs);

	for(unsigned int i=0; i<size; i++)
		for(unsigned int j=0; j<rhs; j++)
			x[i*rhs + j] = A_concat[i][size+j];

	return det;
}

double determ(double *rhs, unsigned int size, unsigned int n)
{
	double det=0, a[n][n], temp[n][n];
	double **ptemp;
	int p, h, k, i, j;

	matrix_copy_AB(&a[0][0], rhs, n,n, size, size);

	if(n==1)
	{
		return a[0][0];
	}

	else if(n==2)
	{
		det=(a[0][0]*a[1][1]-a[0][1]*a[1][0]);
		return det;
	}

	else
	{
		for(p=0;p<n;p++)
		{
			h = 0;
			k = 0;
			for(i=1;i<n;i++)
			{
				for( j=0;j<n;j++)
				{
					if(j==p)
						continue;

					temp[h][k] = a[i][j];
					k++;

					if(k==n-1)
					{
						h++;
						k = 0;
					}
				}
			}
			det=det+a[0][p]*pow(-1,p)*determ(&temp[0][0],n,n-1);
		}
		return det;
	}
}

#endif /* MATRIX_SOLVER_H_ */
