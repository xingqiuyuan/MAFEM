/*
 * sort_network.h
 *
 *  Created on: Nov 15, 2015
 *      Author: fabian
 */

#ifndef SORT_NETWORK_H_
#define SORT_NETWORK_H_

#include "matrix_base.h"


#define min(x, y) (x<y?x:y)
#define max(x, y) (x<y?y:x)
#define SWAP(x,y) { const row_entry a = min(d[x], d[y]); const row_entry b = max(d[x], d[y]); d[x] = a; d[y] = b; }

static inline void sort2(row_entry *d)
{
	SWAP(0, 1);
}

static inline void sort3(row_entry *d)
{
	SWAP(1, 2);
	SWAP(0, 2);
	SWAP(0, 1);
}

static inline void sort4(row_entry *d)
{
	SWAP(0, 1);
	SWAP(2, 3);
	SWAP(0, 2);
	SWAP(1, 3);
	SWAP(1, 2);
}

static inline void sort5(row_entry *d)
{
	SWAP(0, 1);
	SWAP(3, 4);
	SWAP(2, 4);
	SWAP(2, 3);
	SWAP(0, 3);
	SWAP(0, 2);
	SWAP(1, 4);
	SWAP(1, 3);
	SWAP(1, 2);
}

static inline void sort6(row_entry *d)
{
	SWAP(1, 2);
	SWAP(0, 2);
	SWAP(0, 1);
	SWAP(4, 5);
	SWAP(3, 5);
	SWAP(3, 4);
	SWAP(0, 3);
	SWAP(1, 4);
	SWAP(2, 5);
	SWAP(2, 4);
	SWAP(1, 3);
	SWAP(2, 3);
}

static inline void sort7(row_entry *d)
{
	SWAP(1, 2);
	SWAP(0, 2);
	SWAP(0, 1);
	SWAP(3, 4);
	SWAP(5, 6);
	SWAP(3, 5);
	SWAP(4, 6);
	SWAP(4, 5);
	SWAP(0, 4);
	SWAP(0, 3);
	SWAP(1, 5);
	SWAP(2, 6);
	SWAP(2, 5);
	SWAP(1, 3);
	SWAP(2, 4);
	SWAP(2, 3);
}

static inline void sort8(row_entry *d)
{
	SWAP(0, 1);
	SWAP(2, 3);
	SWAP(0, 2);
	SWAP(1, 3);
	SWAP(1, 2);
	SWAP(4, 5);
	SWAP(6, 7);
	SWAP(4, 6);
	SWAP(5, 7);
	SWAP(5, 6);
	SWAP(0, 4);
	SWAP(1, 5);
	SWAP(1, 4);
	SWAP(2, 6);
	SWAP(3, 7);
	SWAP(3, 6);
	SWAP(2, 4);
	SWAP(3, 5);
	SWAP(3, 4);
}

static inline void sort9(row_entry *d)
{
	SWAP(0, 1);
	SWAP(3, 4);
	SWAP(6, 7);
	SWAP(1, 2);
	SWAP(4, 5);
	SWAP(7, 8);
	SWAP(0, 1);
	SWAP(3, 4);
	SWAP(6, 7);
	SWAP(0, 3);
	SWAP(3, 6);
	SWAP(0, 3);
	SWAP(1, 4);
	SWAP(4, 7);
	SWAP(1, 4);
	SWAP(2, 5);
	SWAP(5, 8);
	SWAP(2, 5);
	SWAP(1, 3);
	SWAP(5, 7);
	SWAP(2, 6);
	SWAP(4, 6);
	SWAP(2, 4);
	SWAP(2, 3);
	SWAP(5, 6);
}

static inline void sort10(row_entry *d)
{
	SWAP(4, 9);
	SWAP(3, 8);
	SWAP(2, 7);
	SWAP(1, 6);
	SWAP(0, 5);
	SWAP(1, 4);
	SWAP(6, 9);
	SWAP(0, 3);
	SWAP(5, 8);
	SWAP(0, 2);
	SWAP(3, 6);
	SWAP(7, 9);
	SWAP(0, 1);
	SWAP(2, 4);
	SWAP(5, 7);
	SWAP(8, 9);
	SWAP(1, 2);
	SWAP(4, 6);
	SWAP(7, 8);
	SWAP(3, 5);
	SWAP(2, 5);
	SWAP(6, 8);
	SWAP(1, 3);
	SWAP(4, 7);
	SWAP(2, 3);
	SWAP(6, 7);
	SWAP(3, 4);
	SWAP(5, 6);
	SWAP(4, 5);

}

static inline void sort_network_vector(row_entry *rhs, unsigned int size)
{
	switch(size)
	{
		case  2:  sort2(rhs); break;
		case  3:  sort3(rhs); break;
		case  4:  sort4(rhs); break;
		case  5:  sort5(rhs); break;
		case  6:  sort6(rhs); break;
		case  7:  sort7(rhs); break;
		case  8:  sort8(rhs); break;
		case  9:  sort9(rhs); break;
		case 10: sort10(rhs); break;
	}
}

#undef SWAP
#undef min
#undef max

#endif /* SORT_NETWORK_H_ */
