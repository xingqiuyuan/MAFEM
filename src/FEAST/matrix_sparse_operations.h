/*
 * matrix_sparse_operations.h
 *
 *  Created on: Oct 7, 2015
 *      Author: fabian
 */

#include "matrix_base.h"
#include "matrix_sparse.h"

#include <iostream>

#ifndef MATRIX_SPARSE_OPERATIONS_H_
#define MATRIX_SPARSE_OPERATIONS_H_


// matrix_mul_N_N(r_F.element[0], A.element[0], u.element[0], A.size(ROWS), A.size(COLUMNS), u.size(ROWS),  u.size(COLUMNS));

void sparse_sym_matrix_vector_prod(matrix &dest, sparse_CRS_matrix &A, matrix &B)
{
	int i=0;
	int j=0;

	for (i = 0; i < A.N; ++i)
	{
		for (j = A.IA[i]; j < A.IA[i+1]; ++j)
		{
			int row = i;
			int column = A.JA[j];
			double value = A.A[j];

			dest.element[i][0] += value * B.element[column][0];

			if(column > row)
			dest.element[column][0] += value * B.element[i][0];

		}
	 }
}


void sparse_sym_matrix_vector_prod(matrix &dest, sparse_matrix &A, matrix &B)
{
	int i=0;
	int j=0;

	for (i = 0; i < A.N; ++i)
	{
		for (j = A.IA[i]; j < A.IA[i+1]; ++j)
		{
			int row = i;
			int column = A.JA[j];
			double value = A.A[j];

			dest.element[i][0] += value * B.element[column][0];

			if(column > row)
			dest.element[column][0] += value * B.element[i][0];

		}
	 }
}

void matrix_sparse_sym_matrix_prod(matrix &dest,matrix &A, sparse_matrix &B)
{
	unsigned int i,j,k;

	if(A.N != (unsigned int)B.N)
	{
		cerr << "Dimension Error! Matrix multiply N x N \n";
		return;
	}


	for (i = 0; i < A.M; ++i)
		for (j = 0; j < A.N; ++j)
			for (k = B.IA[j]; k < (unsigned int)B.IA[j+1]; ++k)
			{
				unsigned int row = j;
				unsigned int column = B.JA[k];
				double value = B.A[k];

				dest.element[i][j] += A.element[i][column] * value;

				if(column > row)
					dest.element[i][column] += A.element[i][row] * value;
			}

}



void matrix_sparse_sym_matrix_prod(matrix &dest,matrix &A, sparse_CRS_matrix &B)
{
	unsigned int i,j,k;

	if(A.N != (unsigned int)B.N)
	{
		cerr << "Dimension Error! Matrix multiply N x N \n";
		return;
	}


	for (i = 0; i < A.M; ++i)
		for (j = 0; j < A.N; ++j)
			for (k = B.IA[j]; k < (unsigned int)B.IA[j+1]; ++k)
			{
				unsigned int row = j;
				unsigned int column = B.JA[k];
				double value = B.A[k];

				dest.element[i][j] += A.element[i][column] * value;

				if(column > row)
					dest.element[i][column] += A.element[i][row] * value;
			}

}


#endif /* MATRIX_SPARSE_OPERATIONS_H_ */
