/*
 * matrix_base_sparse.h
 *
 *  Created on: Oct 5, 2015
 *      Author: fabian
 */

#ifndef MATRIX_BASE_SPARSE_H_
#define MATRIX_BASE_SPARSE_H_

#include "matrix_base.h"

#include <vector>
#include <algorithm>
#include <limits>
#include <cmath>
#include <iostream>
#include <stdio.h>

#include "sort_network.h"

using namespace std;
#define ZERO_TINY_ELEMENTS_THRESHOLD 0.0

enum matrix_format  {NONSYMMETRIC=false, SYMMETRIC=true};


class sparse_CRS_matrix
{
public:

	int     *IA; // index of the first Element in that row
	int     *JA; // column index of the A[i] element from left side
	double  *A;  // Values of A

	int     N; // Number of rows
    int     NNZ; // Number non-zeros = ia[n];

    double density;


	std::vector< std::vector<row_entry> > elements;

public:

	sparse_CRS_matrix()
	{
    		this->IA = 0;
    		this->JA = 0;
    		this->A = 0;
    		this->N = 0;
    		this->NNZ = 0;
    		this->density = 0;
	}

	~sparse_CRS_matrix()
	{
		deallocate_elements();
		deallocate_CRS();
	}

    void allocate_elements(unsigned int col_count,unsigned int std_row_capacity)
	{
     	elements.reserve(col_count);

     	std::vector<row_entry> row_init_vector;
     	row_init_vector.reserve(std_row_capacity);

     	for(unsigned int i=0; i < elements.capacity(); ++i)
     	{
     		elements.push_back(row_init_vector);
     	}
	}

	void deallocate_elements(void)
	{
	   	for(unsigned int i=0; i < elements.size(); ++i)
	    {
	   		elements[i].clear();
	    }
		elements.clear();
	}

	void copy_from_sparse_matrix(sparse_CRS_matrix &A)
	{
  		this->allocate_elements(A.elements.size(), 0);

		for(unsigned int i=0; i< A.elements.size(); ++i)
			this->elements[i] = A.elements[i];
	}

	void add_stima_indices(double *M, unsigned int *I, unsigned int size, bool is_square_matrix)
	{
		// Add indices for the sparse matrix
		for(unsigned int i = 0; i<size; ++i)
			for(unsigned int j = 0; j<size; ++j)
				if((is_square_matrix) && (I[i] <= I[j]))
				{
					unsigned int row = I[i] - 1;
					unsigned int col = I[j] - 1;
					double val = M[i*size+j];

					bool index_exist = false;
					for(unsigned int k=0; k < elements[row].size(); ++k)
					{
						if(elements[row][k].column == col)
						{
							elements[row][k].value += val;
							index_exist = true;
							break;
						}
					}
					if(index_exist == false)
						elements[row].push_back(row_entry(col, val));
				}
	}
	
	void add_matrix(double *M, unsigned int size, unsigned int offset_row, unsigned int offset_col)
	{
		// Add indices for the sparse matrix
		for(unsigned int i = 0; i<size; ++i)
			for(unsigned int j = 0; j<size; ++j)
			{
				unsigned int row = i + offset_row;
				unsigned int col = j + offset_col;
				double val = M[i*size+j];

				bool index_exist = false;
				for(unsigned int k=0; k < elements[row].size(); ++k)
				{
					if(elements[row][k].column == col)
					{
						elements[row][k].value += val;
						index_exist = true;
						break;
					}
				}
				if(index_exist == false)
					elements[row].push_back(row_entry(col, val));
			}
	}
	
	void insertValue(double val, unsigned int row, unsigned int col)
	{
		bool index_exist = false;
		for(unsigned int i = 0; i < elements[row].size(); ++i)
		{
			if(elements[row][i].column == col)
			{
				elements[row][i].value += val;
				index_exist = true;
				break;
			}
		}
		if(index_exist == false)
			elements[row].push_back(row_entry(col, val));
	}
	
	void sort_bubble(void)
	{
		// the algo. is here the most effective when the size is <10
		// speeds up linear on multicore

		#pragma omp parallel for
		for(int k=0; k < elements.size(); ++k)
		{
			unsigned int length = elements[k].size();
			unsigned int i, j;

			for (i = 0; i < length - 1; ++i)
			{
				for (j = 0; j < length - i - 1; ++j)
				{
					if (elements[k][j] > elements[k][j + 1])
					{
						row_entry tmp = elements[k][j];
						elements[k][j] = elements[k][j + 1];
						elements[k][j + 1] = tmp;
					}
				}
			}
		}
	}

	void sort_quick(void)
	{
		#pragma omp parallel for
		for(int k=0; k < elements.size(); ++k)
		{
			std::sort(elements[k].begin(), elements[k].end());
		}
	}

	void sort_network(void)
	{
		// the clever one, on single core just a bit slower than threaded bubblesort
		//	but it losed speed on multicore ..

		#pragma omp parallel for
		for(int k=0; k < elements.size(); ++k)
		{
			sort_network_vector(&elements[k].front(), elements[k].size());
		}
	}

	void dirichlet_boundary(index_matrix &dirichlet)
	{
		// Create List with indices to delete
		std::vector<unsigned int> elements_to_delete;
		for(int i=0; i<dirichlet.size(ROWS); ++i)
			for(int j = 0; j < 3; ++j)
				if(dirichlet.element[i][j + 1] == 1)
					elements_to_delete.push_back(dirichlet.element[i][0] * 3 + j);
		std::sort(elements_to_delete.begin(), elements_to_delete.end());

		//Delete Rows and push back the diagonal-entry with a one
		#pragma omp parallel for
		for(int i=0; i<elements_to_delete.size(); ++i)
		{
			unsigned int act_index = elements_to_delete[i];
			elements[act_index].clear();
			elements[act_index].push_back(row_entry(act_index, 1));
		}

		//Delete columns
		#pragma omp parallel for
		for(int i = 0; i < elements.size(); ++i)
		{
			std::vector<unsigned int> row_entries_to_delete;
			for(int j = 0; j < elements[i].size() - 1; ++j)
			{
				int act_element_col = elements[i][j].column;
				if(std::binary_search(elements_to_delete.begin(), elements_to_delete.end(), act_element_col))
				{
//					elements[i][j].value = 0;
					row_entries_to_delete.push_back(j);
				}
			}
			
			for(int k = row_entries_to_delete.size(); k > 0; --k)
				elements[i].erase(elements[i].begin() + row_entries_to_delete[k-1]);
		}
	}

	void skalar_multiply(double skalar_factor)
	{
		//#pragma omp parallel for
		for(unsigned int i=0; i < elements.size(); ++i)
			for(unsigned int j=0; j < elements[i].size(); ++j)
				elements[i][j].value *= skalar_factor;
	}

	void add_indices_from_matrix(matrix &A, bool is_square_matrix, bool is_transposed, unsigned int off_row, unsigned int off_col)
	{
		for(unsigned int i=0; i< A.size(ROWS); ++i)
			for(unsigned int j=0; j< A.size(COLUMNS); ++j)
			{
				if(((is_square_matrix) && (i <= j)) || (!is_square_matrix))
				{
					double buffer = A.element[i][j];
					if(abs(buffer) > ZERO_TINY_ELEMENTS_THRESHOLD)
					{
						if(is_transposed)
						{
							unsigned int row = j+off_row;
							unsigned int col = i+off_col;
							double val = A.element[i][j];

							elements[row].push_back(row_entry(col, val));
						}
						else
						{
							unsigned int row = i+off_row;
							unsigned int col = j+off_col;
							double val = A.element[i][j];

							elements[row].push_back(row_entry(col, val));
						}
					}
				}
			}
	}

	void add_indices_from_sparse_matrix(sparse_CRS_matrix &A, bool is_transposed, unsigned int off_row, unsigned int off_col)
	{
		for(unsigned int i=0; i< A.elements.size(); ++i)
			for(unsigned int j=0; j < A.elements[i].size(); ++j)
			{
				double value 	 = A.elements[i][j].value;
				unsigned int row = i;
				unsigned int col = A.elements[i][j].column;

				if(is_transposed)
					this->elements[col + off_row].push_back(row_entry(row + off_col, value));

				else
					this->elements[row + off_row].push_back(row_entry(col + off_col, value));

			}
	}


	friend std::ostream& operator<< (std::ostream& stream, sparse_CRS_matrix& rmatrix)
	{
		stream << endl;

		for(unsigned int i=0; i < rmatrix.elements.size(); ++i)
			for(unsigned int j=0; j < rmatrix.elements[i].size(); ++j)

			stream << i << " ; " << rmatrix.elements[i][j].column << " ; " << rmatrix.elements[i][j].value << endl;


		return stream;

	}

	void PrintMatrix(void)
	{
		unsigned int act_index = 0;
		for(unsigned int i=0; i < N; ++i)
			for(unsigned int j=0; j < IA[i+1]-IA[i]; ++j)
			{
				cout << i << " ; " << JA[act_index] << " ; " << A[act_index] << endl;
				act_index++;
			}

		//IA = new int[N + 1];
		//JA = new int[NNZ];
		//A = new double[NNZ];
	}


	void allocate_CRS(int mN, int mNNZ)
	{
		N = mN;
		NNZ = mNNZ;

		IA = new int[N + 1];
		JA = new int[NNZ];
		A = new double[NNZ];

		// Check if both allocations were successful. If not throw an error and exit.
		if((IA == 0) || (JA == 0) || (A == 0))
		{
			double req_mem_size = (double)((N+1) * sizeof(int) + NNZ * (sizeof(int) + sizeof(double))) / 1024.0 / 1024.0;
 			cerr << "[matrix-base]: Cannot allocate enough Memory. Requested size [MByte]: " << req_mem_size << endl;
			exit(EXIT_FAILURE);
		}


		clear_CRS();
	}

	void deallocate_CRS(void)
	{
		if(NNZ != 0)
		{
			delete[] IA;
			delete[] JA;
			delete[] A;

	    	IA = 0;
	    	JA = 0;
	    	A = 0;
	    	N = 0;
	    	NNZ = 0;
		}

	}

	void clear_CRS(void)
	{
		for( int i = 0; i <= N; ++i)
		{
			IA[i] = 0;
		}

		for( int i = 0; i < NNZ; ++i)
		{
			JA[i] = 0;
			A[i] = 0;
		}
	}

	void build_CRS (bool zero_based=true)
	{
		int NNZ = 0;

		for(unsigned int i = 0; i < elements.size(); ++i)
			NNZ += elements[i].size();

		unsigned int N = elements.size();

		allocate_CRS(N, NNZ);

		//#pragma omp parallel for
		unsigned int act_index = 0;
		for(unsigned int i = 0; i < elements.size(); ++i)
			for(unsigned int j = 0; j < elements[i].size(); ++j)
			{
				A[act_index] = elements[i][j].value;
				JA[act_index] = elements[i][j].column;
				++act_index;

				unsigned int ROW_INDEX = i + 1;

				if(ROW_INDEX > N ) cerr << "dimension error!" << ROW_INDEX << endl;

				IA[ROW_INDEX]++;
			}

		for(unsigned int i=1; i <= N; ++i)
		{
			IA[i] += IA[i-1];
		}

		density = (double)NNZ / ((double)N * (double)N);

		if(!zero_based)
		{
			for(unsigned int i = 0; i < N + 1; ++i) IA[i]++;
			for(unsigned int i = 0; i < NNZ; ++i)	JA[i]++;
		}
	}
	
	void MakeAveragedUpperTriangularMatrix(char avgType)
	{
		for(unsigned int i = 0; i < elements.size(); ++i)
			for(unsigned int j = 0; j < elements[i].size(); ++j)
			{
				// Check if symmetric element exists
				unsigned int col = elements[i][j].column;
				if(col <= i) continue;
				for(unsigned int k = 0; k < elements[col].size(); ++k)
					if(elements[col][k].column == i)
					{
						if (avgType == 'A') /// Arithmetic average
						{
							elements[i][j].value *= 0.5;
							elements[i][j].value += 0.5 * elements[col][k].value;
						}
						else if (avgType == 'G') /// Geometric average
						{
							int sign = 1 - 2 * signbit(elements[i][j].value);
							elements[i][j].value = sign * sqrt(elements[i][j].value * elements[col][k].value);
						}				
						break;
					}
			}
		MakeUpperTriangularMatrix();
	}
	
	void MakeUpperTriangularMatrix(void)
	{
		for(unsigned int i = 0; i < elements.size(); ++i)
			for(unsigned int j = 0; j < elements[i].size(); ++j)
				if(elements[i][j].column >= i)
				{
					elements[i].erase(elements[i].begin(), elements[i].begin()+j);
					break;
				}
	}

	void MakeLowerTriangularMatrix(void)
	{
		for(unsigned int i = 0; i < elements.size(); ++i)
			for(unsigned int j = 0; j < elements[i].size(); ++j)
				if(elements[i][j].column > i)
				{
					elements[i].erase(elements[i].begin()+j, elements[i].end());
					break;
				}
	}
	
	int checkSymmetry(void)
	{
		double threshold = 5e-7;
		int symmetryFails = 0;
		bool symmetricElementExists;
		
		double absDiff;
		double maxAbsDiff = 0;
		double minAbsDiff = 1e3;
		double avgAbsDiff = 0;
		double refRelDiff = 0;
		
		double relDiff;
		double maxRelDiff = 0;
		double minRelDiff = 1;
		double avgRelDiff = 0;
		double refAbsDiff = 0;
		
		for (unsigned int i = 0; i < elements.size(); ++i)
			for (unsigned int j = 0; j < elements[i].size(); ++j)
			{
				unsigned int col = elements[i][j].column;
				for (unsigned int k = 0; k < elements[col].size(); ++k)
				{
					if (elements[col][k].column == i)
					{
						symmetricElementExists = true;
						absDiff = abs(elements[col][k].value - elements[i][j].value);
						relDiff = absDiff / abs(elements[col][k].value + elements[i][j].value);	
						break;
					}
					else
					{
						absDiff = abs(elements[i][j].value);
						relDiff = 1;
					}
				}
				if (absDiff > threshold)
				{
					maxAbsDiff = std::max(maxAbsDiff, absDiff);
					minAbsDiff = std::min(minAbsDiff, absDiff);
					avgAbsDiff += absDiff;
					
					maxRelDiff = std::max(maxRelDiff, relDiff);
					minRelDiff = std::min(minRelDiff, relDiff);
					avgRelDiff += relDiff;
					
					if (maxRelDiff == relDiff)
						refAbsDiff = absDiff;
					
					if (maxAbsDiff == absDiff)
						refRelDiff = relDiff;
					
					++symmetryFails;
				}
			}
		if (symmetryFails == 0)
		{
			minAbsDiff = 0;
			minRelDiff = 0;
		}
		else
		{
			avgAbsDiff /= symmetryFails;
			avgRelDiff /= symmetryFails;
		}
		
		cout << "Symmetry info: " << endl;
		cout << "  Fails:      " << symmetryFails / 2 << " (difference > " << threshold << ")" << endl;
		cout << "  maxAbsDiff: " << maxAbsDiff << " (relDiff = " << refRelDiff << ")" << endl;
		cout << "  minAbsDiff: " << minAbsDiff << endl;
		cout << "  avgAbsDiff: " << avgAbsDiff << endl;
		cout << "  maxRelDiff: " << maxRelDiff << " (absDiff = " << refAbsDiff << ")" << endl;
		cout << "  minRelDiff: " << minRelDiff << endl;
		cout << "  avgRelDiff: " << avgRelDiff << endl;
		
		return symmetryFails / 2;
	}
	
	matrix MakeFullMatrix(void)
	{
		matrix fullMatrix;
		fullMatrix.allocate_and_clear(elements.size(), elements.size());
		for(unsigned int i = 0; i < elements.size(); ++i)
			for(unsigned int j = 0; j < elements[i].size(); ++j)
				fullMatrix.element[i][elements[i][j].column] = elements[i][j].value;
		return fullMatrix;
	}
	
	void PrintInfo(void)
	{	
		N = elements.size();
		NNZ = 0;
		
		int RowLength;
		int MaxRowLength = 0;
		int MinRowLength = N;
		for(int i = 0; i < N; ++i)
		{	
			RowLength = elements[i].size();
			if(MaxRowLength < RowLength) MaxRowLength = RowLength;
			if(MinRowLength > RowLength) MinRowLength = RowLength;
			NNZ += RowLength;
		}
		
		double AvgRowLength = (double)NNZ / (double)N;
		
		double AbsValue;
		double MaxAbsValue = 0;
		double MinAbsValue = abs(elements[0][0].value);
		double AvgAbsValue = 0;
		for(int i = 0; i < N; ++i)
			for(int j = 0; j < elements[i].size(); ++j)
			{
				AbsValue = abs(elements[i][j].value);
				if(MaxAbsValue < AbsValue) MaxAbsValue = AbsValue;
				if(MinAbsValue > AbsValue) MinAbsValue = AbsValue;
				AvgAbsValue += AbsValue;
			}
		AvgAbsValue /= NNZ;
		
		density = (double)NNZ / ((double)N * (double)N);
		
		cout << "Matrix info: " << endl;
		cout << "  DOF:          " << N << endl;
		cout << "  Density:      " << density << endl;
		cout << "  MaxRowLength: " << MaxRowLength << endl;
		cout << "  MinRowLength: " << MinRowLength << endl;
		cout << "  AvgRowLength: " << AvgRowLength << endl;
		cout << "  MaxAbsValue:  " << MaxAbsValue << endl;
		cout << "  MinAbsValue:  " << MinAbsValue << endl;
		cout << "  AvgAbsValue:  " << AvgAbsValue << endl;
	}
	
	bool LoadMatrixFromFile(char* FileName, unsigned int MatSize)
	{

		ifstream InputFile;

		InputFile.open(FileName, ios::in);

		this->allocate_elements(MatSize, MatSize);

		double readvalue=0;

		// try to open file
		if (InputFile.is_open())
		{
			for(unsigned int i=0; i < MatSize; ++i)
				for(unsigned int j=0; j < MatSize; ++j)
				{
					InputFile >> readvalue;
					this->elements[i].push_back(row_entry(j, readvalue));

					// if an error occurred while converting failbit was set. Check for this, throw a message and quit
					if((InputFile.rdstate() & ifstream::failbit))
					{
							cerr << "matrix_io : while reading " << i*MatSize+j << ". element from file \n";
							return false;
					}
				}
		}
		else
		{
			cerr << "matrix_io: File could not be opened. \n" ;
			return false;
		}
		return true;
	}

};

#endif /* MATRIX_BASE_SPARSE_H_ */
