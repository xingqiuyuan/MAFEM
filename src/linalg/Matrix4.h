///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
/** 
 * \file Matrix4.h 
 * \brief Contains definition of the Base::Matrix4 class and operators. 
 */

#ifndef __MATRIX4_H
#define __MATRIX4_H

#include <Base.h>
#include "VectorN.h"
#include "Vector3.h"
#include "Point3.h"
#include "Matrix3.h"
#include "AffineTransformation.h"

namespace Base {

/**
 * \brief A 4x4 matrix class.
 *
 * This class stores an array of 4 times 4 floating-point value. 
 * It is therefore a matrix with 4 rows and 4 columns.
 * 
 * Such a matrix is used to describe transformations in 3d space
 * like rotation, shearing, scaling, translation and perspective projections.
 * 
 * In contrast to the AffineTransformation matrix class this Matrix4 class
 * can contain perspective projections.
 * 
 * \author Alexander Stukowski
 * \sa AffineTransformation, Matrix3
 */
class Matrix4
{
public:
	/// \brief The four columns of the matrix.
	Vector4 m[4];

	/// \brief Constructs a matrix without initializing its elements.
	/// \note All elements are left uninitialized by this constructor and have therefore a random value!
	Matrix4() {}

	/// \brief Copy constructor that initializes the matrix elements from the elements of another matrix.
	Matrix4(const Matrix4& mat) { 
		memcpy(m, mat.m, sizeof(m)); 
	}

	/// \brief Constructor that initializes all 16 elements of the matrix.
	/// \note Values are given in row-major order, i.e. row by row.
	Matrix4(FloatType m11, FloatType m12, FloatType m13, FloatType m14,
			FloatType m21, FloatType m22, FloatType m23, FloatType m24,
			FloatType m31, FloatType m32, FloatType m33, FloatType m34,
			FloatType m41, FloatType m42, FloatType m43, FloatType m44)
	{
		m[0][0] = m11; m[0][1] = m21; m[0][2] = m31; m[0][3] = m41;
		m[1][0] = m12; m[1][1] = m22; m[1][2] = m32; m[1][3] = m42;
		m[2][0] = m13; m[2][1] = m23; m[2][2] = m33; m[2][3] = m43;
		m[3][0] = m14; m[3][1] = m24; m[3][2] = m34; m[3][3] = m44;
	}

	/// \brief Constructor that extends a 3x3 matrix to a 4x4 matrix.
	Matrix4(const Matrix3& mat) { 
		m[0][0] = mat.m[0][0]; m[0][1] = mat.m[0][1]; m[0][2] = mat.m[0][2]; m[0][3] = 0.0;
		m[1][0] = mat.m[1][0]; m[1][1] = mat.m[1][1]; m[1][2] = mat.m[1][2]; m[1][3] = 0.0;
		m[2][0] = mat.m[2][0]; m[2][1] = mat.m[2][1]; m[2][2] = mat.m[2][2]; m[2][3] = 0.0;
		m[3][0] = 0.0;         m[3][1] = 0.0;         m[3][2] = 0.0;         m[3][3] = 1.0;
	}

	/// \brief Constructor that extends a 3x4 matrix to a 4x4 matrix.
	Matrix4(const AffineTransformation& mat) { 
		m[0][0] = mat.m[0][0]; m[0][1] = mat.m[0][1]; m[0][2] = mat.m[0][2]; m[0][3] = 0.0;
		m[1][0] = mat.m[1][0]; m[1][1] = mat.m[1][1]; m[1][2] = mat.m[1][2]; m[1][3] = 0.0;
		m[2][0] = mat.m[2][0]; m[2][1] = mat.m[2][1]; m[2][2] = mat.m[2][2]; m[2][3] = 0.0;
		m[3][0] = mat.m[3][0]; m[3][1] = mat.m[3][1]; m[3][2] = mat.m[3][2]; m[3][3] = 1.0;
	}

	/// \brief Initializes the matrix to the identity matrix.
	/// All diagonal elements are set to one and all off-diagonal elements are set to zero.
	Matrix4(const IdentityMatrix& IDENTITY) {
		m[0][0] = 1.0; m[0][1] = 0.0; m[0][2] = 0.0; m[0][3] = 0.0;
		m[1][0] = 0.0; m[1][1] = 1.0; m[1][2] = 0.0; m[1][3] = 0.0;
		m[2][0] = 0.0; m[2][1] = 0.0; m[2][2] = 1.0; m[2][3] = 0.0;
		m[3][0] = 0.0; m[3][1] = 0.0; m[3][2] = 0.0; m[3][3] = 1.0;
	}

	/// \brief Initializes the matrix to the null matrix.
	/// All matrix elements are set to zero by this constructor.
	Matrix4(const NullMatrix& NULL_MATRIX) {
		memset(m, 0, sizeof(m));
	}

	/// \brief Assignment operator that creates a copy of the matrix.
	Matrix4& operator=(const Matrix4& mat) { 
		memcpy(m, mat.m, sizeof(m)); return(*this); 
	}

	/// \brief Assignment operator that converts a 3x4 matrix to a 4x4 matrix.
	Matrix4& operator=(const AffineTransformation& mat) { 
		m[0][0] = mat.m[0][0]; m[0][1] = mat.m[0][1]; m[0][2] = mat.m[0][2]; m[0][3] = 0.0;
		m[1][0] = mat.m[1][0]; m[1][1] = mat.m[1][1]; m[1][2] = mat.m[1][2]; m[1][3] = 0.0;
		m[2][0] = mat.m[2][0]; m[2][1] = mat.m[2][1]; m[2][2] = mat.m[2][2]; m[2][3] = 0.0;
		m[3][0] = mat.m[3][0]; m[3][1] = mat.m[3][1]; m[3][2] = mat.m[3][2]; m[3][3] = 1.0;
        return(*this);		
	}

	/// \brief Returns the value of a matrix element.
	/// \param row The row of the element to return (0-3). 
	/// \param col The column of the element to return (0-3).
	/// \return The value of the matrix element.  
	FloatType operator()(size_t row, size_t col) const { 
		MAFEM_ASSERT_MSG(row>=0 && row<4, "Matrix4", "Row index out of range");
		MAFEM_ASSERT_MSG(col>=0 && col<4, "Matrix4", "Column index out of range");
		return m[col][row];
	}

	/// \brief Returns a reference to a matrix element.
	/// \param row The row of the element to return (0-3). 
	/// \param col The column of the element to return (0-3).
	FloatType& operator()(size_t row, size_t col) { 
		MAFEM_ASSERT_MSG(row>=0 && row<4, "Matrix4", "Row index out of range");
		MAFEM_ASSERT_MSG(col>=0 && col<4, "Matrix4", "Column index out of range");
		return m[col][row];
	}

	/// \brief Returns a column from the matrix.
	/// \param i The column to return (0-3).
	/// \return The i-th column of the matrix as a vector.
	const Vector4& column(size_t i) const { 
		MAFEM_ASSERT_MSG(i>=0 && i<4, "Matrix4::column()", "Column index out of range.");
		MAFEM_STATIC_ASSERT(sizeof(m) == sizeof(Vector4)*4);
		return m[i];
	}

	/// \brief Returns a reference to a column in the matrix.
	/// \param i The column to return (0-3).
	/// \return The i-th column of the matrix as a vector reference. Modifying the vector modifies the matrix.
	Vector4& column(size_t i) {
		MAFEM_ASSERT_MSG(i>=0 && i<4, "Matrix4::column()", "Column index out of range.");
		MAFEM_STATIC_ASSERT(sizeof(m) == sizeof(Vector4)*4);
		return m[i];
	}
	
	/// \brief Returns a column from the matrix.
	/// \param i The column to return (0-3).
	/// \return The i-th column of the matrix as a vector.
	const Vector4& getColumn(size_t i) const { return column(i); }
	
	/// \brief Sets all elements in one column of the matrix.
	/// \param i The column to set (0-3).
	/// \param c The new element values as a vector.
	void setColumn(size_t i, const Vector4& c) { column(i) = c; }	

	/// \brief Returns a row from the matrix.
	/// \param i The row to return (0-3).
	/// \return The i-th row of the matrix as a vector.
	Vector4 row(size_t i) const {
		MAFEM_ASSERT_MSG(i>=0 && i<4, "Matrix4::row()", "Row index out of range.");
		return Vector4(m[0][i], m[1][i], m[2][i], m[3][i]);
	}

	/// \brief Sets all elements in one row of the matrix.
	/// \param i The row to set (0-3).
	/// \param r The new element values as a vector.
	void setRow(size_t i, const Vector4& r) {
		MAFEM_ASSERT_MSG(i>=0 && i<4, "Matrix4::setRow()", "Row index out of range.");
		m[0][i] = r[0]; m[1][i] = r[1]; m[2][i] = r[2]; m[3][i] = r[3];
	}	

	/// \brief Returns the translational part of this transformation matrix.
	/// \return A vector that specifies the translation.
	const Vector3& getTranslation() const {
		return *(Vector3*)&(m[3]);
	}

	/// \brief Computes the inverse of the matrix. 
	/// \throw Exception if matrix is not invertible because it is singular.
	Matrix4 inverse() const;

	/// \brief Computes the determinant of the matrix.
	FloatType determinant() const {
		return (m[0][3] * m[1][2] * m[2][1] * m[3][0]-m[0][2] * m[1][3] * m[2][1] * m[3][0]-m[0][3] * m[1][1] * m[2][2] * m[3][0]+m[0][1] * m[1][3] * m[2][2] * m[3][0]+
				m[0][2] * m[1][1] * m[2][3] * m[3][0]-m[0][1] * m[1][2] * m[2][3] * m[3][0]-m[0][3] * m[1][2] * m[2][0] * m[3][1]+m[0][2] * m[1][3] * m[2][0] * m[3][1]+
				m[0][3] * m[1][0] * m[2][2] * m[3][1]-m[0][0] * m[1][3] * m[2][2] * m[3][1]-m[0][2] * m[1][0] * m[2][3] * m[3][1]+m[0][0] * m[1][2] * m[2][3] * m[3][1]+
				m[0][3] * m[1][1] * m[2][0] * m[3][2]-m[0][1] * m[1][3] * m[2][0] * m[3][2]-m[0][3] * m[1][0] * m[2][1] * m[3][2]+m[0][0] * m[1][3] * m[2][1] * m[3][2]+
				m[0][1] * m[1][0] * m[2][3] * m[3][2]-m[0][0] * m[1][1] * m[2][3] * m[3][2]-m[0][2] * m[1][1] * m[2][0] * m[3][3]+m[0][1] * m[1][2] * m[2][0] * m[3][3]+
				m[0][2] * m[1][0] * m[2][1] * m[3][3]-m[0][0] * m[1][2] * m[2][1] * m[3][3]-m[0][1] * m[1][0] * m[2][2] * m[3][3]+m[0][0] * m[1][1] * m[2][2] * m[3][3]);
	}

	/// \brief Returns a pointer to the first element of the matrix.
	/// \return A pointer to 16 matrix element in column-major order.
	/// \sa constData() 
	FloatType* data() {
        MAFEM_STATIC_ASSERT(sizeof(m) == sizeof(FloatType)*16);
		return (FloatType*)this;
	}

	/// \brief Returns a pointer to the first element of the matrix.
	/// \return A pointer to 16 matrix element in column-major order.
	/// \sa data() 
	const FloatType* constData() const {
        MAFEM_STATIC_ASSERT(sizeof(m) == sizeof(FloatType)*16);
		return (const FloatType*)this;
	}

	/// \brief Returns a string representation of this matrix.
	QString toString() const { 
		return QString("%1\n%2\n%3\n%4").arg(row(0).toString(), row(1).toString(), row(2).toString(), row(3).toString());
	}

	/// \brief Generates the identity matrix.
	/// \return A matrix with all diagonal elements set to one and all off-diagonal elements set to zero.
	static Matrix4 identity() { return Matrix4(IDENTITY); }
	/// \brief Generates a matrix that does perspective projection from a viewing frustum.
	static Matrix4 frustum(FloatType left, FloatType right, FloatType bottom, FloatType top, FloatType znear, FloatType zfar);
	/// \brief Generates a matrix that does perspective projection.
	static Matrix4 perspective(FloatType fovy, FloatType aspect, FloatType znear, FloatType zfar);
	/// \brief Generates a projection matrix for orthogonal projection.
	static Matrix4 ortho(FloatType left, FloatType right, FloatType bottom, FloatType top, FloatType znear, FloatType zfar);
	/// \brief Generates a pure rotation matrix around the X axis.
	/// \param angle The rotation angle in radians.
	static Matrix4 rotationX(const FloatType angle);
	/// \brief Generates a pure rotation matrix around the Y axis.
	/// \param angle The rotation angle in radians.
	static Matrix4 rotationY(const FloatType angle);
	/// \brief Generates a pure rotation matrix around the Z axis.
	/// \param angle The rotation angle in radians.
	static Matrix4 rotationZ(const FloatType angle);
	/// \brief Generates a pure rotation matrix around the given axis.
	static Matrix4 rotation(const Rotation& rot);
	/// \brief Generates a pure translation matrix.
	static Matrix4 translation(const Vector3& translation);

private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) { ar & m; }	

	friend class boost::serialization::access;
};

/// \brief Multiplies a 4x4 matrix with a Vector4.
inline Vector4 operator*(const Matrix4& a, const Vector4& v) 
{
	Vector4 r;
	r[0] = a.m[0][0]*v[0] + a.m[1][0]*v[1] + a.m[2][0]*v[2] + a.m[3][0]*v[3];
	r[1] = a.m[0][1]*v[0] + a.m[1][1]*v[1] + a.m[2][1]*v[2] + a.m[3][1]*v[3]; 
	r[2] = a.m[0][2]*v[0] + a.m[1][2]*v[1] + a.m[2][2]*v[2] + a.m[3][2]*v[3]; 
	r[3] = a.m[0][3]*v[0] + a.m[1][3]*v[1] + a.m[2][3]*v[2] + a.m[3][3]*v[3];
	return r;
}

/// \brief Multiplies a 4x4 matrix with a Vector3.
inline Vector3 operator*(const Matrix4& a, const Vector3& v) 
{
	return Vector3(a.m[0][0]*v[0] + a.m[1][0]*v[1] + a.m[2][0]*v[2], 
				   a.m[0][1]*v[0] + a.m[1][1]*v[1] + a.m[2][1]*v[2], 
				   a.m[0][2]*v[0] + a.m[1][2]*v[1] + a.m[2][2]*v[2])
				   / (a.m[0][3]*v[0] + a.m[1][3]*v[1] + a.m[2][3]*v[2] + a.m[3][3]);
}

/// \brief Multiplies a 4x4 matrix with a Point3.
inline Point3 operator*(const Matrix4& a, const Point3& p)
{
	return Point3(a.m[0][0]*p[0] + a.m[1][0]*p[1] + a.m[2][0]*p[2] + a.m[3][0],
				  a.m[0][1]*p[0] + a.m[1][1]*p[1] + a.m[2][1]*p[2] + a.m[3][1],
				  a.m[0][2]*p[0] + a.m[1][2]*p[1] + a.m[2][2]*p[2] + a.m[3][2]) 
					/ (a.m[0][3]*p[0] + a.m[1][3]*p[1] + a.m[2][3]*p[2] + a.m[3][3]);
}

/// \brief Multiplies a 4x4 matrix with a 4x4 Matrix.
inline Matrix4 operator*(const Matrix4& a, const Matrix4& b) 
{
	Matrix4 m;
	for(int i=0; i<4; i++) {
		for(int j=0; j<4; j++) {
			FloatType v = 0.0;
			for(int k=0; k<4; k++)
				v += a(i, k) * b(k, j);
			m(i, j) = v;
		}
	}
	return m;
}

/// \brief Multiplies a 4x4 matrix with a 3x4 Matrix.
inline Matrix4 operator*(const Matrix4& a, const AffineTransformation& b) 
{
    return a * Matrix4(b);
}

/// \brief Multiplies a 3x4 matrix with a 4x4 Matrix.
inline Matrix4 operator*(const AffineTransformation& a, const Matrix4& b) 
{
    return Matrix4(a) * b;
}

/// \brief Multiplies a 4x4 matrix with a 3x3 Matrix.
inline Matrix4 operator*(const Matrix4& a, const Matrix3& b) 
{
    return a * Matrix4(b);
}

/// \brief Multiplies a 3x3 matrix with a 4x4 Matrix.
inline Matrix4 operator*(const Matrix3& a, const Matrix4& b) 
{
    return Matrix4(a) * b;
}

/// \brief Writes the matrix to an output stream.
inline std::ostream& operator<<(std::ostream &os, const Matrix4& m) {
	return os << m.row(0) << endl << m.row(1) << endl << m.row(2) << endl << m.row(3);
}

/// \brief Writes the vector to a logging stream.
inline LoggerObject& operator<<(LoggerObject& log, const Matrix4& m)
{
	for(size_t row = 0; row < 4; row++)
		log.space() << m(row, 0) << m(row, 1) << m(row, 2) << m(row, 3) << endl;
	return log;
} 

/// \brief Writes a matrix to an output stream.
inline QDataStream& operator<<(QDataStream& stream, const Matrix4& m)
{
	return stream << m.column(0) << m.column(1) << m.column(2) << m.column(3);
}

/// \brief Reads a matrix from an input stream.
inline QDataStream& operator>>(QDataStream& stream, Matrix4& m)
{
	return stream >> m.column(0) >> m.column(1) >> m.column(2) >> m.column(3);
}

};	// End of namespace Base

Q_DECLARE_METATYPE(Base::Matrix4)
Q_DECLARE_TYPEINFO(Base::Matrix4, Q_PRIMITIVE_TYPE);

// We don't need versioning info for this type when serializing.
BOOST_CLASS_IMPLEMENTATION(Base::Matrix4, boost::serialization::object_serializable)

#endif // __MATRIX4_H
