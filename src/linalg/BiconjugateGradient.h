///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_BICONJUGATE_GRADIENT_H
#define __MAFEM_BICONJUGATE_GRADIENT_H

#include <MAFEM.h>
#include "Preconditioner.h"

namespace MAFEM {

/******************************************************************************
* This header file contains methods to solve a linear system of equations
* using the Biconjugate Grdaient (BCG) method.
******************************************************************************/

/******************************************************************************
* Solves a linear system of equations using BCG method.
******************************************************************************/
template <class MAT, class VEC>
size_t bicg_solve(const MAT& A, 
				VEC& x, 
				const VEC& b, 
				const size_t max_iter,
				const FloatType toler)
{
	typedef typename VEC::value_type  value_type;

	const size_t size = x.size();
	size_t iteration = 0;

	value_type nrm = sqrt(inner_prod(b,b));

	VEC r(size);
	VEC rr(size);
	VEC p(size);
	VEC pp(size);
	VEC q(size);
	VEC qq(size);

	FloatType res, rho_1, rho_2, alpha, beta;

	// resid = b - prod(A, x);
	noalias(r) = b;
	axpy_prod(A, -x, r, false);
	noalias(rr) = r;

	if(nrm == 0.0)
		nrm = 1;
	if((res = sqrt(inner_prod(r,r)) / nrm) <= toler) {
		//std::cout << "Convergence after  0 iterations." << std::endl;
		return 0;
	}

	while(iteration < max_iter) {
		rho_1 = inner_prod(r,rr);
		if (rho_1 == 0) {
			//std::cout << "Convergence after " << iteration << " iterations." << std::endl;
			return iteration;
		}
		if (iteration == 0) {
			noalias(p) = r;
			noalias(pp) = rr;
		} else {
			beta = rho_1 / rho_2;
			noalias(p) = r + beta*p;
			noalias(pp) = rr + beta*pp;
		}
		axpy_prod(A, p, q, true);
		axpy_prod(pp, A, qq, true);
		alpha = rho_1/inner_prod(pp,q);
		x.plus_assign(alpha * p);
		r.minus_assign(alpha * q);
		rr.minus_assign(alpha * qq);
		rho_2 = rho_1;

		//std::cout << "Iteration : " << std::setw(4) << iteration;
		//std::cout << "  ... Residual : " << res << std::endl;
		if((res = sqrt(inner_prod(r,r)) / sqrt(inner_prod(b,b))) < toler) {
			//std::cout << "Convergence after " << iteration << " iterations." << std::endl;
			return iteration;
		}
		iteration++;
	}
	return iteration;
}

}; // End of namespace MAFEM

#endif		// __MAFEM_BICONJUGATE_GRADIENT_H
