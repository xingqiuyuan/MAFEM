///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
/** 
 * \file Quaternion.h 
 * \brief Contains definition of the Base::Quaternion class and operators. 
 */

#ifndef __QUATERNION_H
#define __QUATERNION_H

#include <Base.h>
#include "Vector3.h"

namespace Base {

class AffineTransformation;	// defined in AffineTransformation.h

// Empty tag class.
class IdentityQuaternion {};
// This dummy instance should be passed to the Quaternion class constructor to initialize it to the identity quaternion.
extern IdentityQuaternion IDENTITY_QUAT;

/**
 * \brief Represents a rotation in 3d space.
 * 
 * Rotations can also be represented by the Rotation and the AffineTransformation
 * class. Please note that only the Rotation class is able to represent rotations with more than
 * one revolutions, i.e. rotation angles larger than 360 degrees.
 * 
 * \author Alexander Stukowski
 * \sa Rotation, AffineTransformation
 */
class Quaternion
{
public:	

	/// \brief The four components of the quaternion.
	FloatType X, Y, Z, W;

	/////////////////////////////// Constructors /////////////////////////////////

	/// \brief Constructs a quaternion without initializing its components.
	/// \note All components are left uninitialized by this constructor and will therefore have a random value!
	Quaternion() {}

	/// \brief Initializes the quaternion with the given values.
	/// \param _x The first quaternion component.
	/// \param _y The second quaternion component.
	/// \param _z The third quaternion component.
	/// \param _w The fourth quaternion component.
	Quaternion(FloatType _x, FloatType _y, FloatType _z, FloatType _w) : X(_x), Y(_y), Z(_z), W(_w) {}

	/// \brief Constructs an identity quaternion.
	/// \param IDENTITY_QUAT A dummy parameter to distinguish this overloaded constructor from the others. 
	///        When using this constructor, just use the special value \c IDENTITY_QUAT here.
	/// 
	/// The new quaternion represents the null transformation, i.e. no rotation at all.
	Quaternion(IdentityQuaternion IDENTITY_QUAT) : X(0), Y(0), Z(0), W(1) {}

	/// \brief Initializes the quaternion from rotational part of a transformation matrix.
	/// \param tm A rotation matrix. 
	///
	/// It is assumed that \a tm is a pure rotation matrix.  
	explicit Quaternion(const AffineTransformation& tm);

    ///////////////////////////// Component access ///////////////////////////////

	/// \brief Returns a reference to the i-th component of the quaternion.
	/// \param i The index specifying the component to return (0=X, 1=Y, 2=Z, 3=W).
	/// \return A reference to the i-th component that can be used to change the component's value. 
	FloatType& operator[](size_t i) {
		MAFEM_STATIC_ASSERT(sizeof(Quaternion) == sizeof(FloatType) * 4);
		MAFEM_ASSERT(i>=0 && i<4);
		return (&X)[i]; 
	}

	/// \brief Returns a constant reference to the i-th component of the quaternion.
	/// \param i The index specifying the component to return (0=X, 1=Y, 2=Z, 3=W).
	/// \return The value of the i-th component. 
	const FloatType& operator[](size_t i) const {
		MAFEM_STATIC_ASSERT(sizeof(Quaternion) == sizeof(FloatType) * 4);
		MAFEM_ASSERT(i>=0 && i<4);
		return (&X)[i]; 
	}

    /////////////////////////////// Unary operators //////////////////////////////

	/// \brief Negates all components of the quaternion.
	/// \return A new quaternion with all components negated. 
	/// \note The returned quaternion does not represent the inverse rotation!
	/// \sa inverse()
	Quaternion operator-() const { return Quaternion(-X, -Y, -Z, -W); } 

	/// \brief Returns the inverse (or conjugate) of this rotation.
	/// \return A new quaternion representing the inverse rotation of this quaternion.
	Quaternion inverse() const { return Quaternion(-X, -Y, -Z, W); }

	////////////////////////////////// Comparison ////////////////////////////////

	/// \brief Compares two quaternions for equality.
	/// \param q The quaternion to compare with.
	/// \return \c true if each of the components are equal; \c false otherwise.
	bool operator==(const Quaternion& q) const { return (q.X == X && q.Y == Y && q.Z == Z && q.W == W); }

	/// \brief Compares two quaternions for inequality.
	/// \param q The quaternion to compare with.
	/// \return \c true if any of the components are not equal; \c false if all are equal.
	bool operator!=(const Quaternion& q) const { return !(q == *this); }

	///////////////////////////////// Interpolation //////////////////////////////

	/// \brief Interpolates between the two quaternions.
	/// \param q1 The first rotation.
	/// \param q2 The second rotation.
	/// \param t The parameter for the linear interpolation in the range [0,1].
	/// \return A linear interpolation between \a q1 and \a q2.
    static Quaternion interpolate(const Quaternion& q1, const Quaternion& q2, FloatType t);

    ////////////////////////////////// Utilities /////////////////////////////////

	/// \brief Returns a string representation of this quaternion.
	/// \return A string that contains the four components of the quaternion.
	QString toString() const { return QString("[%1 %2 %3 %4]").arg(X).arg(Y).arg(Z).arg(W); }

private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) { ar & X & Y & Z & W; }	

	friend class boost::serialization::access;
};

/// \brief Computes the scalar product of two quaternions.
/// \param a The first quaternion.
/// \param b The second quaternion.
/// \return The scalar quaternion product: a.X*b.X + a.Y*b.Y + a.Z*b.Z + a.W*b.W 
inline FloatType DotProduct(const Quaternion& a, const Quaternion& b) { 
	return a.X*b.X + a.Y*b.Y + a.Z*b.Z + a.W*b.W;
}

/// \brief Multiplies two quaternions.
/// \param a The first rotation.
/// \param b The second rotation.
/// \return A new rotation that is equal to first applying rotation \a b and then applying rotation \a a.
inline Quaternion operator*(const Quaternion& a, const Quaternion& b) 
{
	return Quaternion(
		a.W*b.X + a.X*b.W + a.Y*b.Z - a.Z*b.Y,
		a.W*b.Y + a.Y*b.W + a.Z*b.X - a.X*b.Z,
		a.W*b.Z + a.Z*b.W + a.X*b.Y - a.Y*b.X,
		a.W*b.W - a.X*b.X - a.Y*b.Y - a.Z*b.Z);
}

/// \brief Returns the quaternion divided by its length.
/// \param q The input quaternion.
/// \return The normalized quaternion.
inline Quaternion Normalize(const Quaternion& q) {
	MAFEM_ASSERT_MSG(DotProduct(q,q) > 0, "Normalize(const Quaternion&)", "Cannot normalize the null quaternion.");
	FloatType c = 1.0 / sqrt(DotProduct(q,q));
	return Quaternion(q.X * c, q.Y * c, q.Z * c, q.W * c);
}

/// \brief Writes the Quaternion to a text output stream.
/// \param os The output stream.
/// \param q The quaternion to write to the output stream \a os.
/// \return The output stream \a os.
inline std::ostream& operator<<(std::ostream &os, const Quaternion& q) {
	return os << '[' << q.X << ' ' << q.Y << ' ' << q.Z << ' ' << q.W << ']';
}

/// \brief Writes the Quaternion to a logging stream.
/// \param log The logging stream.
/// \param q The quaternion to write to the logging stream \a log.
/// \return The logging stream \a log.
inline LoggerObject& operator<<(LoggerObject& log, const Quaternion& q)
{
	return log.space() << "[" << q.X << q.Y << q.Z << q.W << "]";
}

/// \brief Writes a Quaternion to a binary output stream.
/// \param stream The output stream.
/// \param q The quaternion to write to the output stream \a stream.
/// \return The output stream \a stream.
inline QDataStream& operator<<(QDataStream& stream, const Quaternion& q)
{
	return stream << q.X << q.Y << q.Z << q.W;
}

/// \brief Reads a Quaternion from a binary input stream.
/// \param stream The input stream.
/// \param q Reference to a quaternion variable where the parsed data will be stored.
/// \return The input stream \a stream.
inline QDataStream& operator>>(QDataStream& stream, Quaternion& q)
{
	return stream >> q.X >> q.Y >> q.Z >> q.W;
}

};	// End of namespace Base

Q_DECLARE_METATYPE(Base::Quaternion)
Q_DECLARE_TYPEINFO(Base::Quaternion, Q_PRIMITIVE_TYPE);

// We don't need versioning info for this type when serializing.
BOOST_CLASS_IMPLEMENTATION(Base::Quaternion, boost::serialization::object_serializable)

#endif // __QUATERNION_H
