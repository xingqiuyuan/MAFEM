///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
/** 
 * \file Rotation.h 
 * \brief Contains definition of the Base::Rotation class and operators. 
 */
 
#ifndef __ROTATION_H
#define __ROTATION_H

#include <Base.h>
#include "Vector3.h"
#include "Quaternion.h"

namespace Base {

// Empty tag class.
class NullRotation {};
// This dummy instance should be passed to the Rotation class constructor to initialize it to zero.
extern NullRotation NULL_ROTATION;

/**
 * \brief This describes a three-dimensional rotation in space using an axis and an angle.
 * 
 * The rotation is defined by an unit vector that specifies the axis of rotation
 * and an angle.
 * 
 * Rotations can also be represented by the Quaternion and the AffineTransformation
 * class but only the Rotation class is able to represent rotations with more than
 * one revolutions, i.e. rotation angles larger than 360 degrees.
 * 
 * \author Alexander Stukowski
 * \sa Quaternion, AffineTransformation
 */
class Rotation
{
public:	

	/// \brief The axis of rotation. This must be a unit vector.
	Vector3 axis;
	
	/// \brief The rotation angle in radians.
	FloatType angle;

	/////////////////////////////// Constructors /////////////////////////////////

	/// \brief Constructs a Rotation object without initializing its components.
	/// \note The axis and the angle are left uninitialized by this constructor and will therefore have a random value!
	Rotation() {}

	/// \brief Constructor that builds up a rotation from an axis and an angle.
	/// \param _axis The vector that specifies the rotation axis. It does not have to be a unit vector.
	///              It is automatically normalized by the Rotation constructor.
	/// \param _angle The rotation angle in radians. 
	Rotation(const Vector3& _axis, FloatType _angle) : axis(Normalize(_axis)), angle(_angle) {}

	/// \brief Initializes the object to the null rotation.
	/// \param NULL_ROTATION A dummy parameter to distinguish this overloaded constructor from the others. 
	///        When using this constructor, just use the special value \c NULL_ROTATION here.
	/// 
	/// The axis is initialized with the (0,0,1) vector and the angle is set to zero.
    Rotation(NullRotation NULL_ROTATION) : axis(0,0,1), angle(0) {}

	/// \brief Initializes the object from rotational part of the matrix.
	/// \param tm A rotation matrix. 
	///
	/// It is assumed that \a tm is a pure rotation matrix. 
	/// The calcluated rotation angle will be in the range [-pi, +pi].  
    Rotation(const AffineTransformation& tm);

	/// \brief Initializes the object from a quaternion.
	/// \param q The input rotation.
	/// 
	/// The calcluated rotation angle will be in the range [0, 2*pi].
	Rotation(const Quaternion& q) {
		FloatType scaleSquared = q.X*q.X + q.Y*q.Y + q.Z*q.Z;
		if(scaleSquared <= FLOATTYPE_EPSILON) {
			angle = 0.0;
			axis = Vector3(0,0,1);
		}
		else {
			if(q.W < -1.0)
				angle = 2.0 * FLOATTYPE_PI;
			else if(q.W > 1.0)
				angle = 0.0;
			else
				angle = 2.0 * acos(q.W);
			axis = Vector3(q.X, q.Y, q.Z) / sqrt(scaleSquared);
			MAFEM_ASSERT(abs(Length(axis) - 1.0) <= FLOATTYPE_EPSILON);
		}
	}

    /////////////////////////////// Unary operators //////////////////////////////

	/// \brief Returns the inverse of this rotation.
	/// \return A rotation with the same axis but negative rotation angle.
	Rotation inverse() const  { return Rotation(axis, -angle); }	 

	/// \brief Converts the rotation to a Quaternion.
	/// \return A quaternion that represents the same rotation.
	///
	/// Please note that any extra revolutions are lost by this conversion.
	operator Quaternion() const {
		FloatType s, omega;
		omega = angle * 0.5;
		s = sin(omega);
		return Normalize(Quaternion(axis.X * s, axis.Y * s, axis.Z * s, cos(omega)));
	}

	/////////////////////////////// Binary operators /////////////////////////////

	/// \brief Adds the given rotation to this rotation.
	/// \param r2 The rotation to add to this rotation.
	/// \return This resulting rotation which is equal to \c r2*(*this). 
	Rotation& operator+=(const Rotation& r2) { *this = r2 * (*this); return *this; }

	/// \brief Adds the inverse of another rotation to this rotation.
	/// \param r2 The rotation to substract from this rotation.
	/// \return This resulting rotation which is equal to \c (*this)*r2.inverse(). 
	Rotation& operator-=(const Rotation& r2) { *this = (*this) * r2.inverse(); return *this; }

	////////////////////////////////// Comparison ////////////////////////////////

	/// \brief Returns whether two rotations are the same.
	/// \param r The rotation to compare with.
	/// \return \c true if the axis and the angle of both rotations are either equal or opposite;
	///         \c false otherwise.
	bool operator==(const Rotation& r) const { return ((r.axis==axis) && (r.angle==angle)) || ((r.axis==-axis) && (r.angle==-angle)); }

	/// \brief Returns whether two rotations are the not same.
	/// \param r The rotation to compare with.
	/// \return \c true if the axis or the angle of both rotations are neither equal or opposite;
	///         \c false otherwise.
	bool operator!=(const Rotation& r) const { return ((r.axis!=axis) || (r.angle!=angle)) && ((r.axis!=-axis) || (r.angle!=-angle)); }

	/// \brief Returns whether the rotation angle is zero.
	/// \param NULL_ROTATION Just use the special value \c NULL_ROTATION here.
	/// \return \c true if the rotation angle is zero; \c false otherwise. 
	bool operator==(const NullRotation& NULL_ROTATION) const { return (angle == 0.0); }

	/// \brief Returns whether the rotation angle is not zero.
	/// \param NULL_ROTATION Just use the special value \c NULL_ROTATION here.
	/// \return \c true if the rotation angle is not zero; \c false otherwise. 
	bool operator!=(const NullRotation& NULL_ROTATION) const { return (angle != 0.0); }

	///////////////////////////////// Interpolation //////////////////////////////

	/// \brief Interpolates between the two rotations and handles multiple revolutions.
	/// \param rot1 The first rotation.
	/// \param rot2 The second rotation.
	/// \param t The parameter for the linear interpolation in the range [0,1].
	/// \return A linear interpolation between \a rot1 and \a rot2.
    static Quaternion interpolate(const Rotation& rot1, const Rotation& rot2, FloatType t);

    ////////////////////////////////// Utilities /////////////////////////////////

	/// \brief Returns the number of revolutions.
	/// \return The rounded value of \c angle divided by 2*pi.
	/// \sa setRevolutions()
	/// \sa addRevolutions()
	int revolutions() const { return (int)(angle/(FLOATTYPE_PI*2.0)); }
	
	/// \brief Sets the number of revolutions.
	/// \param n The new number of revolutions. This can be negative.
	/// \sa revolutions()
	/// \sa addRevolutions()
	void setRevolutions(int n) { angle = fmod(angle, FLOATTYPE_PI*(FloatType)2.0) + (FLOATTYPE_PI*(FloatType)2.0*n); } 

	/// \brief Adds the given number of revolutions.
	/// \param n The number of revolutions to add to the angle. This can be negative.
	///
	/// The rotation angle is increased by \c n*2*pi.
	/// \sa revolutions() 
	void addRevolutions(int n) { angle += (FLOATTYPE_PI*2.0) * n; }

	/// \brief Returns a string representation of this rotation.
	/// \return A string that contains the components of the rotation structure.
	QString toString() const { return QString("[Axis: %1 Angle: %2]").arg(axis.toString()).arg(angle); }

private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) { ar & axis & angle; }	

	friend class boost::serialization::access;
};

/// \brief Performs the multiplication of two rotations.
/// \param r1 The first rotation.
/// \param r2 The second rotation.
/// \return A new rotation that is equal to first applying \a r2 and then applying \a r1.
inline Rotation operator*(const Rotation& r1, const Rotation& r2) { 
	Quaternion q1 = (Quaternion)r1;
	Quaternion q2 = (Quaternion)r2;
	Quaternion q = q1 * q2;
	Rotation result(q);
	int rev;
	if(DotProduct(r1.axis, r2.axis) >= 0.0)
		rev = (int)floor(((r1.angle+r2.angle) / (FLOATTYPE_PI*2.0)));
	else
		rev = (int)floor(((r1.angle-r2.angle) / (FLOATTYPE_PI*2.0)));
	if(((rev & 1) != 0)) {
		result.angle = -result.angle;
		rev++;
		result.axis = -result.axis;
	}
	result.addRevolutions(rev);
	return result;
}

/// \brief Writes the Rotation to a text output stream.
/// \param os The output stream.
/// \param r The rotation to write to the output stream \a os.
/// \return The output stream \a os.
inline std::ostream& operator<<(std::ostream &os, const Rotation& r) {
	return os << '[' << r.axis.X << ' ' << r.axis.Y  << ' ' << r.axis.Z << "], " << r.angle;
}

/// \brief Writes the Rotation to a logging stream.
/// \param log The logging stream.
/// \param r The rotation to write to the logging stream \a log.
/// \return The logging stream \a log.
inline LoggerObject& operator<<(LoggerObject& log, const Rotation& r)
{
	return log.space() << "[" << r.axis.X << r.axis.Y << r.axis.Z << "]," << r.angle;
}

/// \brief Writes a Rotation to a binary output stream.
/// \param stream The output stream.
/// \param r The rotation to write to the output stream \a stream.
/// \return The output stream \a stream.
inline QDataStream& operator<<(QDataStream& stream, const Rotation& r)
{
	return stream << r.axis << r.angle;
}

/// \brief Reads a Rotation from a binary input stream.
/// \param stream The input stream.
/// \param r Reference to a rotation variable where the parsed data will be stored.
/// \return The input stream \a stream.
inline QDataStream& operator>>(QDataStream& stream, Rotation& r)
{
	return stream >> r.axis >> r.angle;
}

};	// End of namespace Base

Q_DECLARE_METATYPE(Base::Rotation)
Q_DECLARE_TYPEINFO(Base::Rotation, Q_PRIMITIVE_TYPE);

// We don't need versioning info for this type when serializing.
BOOST_CLASS_IMPLEMENTATION(Base::Rotation, boost::serialization::object_serializable)

#endif // __ROTATION_H
