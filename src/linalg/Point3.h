///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
/** 
 * \file Point3.h 
 * \brief Contains definition of the Base::Point_3 template class and operators. 
 */

#ifndef __POINT3_H
#define __POINT3_H

#include <Base.h>
#include "Vector3.h"

namespace Base {

// Empty tag class.
class Origin {};
// This dummy instance should be passed to the Point_3/Point_2 constructor to initialize it to the origin.
extern Origin ORIGIN;

/**
 * \brief A point in three-dimensional space.
 * 
 * This is one of the basic vector algebra classes. It represents a three
 * dimensional point in space defined by the coordinates X, Y and Z. 
 * There are two instances of this template point class: \c Point3 is for floating-point points 
 * and \c Point3I is for integer points with three components.
 * 
 * Note that there is also a class called Vector_3 that is used for vector
 * in a three dimensional coordinate system.
 * 
 * \author Alexander Stukowski
 * \sa Vector_3
 * \sa Point_2
 */
template<typename T> 
class Point_3
{
public:	
	/// \brief The X coordinate of the point.
	T X;
	/// \brief The Y coordinate of the point.
	T Y;
	/// \brief The Z coordinate of the point.
	T Z;

	/////////////////////////////// Constructors /////////////////////////////////

	/// \brief Constructs a point without initializing its coordinates.
	/// \note All coordinates are left uninitialized by this constructor and will therefore have a random value!
	Point_3() {}

	/// \brief Constructs a point with all coordinates set to the given value. 
	/// \param val The value to be assigned to each of the points's coordinates.
	inline Point_3(T val) { X = Y = Z = val; }

	/// \brief Initializes the coordinates of the point with the given values.
	/// \param x The X coordinate of the new point.
	/// \param y The Y coordinate of the new point.
	/// \param z The Z coordinate of the new point.
	inline Point_3(T x, T y, T z) : X(x), Y(y), Z(z) {}

	/// \brief Initializes the coordinates of the point with the values in the given array.
	/// \param val An array of three coordinates.
	inline Point_3(T val[3]) : X(val[0]), Y(val[1]), Z(val[2]) {}

	/// \brief Initializes the point to the origin. All coordinates are set to zero.
	/// \param ORIGIN A dummy parameter to distinguish this overloaded constructor from the others. 
	///        When using this constructor, just use the special value \c ORIGIN here.
	inline Point_3(Origin ORIGIN) : X((T)0), Y((T)0), Z((T)0) {}

    ///////////////////////////// Component access ///////////////////////////////

	/// \brief Returns a reference to the i-th coordinate of the point.
	/// \param i The index specifying the coordinate to return (0=X, 1=Y, 2=Z).
	/// \return A reference to the i-th coordinate that can be used to change the coordinate's value. 
	inline T& operator[](size_t i) {
		MAFEM_STATIC_ASSERT(sizeof(Point_3<T>) == sizeof(T) * 3);
		MAFEM_ASSERT(i>=0 && i<size());
		return (&X)[i];
	}

	/// \brief Returns a constant reference to the i-th coordinate of the point.
	/// \param i The index specifying the coordinate to return (0=X, 1=Y, 2=Z).
	/// \return The value of the i-th coordinate. 
	inline const T& operator[](size_t i) const {
		MAFEM_STATIC_ASSERT(sizeof(Point_3<T>) == sizeof(T) * 3);
		MAFEM_ASSERT(i>=0 && i<size());
		return (&X)[i]; 
	}

	/// \brief Returns a pointer to the first coordinate (X) of the point.
	/// \return A pointer to three consecutive floating-point numbers: X, Y and Z.
	/// \sa constData() 
	inline T* data() { 		
        MAFEM_STATIC_ASSERT(sizeof(Point_3<T>) == sizeof(T) * 3);
		return (T*)this;
	}

	/// \brief Returns a pointer to the first coordinate of the point for read-only access.
	/// \return A pointer to three consecutive floating-point numbers: X, Y and Z.
	/// \sa data()
	inline const T* constData() const {
        MAFEM_STATIC_ASSERT(sizeof(Point_3<T>) == sizeof(T) * 3);
		return (const T*)this;
	}

	/// \brief Casts the point to another point type.
	template<typename T2>
	operator Point_3<T2>() const { return Point_3<T2>((T2)X, (T2)Y, (T2)Z); }

	///////////////////////////// Assignment operators ///////////////////////////

	/// \brief Adds a vector to this point and stores the result in this point.
	/// \param v The vector to add to this point.
	/// \return A reference to \c this point, which has been changed.
	inline Point_3<T>& operator+=(const Vector_3<T>& v) { X += v.X; Y += v.Y; Z += v.Z; return *this; }

	/// \brief Substracts a vector from this point and stores the result in this point.
	/// \param v The vector to substract from this point.
	/// \return A reference to \c this point, which has been changed.
	inline Point_3<T>& operator-=(const Vector_3<T>& v) { X -= v.X; Y -= v.Y; Z -= v.Z; return *this; }

	//////////////////////////// Coordinate read access //////////////////////////

	/// Returns the X coordinate of the point.
	const T& x() const { return X; }

	/// Returns the Y coordinate of the point.
	const T& y() const { return Y; }

	/// Returns the Z coordinate of the point.
	const T& z() const { return Z; }
	
	//////////////////////////// Coordinate write access //////////////////////////

	/// \brief Sets the X coordinate of this point to a new value.
	/// \param value The new value that is assigned to the point coordinate.
	void setx(const T& value) { X = value; }

	/// \brief Sets the Y coordinate of this point to a new value.
	/// \param value The new value that is assigned to the point coordinate.
	void sety(const T& value) { Y = value; }

	/// \brief Sets the Z coordinate of this point to a new value.
	/// \param value The new value that is assigned to the point coordinate.
	void setz(const T& value) { Z = value; }	

	////////////////////////////////// Comparison ////////////////////////////////

	/// \brief Compares two points for equality.
	/// \param p The point this point should be compared to.
	/// \return \c true if each of the coordinates are equal; \c false otherwise.
	inline bool operator==(const Point_3<T>& p) const { return (p.X==X) && (p.Y==Y) && (p.Z==Z); }

	/// \brief Compares two points for inequality.
	/// \param p The point this point should be compared to.
	/// \return \c true if any of the coordinates are not equal; \c false if all are equal.
	inline bool operator!=(const Point_3<T>& p) const { return (p.X!=X) || (p.Y!=Y) || (p.Z!=Z); }

	/// \brief Checks whether the point is the origin, i.e. all coordinates are zero.
	/// \param ORIGIN Just use the special value \c ORIGIN here.
	/// \return \c true if all of the null vector are zero; \c false otherwise
	inline bool operator==(const Origin& ORIGIN) const { return (X==(T)0) && (Y==(T)0) && (Z==(T)0); }

	/// \brief Checks whether the point is not the origin, i.e. any of the coordinates is nonzero.
	/// \param ORIGIN Just use the special value \c ORIGIN here.
	/// \return \c true if any of the coordinates is nonzero; \c false if this is the null vector otherwise
	inline bool operator!=(const Origin& ORIGIN) const { return (X!=(T)0) || (Y!=(T)0) || (Z!=(T)0); }

	/// \brief Checks whether two points are equal within a given tolerance.
	/// \param p The point that should be compared to this point.
	/// \param tolerance A non-negative threshold for the equality test. The two points are considered equal when 
	///        the differences in the X, Y, and Z directions are all smaller than this tolerance value.
	/// \return \c true if this point is equal to the given point \a p within the given tolerance.
	inline bool equals(const Point_3<T>& p, T tolerance) const { 
		return (abs(p.X - X) <= tolerance) && (abs(p.Y - Y) <= tolerance) && (abs(p.Z - Z) <= tolerance); 
	}

	/////////////////////////////// Binary operators /////////////////////////////

	/// \brief Computes the sum of two points.
	/// \param p The second operand.
	/// \return The sum of two points.
	inline Point_3<T> operator+(const Point_3<T>& p) const { return Point_3<T>(X + p.X, Y + p.Y, Z + p.Z); }

	/// \brief Computes the sum of a point and a vector.
	/// \param v The second operand.
	/// \return The new point.
	inline Point_3<T> operator+(const Vector_3<T>& v) const { return Point_3<T>(X + v.X, Y + v.Y, Z + v.Z); }

	/// \brief Computes the difference of two points.
	/// \param p The second operand.
	/// \return The vector connecting the two points.	
	inline Vector_3<T> operator-(const Point_3<T>& p) const { return Vector_3<T>(X - p.X, Y - p.Y, Z - p.Z); }

	/// \brief Substracts a vector from a point.
	/// \param v The second operand.
	/// \return The new point.
	inline Point_3<T> operator-(const Vector_3<T>& v) const { return Point_3<T>(X - v.X, Y - v.Y, Z - v.Z); }

	/// \brief Converts the point to a vector.
	/// \param ORIGIN Just use the special value \c ORIGIN here.
	/// \return A vector with its components equal to the coordinates of this point. 
	inline const Vector_3<T>& operator-(Origin ORIGIN) const { 
		MAFEM_STATIC_ASSERT(sizeof(Point_3<T>) == sizeof(Vector_3<T>));
		return *(Vector_3<T>*)this; 
	}

	/// \brief Multiplies all coordinates of a point with a scalar.
	/// \param s The scalar value.
	/// \return A new point with the scaled coordinates.
	inline Point_3<T> operator*(T s) const { return Point_3<T>(X*s, Y*s, Z*s); }

	/// \brief Divides all coordinates of a point by a scalar.
	/// \param s The scalar value.
	/// \return A new point with the scaled coordinates.
	inline Point_3<T> operator/(T s) const { return Point_3<T>(X/s, Y/s, Z/s); }

	///////////////////////////////// Information ////////////////////////////////
	
	/// \brief Returns the number of coordinates of this point.
	/// \return Always returns 3.
	inline size_t size() const { return 3; }

	/// \brief Returns the number of dimensions of this point.
	/// \return Always returns 3.
	inline size_t dimension() const { return 3; }

	/// \brief Gives a string representation of this point.
	/// \return A string that contains the coordinates of the point. 
	QString toString() const { return QString("(%1 %2 %3)").arg(X).arg(Y).arg(Z); }

private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) { ar & X & Y & Z; }	

	friend class boost::serialization::access;
};

/// \brief Converts a vector to a point.
/// \param ORIGIN Just use the special value \c ORIGIN here.
/// \param v The vector to convert.
/// \return A point with its coordinates equal to the components of the vector \a v. 
template<typename T>
inline Point_3<T> operator+(const Origin& ORIGIN, const Vector_3<T>& v) { 
	return Point_3<T>(v.X, v.Y, v.Z); 
}

/// \brief Calculates the squared distance between two points.
/// \param a The first point.
/// \param b The second point.
/// \return The squared distance between \a a and \a b.
template<typename T>
inline T DistanceSquared(const Point_3<T>& a, const Point_3<T>& b) {
	return square(a.X - b.X) + square(a.Y - b.Y) + square(a.Z - b.Z);
}

/// \brief Calculates the distance between two points.
/// \param a The first point.
/// \param b The second point.
/// \return The distance between \a a and \a b.
template<typename T>
inline T Distance(const Point_3<T>& a, const Point_3<T>& b) {
	return (T)sqrt(DistanceSquared(a, b));
}

/// Returns the index of the coordinate with the maximum value.
/// Post-Condition: 0 <= Return Value < 3
template<typename T>
inline size_t MaxComponent(const Point_3<T>& a) {
    return ((a.X >= a.Y) ? ((a.X >= a.Z) ? 0 : 2) : ((a.Y >= a.Z) ? 1 : 2));	
}

/// Returns the index of the coordinate with the minimum value.
/// Post-Condition: 0 <= Return Value < 3
template<typename T>
inline size_t MinComponent(const Point_3<T>& a) {
    return ((a.X <= a.Y) ? ((a.X <= a.Z) ? 0 : 2) : ((a.Y <= a.Z) ? 1 : 2));
}

/// Returns the index of the component with the maximum absolute value.
/// Post-Condition: 0 <= Return Value < 3
template<typename T>
inline size_t MaxAbsComponent(const Point_3<T>& a) {
    return ((abs(a.X) >= abs(a.Y)) ? ((abs(a.X) >= abs(a.Z)) ? 0 : 2) : ((abs(a.Y) >= abs(a.Z)) ? 1 : 2));	
}

/// \brief Writes the point to a text output stream.
/// \param os The output stream.
/// \param p The point to write to the output stream \a os.
/// \return The output stream \a os.
template<typename T>
inline std::ostream& operator<<(std::ostream &os, const Point_3<T> &p) {
	return os << p.X << ' ' << p.Y  << ' ' << p.Z;    
}

/// \brief Writes the point to a logging stream.
/// \param log The logging stream.
/// \param p The point to write to the logging stream \a log.
/// \return The logging stream \a log.
template<typename T>
inline LoggerObject& operator<<(LoggerObject& log, const Point_3<T>& p)
{
     log.space() << "(" << p.X << p.Y << p.Z << ")";
     return log.space();
} 

/// \brief Writes the point to a binary output stream.
/// \param stream The output stream.
/// \param p The point to write to the output stream \a stream.
/// \return The output stream \a stream.
template<typename T>
inline QDataStream& operator<<(QDataStream& stream, const Point_3<T>& p)
{
	return stream << p.X << p.Y << p.Z;
}

/// \brief Reads a point from a binary input stream.
/// \param stream The input stream.
/// \param p Reference to a point variable where the parsed data will be stored.
/// \return The input stream \a stream.
template<typename T>
inline QDataStream& operator>>(QDataStream& stream, Point_3<T>& p)
{
	return stream >> p.X >> p.Y >> p.Z;
}

template class Point_3<FloatType>;
template class Point_3<int>;

/** 
 * \fn typedef Point3
 * \brief Template class instance of the Point_3 class used for floating-point points. 
 */
typedef Point_3<FloatType>	Point3;

/** 
 * \fn typedef Point3I
 * \brief Template class instance of the Point_3 class used for integer points. 
 */
typedef Point_3<int>		Point3I;


/// \brief Computes the hash value for a point.
template<typename T>
std::size_t hash_value(const Point_3<T>& p)
{
	std::size_t seed = 0;
	boost::hash_combine(seed, p.X);
	boost::hash_combine(seed, p.Y);
	boost::hash_combine(seed, p.Z);
	return seed;
}

};	// End of namespace Base

Q_DECLARE_METATYPE(Base::Point3);
Q_DECLARE_METATYPE(Base::Point3I);
Q_DECLARE_TYPEINFO(Base::Point3, Q_PRIMITIVE_TYPE);
Q_DECLARE_TYPEINFO(Base::Point3I, Q_PRIMITIVE_TYPE);

// We don't need versioning info for this type when serializing.
BOOST_CLASS_IMPLEMENTATION(Base::Point3, boost::serialization::object_serializable)
BOOST_CLASS_IMPLEMENTATION(Base::Point3I, boost::serialization::object_serializable)


#endif // __POINT3_H
