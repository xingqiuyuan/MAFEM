///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
/** 
 * \file AffineDecomposition.h 
 * \brief Contains definition of Base::AffineDecomposition class. 
 */

#ifndef __AFFINE_DECOMP_H
#define __AFFINE_DECOMP_H

#include <Base.h>
#include "Vector3.h"
#include "Rotation.h"
#include "Scaling.h"
#include "AffineTransformation.h"

namespace Base {

/**
 * \brief Decomposes a matrix into translation, rotation and scaling parts.
 * 
 * A AffineTransformation matrix is decomposed in the following way:
 * 
 * M = T * F * R * S
 * 
 * with 
 * \li T - Translation
 * \li F - Sign of determinant
 * \li R - Rotation
 * \li S - Scaling  
 * 
 * The scaling matrix is spectrally decomposed into S = U * K * Transposed(U).
 * 
 * \note Decomposing a matrix into its affine parts is a slow operation and should only be done when really necessary.
 *
 * \author Alexander Stukowski
 * \sa AffineTransformation, Rotation, Scaling
 */
class AffineDecomposition
{
public:
	/// Translation part.
	Vector3 translation;

	/// Rotation part.
	Quaternion rotation;

	/// Scaling part.
	Scaling scaling;

	/// Sign of determinant (-1 or +1).
	FloatType sign;	

	/// \brief Constructor that decomposes the given matrix into its affine parts.
	///
	/// After the constructor has been called the components of the decomposed
	/// transformation can be accessed through the #translation, #rotation,
	/// #scaling and #sign member variables.
	AffineDecomposition(const AffineTransformation& tm);
};

};	// End of namespace Base

#endif // __AFFINE_DECOMP_H
