///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
/** 
 * \file Scaling.h 
 * \brief Contains definition of the Base::Scaling class and operators. 
 */
 
#ifndef __SCALING_H
#define __SCALING_H

#include <Base.h>
#include "Vector3.h"
#include "Quaternion.h"

namespace Base {

// Empty tag class.
class IdentityScaling {};
// This dummy instance should be passed to the Scaling class constructor to initialize it to the identity.
extern IdentityScaling IDENTITY_SCALING;

/**
 * \brief The Scaling structure describes an arbitrary non-uniform scaling in an arbitrary axis system.
 * 
 * The Vector3 #S sepcifies the scaling factors along the x, y, and z axes, and the quaternion #Q 
 * defines the axis system in which the scaling is applied.
 * 
 * \author Alexander Stukowski
 */
class Scaling
{
public:	
	/// \brief The scaling factors in x, y and z directions of the axis system specified by Q.
	Vector3 S;
	
	/// \brief The orientation of the axis system the scaling is applied in.
	Quaternion Q;

	/////////////////////////////// Constructors /////////////////////////////////

	/// \brief Constructs a Scaling object without initializing its components.
	/// \note The components are left uninitialized by this constructor and will therefore have a random value!
	Scaling() {}

	/// \brief Initializes a Scaling with the scaling factors and the coordinate system.
	/// \param scaling The scaling factors in x, y and z directions of the axis system specified by \a orientation.
	/// \param orientation The orientation of the axis system the scaling is applied in.
	Scaling(const Vector3& scaling, const Quaternion& orientation) : S(scaling), Q(orientation) {}

	/// \brief Initializes a Scaling with the scaling factors and the identity coordinate system.
	/// \param scaling The scaling factors in x, y and z directions of the identity coordinate system.
	Scaling(const Vector3& scaling) : S(scaling), Q(IDENTITY_QUAT) {}
	
	/// \brief Initializes the object to the identity.
	/// \param IDENTITY_SCALING A dummy parameter to distinguish this overloaded constructor from the others. 
	///        When using this constructor, just use the special value \c IDENTITY_SCALING here.
	/// 
	/// The Scaling structure is initialized with the scaling factors (1,1,1), i.e. no scaling at all.  
	Scaling(const IdentityScaling& IDENTITY_SCALING) : S(1,1,1), Q(IDENTITY_QUAT) {}

	/////////////////////////////// Unary operators //////////////////////////////
	
	/// \brief Returns the inverse of this scaling.
	/// \return The inverse scaling that exactly compensates this scaling.
	Scaling inverse() const {
		MAFEM_ASSERT_MSG(S.X != 0 && S.Y != 0 && S.Z, "Scaling::inverse()", "Cannot invert a singular scaling value.");
		return Scaling(Vector3(1.0 / S.X, 1.0 / S.Y, 1.0 / S.Z), Normalize(Q.inverse())); 
	}
	
	/////////////////////////////// Binary operators /////////////////////////////

	/// \brief Performs the multiplication of two scaling structures.
	/// \param s2 The second scaling.
	/// \return A scaling structure that is equal to first applying scaling \a s2 and then \c this scaling.
	Scaling operator*(const Scaling& s2) const;

	/// \brief Adds the given scaling to this scaling.
	/// \param s2 The scaling to add to this scaling.
	/// \return This resulting scaling which is equal to \c s2*(*this). 
	Scaling& operator+=(const Scaling& s2) { *this = s2 * (*this); return *this; }

	/// \brief Adds the inverse of another scaling to this scaling.
	/// \param s2 The scaling to substract from this scaling.
	/// \return This resulting scaling which is equal to \c (*this)*s2.inverse(). 
	Scaling& operator-=(const Scaling& s2) { *this = *this * s2.inverse(); return *this; }

	////////////////////////////////// Comparison ////////////////////////////////

	/// \brief Compares two scaling structures for equality.
	/// \param s The scaling to compare with.
	/// \return \c true if each of the components are equal; \c false otherwise.
	bool operator==(const Scaling& s) const { return (s.S==S) && (s.Q==Q); }

	/// \brief Compares two scaling structures for inequality.
	/// \param s The scaling to compare with.
	/// \return \c true if any of the components are not equal; \c false if all are equal.
	bool operator!=(const Scaling& s) const { return (s.S!=S) || (s.Q!=Q); }

	/// \brief Returns whether this is the identity.
	/// \param IDENTITY_SCALING Just use the special value \c IDENTITY_SCALING here.
	/// \return \c true if the scaling in each of the three spatial directions is 1; 
	///         \c false otherwise.
	bool operator==(const IdentityScaling& IDENTITY_SCALING) const { return (S == Vector3(1,1,1)); }

	/// \brief Returns whether this is not the identity.
	/// \param IDENTITY_SCALING Just use the special value \c IDENTITY_SCALING here.
	/// \return \c true if the scaling in any of the three spatial directions is not 1; 
	///         \c false otherwise.
	bool operator!=(const IdentityScaling& IDENTITY_SCALING) const { return (S != Vector3(1,1,1)); }

	///////////////////////////////// Interpolation //////////////////////////////

	/// \brief Computes a linear interpolation between two scaling structures.
	/// \param s1 The first scaling.
	/// \param s2 The second scaling.
	/// \param t The parameter for the linear interpolation in the range [0,1].
	/// \return A linear interpolation between \a s1 and \a s2.
    static Scaling interpolate(const Scaling& s1, const Scaling& s2, FloatType t);

    ////////////////////////////////// Utilities /////////////////////////////////

	/// Returns a string representation of this scaling.
	QString toString() const { return QString("[Scaling: %1 Orientation: %2]").arg(S.toString(), Q.toString()); }

private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) { ar & S & Q; }	

	friend class boost::serialization::access;
};

/// \brief Writes the Scaling to a text output stream.
/// \param os The output stream.
/// \param s The scaling to write to the output stream \a os.
/// \return The output stream \a os.
inline std::ostream& operator<<(std::ostream &os, const Scaling& s) {
	return os << '[' << s.S << "], " << s.Q;
}

/// \brief Writes the Scaling to a logging stream.
/// \param log The logging stream.
/// \param s The scaling to write to the logging stream \a log.
/// \return The logging stream \a log.
inline LoggerObject& operator<<(LoggerObject& log, const Scaling& s)
{
	return log.space() << "[" << s.S << "]," << s.Q;
}

/// \brief Writes a Scaling to a binary output stream.
/// \param stream The output stream.
/// \param s The scaling to write to the output stream \a stream.
/// \return The output stream \a stream.
inline QDataStream& operator<<(QDataStream& stream, const Scaling& s)
{
	return stream << s.S << s.Q;
}

/// \brief Reads a Scaling from a binary input stream.
/// \param stream The input stream.
/// \param s Reference to a scaling variable where the parsed data will be stored.
/// \return The input stream \a stream.
inline QDataStream& operator>>(QDataStream& stream, Scaling& s)
{
	return stream >> s.S >> s.Q;
}

};	// End of namespace Base

Q_DECLARE_METATYPE(Base::Scaling)
Q_DECLARE_TYPEINFO(Base::Scaling, Q_PRIMITIVE_TYPE);

// We don't need versioning info for this type when serializing.
BOOST_CLASS_IMPLEMENTATION(Base::Scaling, boost::serialization::object_serializable)

#endif // __SCALING_H
