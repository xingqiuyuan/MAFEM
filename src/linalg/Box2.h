///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
/** 
 * \file Box2.h 
 * \brief Contains the definition of the Base::Box_2 template class. 
 */

#ifndef __BOX2_H
#define __BOX2_H

#include <Base.h>
#include "Vector2.h"
#include "Point2.h"

namespace Base {

/**
 * \brief A bounding box in 2d space.
 *
 * This class stores an axis-aligned box in 2d.
 * It is defined by minimum and maximum coordinates in X and Y direction.
 * 
 * \author Alexander Stukowski
 * \sa Box_3
 */
template<typename T>
class Box_2
{
public:	
	/// The coordinates of the lower left corner.
	Point_2<T> minc;
	/// The coordinates of the upper right corner.
	Point_2<T> maxc;

	/////////////////////////////// Constructors /////////////////////////////////

	/// \brief Creates an empty box.
	Box_2() : minc(numeric_limits<T>::max()), maxc(-numeric_limits<T>::max()) {}

	/// \brief Initializes the box with the minimum and maximum coordinates.
	/// \param minCorner A point that specifies the corner with minimum coordinates of the box.
	/// \param maxCorner A point that specifies the corner with maximum coordinates of the box.	
	Box_2(const Point_2<T>& minCorner, const Point_2<T>& maxCorner) : minc(minCorner), maxc(maxCorner) {
		MAFEM_ASSERT_MSG(minc.X <= maxc.X, "Box_3 constructor", "X component of the minimum corner point must not be larger than the maximum corner point.");
		MAFEM_ASSERT_MSG(minc.Y <= maxc.Y, "Box_3 constructor", "Y component of the minimum corner point must not be larger than the maximum corner point.");
	}

	/// \brief Initializes the box with the given coordinates.
	Box_2(T xmin, T ymin, T xmax, T ymax) : minc(Point_2<T>(xmin, ymin)), maxc(Point_2<T>(xmax, ymax)) {
		MAFEM_ASSERT(minc.X <= maxc.X);
		MAFEM_ASSERT(minc.Y <= maxc.Y);
	}

	///////////////////////////////// Attributes /////////////////////////////////

	/// \brief Checks whether this is an empty box.
	///
	/// The box is considered empty when one of the maximum corner coodinates is less
	/// then the minimum corner coordinate.
	/// \return true if this box is empty; false otherwise.
	bool isEmpty() const {
        return (minc.X > maxc.X) || (minc.Y > maxc.Y);
	}

	/// \brief Resets the box to the empty state.
	void setEmpty() {
		minc = Point_2<T>( numeric_limits<T>::max());
		maxc = Point_2<T>(-numeric_limits<T>::max());
	}

	/// \brief Returns the position of one of the four corners of the box corner.
	/// \param i The index of the corner (0 - 3).
	/// \return The coordinate of the i-th corner of the box.
	Point_2<T> operator[](size_t i) const {
		switch(i) {
			case 0: return Point_2<T>(minc.X, minc.Y);
			case 1: return Point_2<T>(maxc.X, minc.Y);
			case 2: return Point_2<T>(maxc.X, maxc.Y);
			case 3: return Point_2<T>(minc.X, maxc.Y);
			default:				
				MAFEM_ASSERT_MSG(false, "Box2::operator[]", "Corner index out of range.");
				throw std::invalid_argument("Corner index out of range.");
				return ORIGIN;
		}
	}

	/// \brief Computes the width of the box.
	T width() const { return maxc.X - minc.X; }

	/// \brief Computes the height of the box.
	T height() const { return maxc.Y - minc.Y; }
	
	/// \brief Computes the center of the box.
	/// \return The center of the box.
	Point_2<T> center() const {
		return Point_2<T>((minc.X + maxc.X) / 2, (minc.Y + maxc.Y) / 2);
	}

	/// \brief Computes the size of the box.
	/// \return The difference between the maximum and minimum corner.
	/// \sa width(), height()
	Vector_2<T> size() const {
		return maxc - minc;
	}
	
	/// \brief Returns the size of the box in the given dimension.
	/// \param dimension The index of the dimension (0 or 1).
	/// \sa size(), width(), height()
	T size(size_t dimension) const { 
		return maxc[dimension] - minc[dimension]; 
	}

	/////////////////////////////// Classification ///////////////////////////////
    
	/// \brief Checks whether a point is inside the box.
	/// \param p The point to test.
	/// \return true if the given point is inside or on the edge of the bounding box; false if it is completely outside the box.
	bool contains(const Point_2<T>& p) const {
		if(p.X < minc.X || p.X > maxc.X) return false;
		if(p.Y < minc.Y || p.Y > maxc.Y) return false;
		return true;
	}

    //////////////////////////////// Modification ////////////////////////////////

	/// \brief Enlarges the box to include the given point.
	/// \sa addPoints(), addBox()
	void addPoint(const Point_2<T>& p) {
		minc.X = min(minc.X, p.X); maxc.X = max(maxc.X, p.X);
		minc.Y = min(minc.Y, p.Y); maxc.Y = max(maxc.Y, p.Y);
	}

	/// \brief Enlarges the box to include the given point.
	/// \sa addPoints(), addBox()
	void addPoint(T x, T y) { 
		minc.X = min(minc.X, x); maxc.X = max(maxc.X, x);
		minc.Y = min(minc.Y, y); maxc.Y = max(maxc.Y, y);
	}

	/// \brief Enlarges the box to include the given point.
	/// \sa addPoint()
	Box_2& operator+=(const Point_2<T>& p) {
		addPoint(p);
		return *this;
	}

	/// \brief Enlarges the box to include the given points.
	/// \param points Pointer to the first element of an array of points.
	/// \param count The number of points in the array.
	/// \sa addPoint()
	void addPoints(const Point_2<T>* points, size_t count) {
		for(; count != 0; count--, points++) {
			minc.X = min(minc.X, points->X); maxc.X = max(maxc.X, points->X);
			minc.Y = min(minc.Y, points->Y); maxc.Y = max(maxc.Y, points->Y);
		}
	}

	/// \brief Enlarges this box to include the given box.
	/// \sa addPoint()
	void addBox(const Box_2& b) {
		minc.X = min(minc.X, b.minc.X); maxc.X = max(maxc.X, b.maxc.X);
		minc.Y = min(minc.Y, b.minc.Y); maxc.Y = max(maxc.Y, b.maxc.Y);
	}

	/// \brief Enlarges the box to include the given x coordindate.
    void includeX(T x) {
        minc.X = min(minc.X, x); maxc.X = max(maxc.X, x);
	}

	/// \brief Enlarges the box to include the given y coordindate.
    void includeY(T y) {
        minc.Y = min(minc.Y, y); maxc.Y = max(maxc.Y, y);
	}

    ////////////////////////////////// Utilities /////////////////////////////////

	/// \brief Returns a string representation of this box.
	QString toString() const { return QString("[Min: %1 Max: %2]").arg(minc.toString()).arg(maxc.toString()); }	
};

/// \brief Writes the box to a logging stream.
template<typename T>
inline LoggerObject& operator<<(LoggerObject& log, const Box_2<T>& b)
{
	log.nospace() << '[' << b.minc << "] - [" << b.maxc << ']';
	return log;
} 

/// \brief Writes a 2d box to a binary output stream.
template<typename T>
inline QDataStream& operator<<(QDataStream& stream, const Box_2<T>& b)
{
	return stream << b.minc << b.maxc;
}

/// \brief Reads a 2d box from a binary input stream.
template<typename T>
inline QDataStream& operator>>(QDataStream& stream, Box_2<T>& b)
{
	return stream >> b.minc >> b.maxc;
}

template class Box_2<FloatType>;
template class Box_2<int>;

/** 
 * \fn typedef Box2
 * \brief Template class instance of the Box_2 class used for floating-point calculations based on Point2. 
 */
typedef Box_2<FloatType>	Box2;

/** 
 * \fn typedef Box2I
 * \brief Template class instance of the Box_2 class used for integer calculations based on Point2I. 
 */
typedef Box_2<int>			Box2I;

};	// End of namespace Base

Q_DECLARE_METATYPE(Base::Box2)
Q_DECLARE_METATYPE(Base::Box2I)
Q_DECLARE_TYPEINFO(Base::Box2, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(Base::Box2I, Q_MOVABLE_TYPE);

#endif // __BOX2_H
