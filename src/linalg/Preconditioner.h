///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_PRECONDITIONER_H
#define __MAFEM_PRECONDITIONER_H

#include <MAFEM.h>
#include "Cholesky.h"

namespace MAFEM {

/******************************************************************************
* Defines template preconditioner classes used by the CG solver.
******************************************************************************/

/******************************************************************************
* Do-nothing preconditioner.
******************************************************************************/
template<class MATRIX>
class IdentityPreconditioner 
{
public:
	IdentityPreconditioner(const MATRIX& A) {}

	template<class Vector>
	void apply(const Vector& b, Vector& x) const { noalias(x) = b; }
private:
	IdentityPreconditioner() {}
};

/******************************************************************************
* Scales x with diagonal of a given matrix.
******************************************************************************/
template<class Matrix> 
class DiagonalPreconditioner 
{
public:
	typedef typename Matrix::value_type value_type;
	typedef typename Matrix::size_type size_type;

	DiagonalPreconditioner(const Matrix & A, const FloatType eps = FLOATTYPE_EPSILON) : diag(A.size1()), EPS(eps) { 
		for(size_type i=0; i<A.size1(); i++) {
			if(abs(A(i,i)) > EPS)
				diag(i) = 1.0 / A(i,i);
			else
				diag(i) = 0;
		}
	}

	template<class Vector>
	void apply(const Vector& b, Vector& x) const {
		noalias(x) = element_prod(diag, b);
	}

private:
	DiagonalPreconditioner() {};
	ublas::vector<value_type> diag;
	const FloatType EPS;
};

/******************************************************************************
* Decomposes given matrix and solve L L^T x = b
* Note: L is a lower triangular matrix computed by a fast 
*       incomplete cholesky decomposition. L has the same
*       sparsity pattern as A and a full diagonal.
******************************************************************************/
template<class Matrix> 
class IncompleteCholeskyPreconditioner 
{
public:
	IncompleteCholeskyPreconditioner(const Matrix & A) : L(A) { 
		incomplete_cholesky_decompose(L);
	}

	template<class Vector>
	void apply(const Vector& b, Vector& x) const {
		noalias(x) = b;
		cholesky_solve(L, x, ublas::lower());
	}

private:
	IncompleteCholeskyPreconditioner() {}
	Matrix L;
};

}; // End of namespace MAFEM

#endif		// __MAFEM_PRECONDITIONER_H
