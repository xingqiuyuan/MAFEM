///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
/** 
 * \file Ray.h 
 * \brief Contains definition of the Base::Ray3 template class. 
 */

#ifndef __RAY_H
#define __RAY_H

#include <Base.h>
#include "Vector3.h"
#include "Point3.h"
#include "AffineTransformation.h"

namespace Base {

/**
 * \brief Infinate ray in 3d space.
 * 
 * A ray is defined by a base point and a direction vector.
 * 
 * \author Alexander Stukowski 
 */
class Ray3
{
public:	
	/// A base point on the ray.
	Point3 base;
	
	/// The direction vector.
	Vector3 dir;

	/////////////////////////////// Constructors /////////////////////////////////

	/// \brief This empty default constructor does not initialize the components!
	/// \note All components of the ray are left uninitialized by this constructor and 
	///       will therefore have a random value!
	Ray3() {}

	/// \brief Initializes the ray with a base point and a direction vector.
	/// \param b A point through which the ray passes.
	/// \param d A direction vector. This vector should have length 1.
	Ray3(const Point3& b, const Vector3& d) : base(b), dir(d) {}

	/// \brief Initializes the ray from two points.
	/// \param a The first point on the ray
	/// \param b The second point on the ray.
	///
	/// The direction vector of the ray is initialized to the vector pointing from
	/// point \a a to point \a b.
	Ray3(const Point3& a, const Point3& b) : base(a), dir(b - a) {}
	
	////////////////////////////////// Queries ///////////////////////////////////
	
	/// \brief Returns a point on the ray at the given parameter position.
	/// \param t The parameter that specifies the position on the ray
	///        starting at the base point and going into the direction specified
	///        by the ray vector.
	/// \return The point base + dir * t.
	Point3 point(FloatType t) const { return base + dir * t; }

    /////////////////////////////// Unary operators //////////////////////////////

	/// \brief Flips the ray direction.
	/// \return A new ray with reversed direction vector.
	Ray3 operator-() const { return(Ray3(base, -dir)); } 

    ////////////////////////////////// Utilities /////////////////////////////////

	/// \brief Returns a string representation of this ray.
	QString toString() const { return QString("[Base: %1 Dir: %2]").arg(base.toString(), dir.toString()); }

private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) { ar & base & dir; }	

	friend class boost::serialization::access;
};

/// \brief Transforms a ray.
/// \param tm The transformation matrix.
/// \param ray The ray to be transformed.
/// \return A new ray with transformed base point and direction vector.
///         The direction is normalized after the transformation.
inline Ray3 operator*(const AffineTransformation& tm, const Ray3& ray) {
	return Ray3(tm * ray.base, Normalize(tm * ray.dir));
}

/// \brief Writes the ray to an output stream.
/// \param os The output stream.
/// \param p The ray to write to the output stream \a os.
/// \return The output stream \a os.
inline std::ostream& operator<<(std::ostream &os, const Ray3 &r) {
	return os << '[' << r.base.X << ' ' << r.base.Y  << ' ' << r.base.Z << "], (" << r.dir.X << ' ' << r.dir.Y  << ' ' << r.dir.Z << ')';
}

/// \brief Writes the ray to a logging stream.
/// \param log The logging stream.
/// \param p The ray to write to the logging stream \a log.
/// \return The logging stream \a log.
inline LoggerObject& operator<<(LoggerObject& l, const Ray3 &r) {
	l.nospace() << '[' << r.base.X << ' ' << r.base.Y  << ' ' << r.base.Z << "], (" << r.dir.X << ' ' << r.dir.Y  << ' ' << r.dir.Z << ')';
	return l;
}

/// \brief Writes the ray to a binary output stream.
/// \param stream The output stream.
/// \param p The ray to write to the output stream \a stream.
/// \return The output stream \a stream.
inline QDataStream& operator<<(QDataStream& stream, const Ray3& r)
{
	return stream << r.base << r.dir;
}

/// \brief Reads a ray from a binary input stream.
/// \param stream The input stream.
/// \param p Reference to a ray variable where the parsed data will be stored.
/// \return The input stream \a stream.
inline QDataStream& operator>>(QDataStream& stream, Ray3& r)
{
	return stream >> r.base >> r.dir;
}

};	// End of namespace Base

Q_DECLARE_METATYPE(Base::Ray3)
Q_DECLARE_TYPEINFO(Base::Ray3, Q_PRIMITIVE_TYPE);

// We don't need versioning info for this type when serializing.
BOOST_CLASS_IMPLEMENTATION(Base::Ray3, boost::serialization::object_serializable)

#endif // __RAY_H
