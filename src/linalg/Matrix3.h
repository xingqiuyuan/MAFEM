///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
/** 
 * \file Matrix3.h 
 * \brief Contains definition of the Base::Matrix3 class and operators. 
 */
 
#ifndef __MATRIX3_H
#define __MATRIX3_H

#include <Base.h>
#include <utilities/Exception.h>
#include "Vector3.h"
#include "Point3.h"
#include "Quaternion.h"
#include "Scaling.h"
#include "Rotation.h"

namespace Base {

// Empty tag class.
class IdentityMatrix {};
// This dummy instance should be passed to the AffineTransformation class constructor to initialize it to the identity matrix.
extern IdentityMatrix IDENTITY;

// Empty tag class.
class NullMatrix {};
// This dummy instance should be passed to the AffineTransformation class constructor to initialize it to the null matrix.
extern NullMatrix NULL_MATRIX;

/**
 * \brief A 3x3 matrix class.
 *
 * This class stores an array of 3 times 3 floating-point value. 
 * It is therefore a matrix with 3 rows and 3 columns.
 * 
 * Such a matrix is used to describe affine transformations in 3d space
 * like rotation, shearing and scaling.
 * 
 * In contrast to the AffineTransformation matrix class this Matrix3 class
 * cannot store a translation component.
 * 
 * \author Alexander Stukowski
 * \sa AffineTransformation, Matrix4
 */
class Matrix3
{
private:
	/// The 3 x 3 elements of the matrix.
	/// Elements are stored in column-major order, i.e. the first 
	/// array index specifies the columns and the second index the row.
	FloatType m[3][3];

public:

	/// \brief Constructs a matrix without initializing its elements.
	/// \note All elements are left uninitialized by this constructor and have therefore a random value!
	Matrix3() {}

	/// \brief Copy constructor that initializes the matrix elements from the elements of another matrix.
	Matrix3(const Matrix3& mat) { 
		memcpy(m, mat.m, sizeof(m)); 
	}

	/// \brief Constructor that initializes all 9 elements of the matrix to the given values.
	/// \note Values are given in row-major order, i.e. row by row.
	Matrix3(FloatType m11, FloatType m12, FloatType m13,
			FloatType m21, FloatType m22, FloatType m23,
			FloatType m31, FloatType m32, FloatType m33)
	{
		m[0][0] = m11; m[0][1] = m21; m[0][2] = m31;
		m[1][0] = m12; m[1][1] = m22; m[1][2] = m32;
		m[2][0] = m13; m[2][1] = m23; m[2][2] = m33;
	}

	/// \brief Initializes the matrix to the null matrix.
	/// All matrix elements are set to zero by this constructor.
	Matrix3(const NullMatrix& NULL_MATRIX) {
		memset(m, 0, sizeof(m));
	}

	/// \brief Initializes the matrix to the identity matrix.
	/// All diagonal elements are set to one and all off-diagonal elements are set to zero.
	Matrix3(const IdentityMatrix& IDENTITY) {
		m[0][0] = 1.0; m[0][1] = 0.0; m[0][2] = 0.0;
		m[1][0] = 0.0; m[1][1] = 1.0; m[1][2] = 0.0;
		m[2][0] = 0.0; m[2][1] = 0.0; m[2][2] = 1.0;
	}

	/// \brief Returns the value of a matrix element.
	/// \param row The row of the element to return (0-2). 
	/// \param col The column of the element to return (0-2).
	/// \return The value of the matrix element.  
	FloatType operator()(size_t row, size_t col) const { 
		MAFEM_ASSERT_MSG(row>=0 && row<3, "Matrix3", "Row index out of range");
		MAFEM_ASSERT_MSG(col>=0 && col<3, "Matrix3", "Column index out of range");
		return m[col][row];
	}

	/// \brief Returns a reference to a matrix element.
	/// \param row The row of the element to return (0-2). 
	/// \param col The column of the element to return (0-2).
	FloatType& operator()(size_t row, size_t col) { 
		MAFEM_ASSERT_MSG(row>=0 && row<3, "Matrix3", "Row index out of range");
		MAFEM_ASSERT_MSG(col>=0 && col<3, "Matrix3", "Column index out of range");
		return m[col][row];
	}

	/// \brief Returns a column from the matrix.
	/// \param i The column to return (0-2).
	/// \return The i-th column of the matrix as a vector.
	const Vector3& column(size_t i) const { 
		MAFEM_ASSERT_MSG(i>=0 && i<3, "Matrix3::column()", "Column index out of range.");
		MAFEM_STATIC_ASSERT(sizeof(m) == sizeof(Vector3)*3);
		return *(const Vector3*)m[i];
	}

	/// \brief Returns a reference to a column in the matrix.
	/// \param i The column to return (0-2).
	/// \return The i-th column of the matrix as a vector reference. Modifying the vector modifies the matrix.
	Vector3& column(size_t i) {
		MAFEM_ASSERT_MSG(i>=0 && i<3, "Matrix3::column()", "Column index out of range.");
		MAFEM_STATIC_ASSERT(sizeof(m) == sizeof(Vector3)*3);
		return *(Vector3*)m[i];
	}
	
	/// \brief Returns a column from the matrix.
	/// \param i The column to return (0-2).
	/// \return The i-th column of the matrix as a vector.
	const Vector3& getColumn(size_t i) const { return column(i); }
	
	/// \brief Sets all elements in one column of the matrix.
	/// \param i The column to set (0-2).
	/// \param c The new element values as a vector.
	void setColumn(size_t i, const Vector3& c) { column(i) = c; }	

	/// \brief Returns a row from the matrix.
	/// \param i The row to return (0-2).
	/// \return The i-th row of the matrix as a vector.
	Vector3 row(size_t i) const {
		MAFEM_ASSERT_MSG(i>=0 && i<3, "Matrix3::row()", "Row index out of range.");
		return Vector3(m[0][i], m[1][i], m[2][i]);
	}

	/// \brief Sets all elements in one row of the matrix.
	/// \param i The row to set (0-2).
	/// \param r The new element values as a vector.
	void setRow(size_t i, const Vector3& r) {
		MAFEM_ASSERT_MSG(i>=0 && i<3, "Matrix3::setRow()", "Row index out of range.");
		m[0][i] = r.X; m[1][i] = r.Y; m[2][i] = r.Z;
	}	

	/// \brief Computes the inverse of the matrix. 
	/// \throw Exception if matrix is not invertible because it is singular.
	Matrix3 inverse() const {
		const FloatType det = determinant();
		MAFEM_ASSERT_MSG(det != 0, "Matrix3::inverse()", "Singular matrix cannot be inverted: determinant is zero.");
		if(det == 0) throw Exception("AffineTransformation cannot be inverted: determinant is zero.");
		return Matrix3( (m[1][1]*m[2][2] - m[1][2]*m[2][1])/det,
						(m[2][0]*m[1][2] - m[1][0]*m[2][2])/det,
						(m[1][0]*m[2][1] - m[1][1]*m[2][0])/det,
						(m[2][1]*m[0][2] - m[0][1]*m[2][2])/det,
						(m[0][0]*m[2][2] - m[2][0]*m[0][2])/det,
						(m[0][1]*m[2][0] - m[0][0]*m[2][1])/det,
						(m[0][1]*m[1][2] - m[1][1]*m[0][2])/det,
						(m[0][2]*m[1][0] - m[0][0]*m[1][2])/det,
						(m[0][0]*m[1][1] - m[1][0]*m[0][1])/det);
	}

	/// \brief Computes the determinant of the matrix.
	FloatType determinant() const {
		return((m[0][0]*m[1][1] - m[0][1]*m[1][0])*(m[2][2])
			  -(m[0][0]*m[1][2] - m[0][2]*m[1][0])*(m[2][1])
			  +(m[0][1]*m[1][2] - m[0][2]*m[1][1])*(m[2][0]));
	}

	/// \brief Returns the transpose of this matrix.
	/// \return A new matrix with columns and rows swapped.
	Matrix3 transposed() const {
		return Matrix3(
			m[0][0], m[0][1], m[0][2],
			m[1][0], m[1][1], m[1][2],
			m[2][0], m[2][1], m[2][2]);
	}
	
	/// \brief Returns the sum of the absolute values of the entries of this matrix.
	FloatType absSum() const
	{
		FloatType sum = 0;
		for(int i = 0; i < 3; ++i)
			for(int j = 0; j < 3; ++j)
				sum += abs(m[i][j]);
		return sum;
	}

	/// \brief Balances the matrix.
	///
	/// Replaces the matrix with a balanced matrix with identical eigenvalues.
	/// A symmetric matrix is already balanced and is unafected by this procedure.
	void balance();

	/// \brief Reduces matrix to Hessenberg form.
	///
	/// Reduction to Hessenberg form by the elimination method.
	/// This real, nonsymmetric matrix is replaced by an upper Hessenberg matrix 
	/// with identical eigenvalues. 
	/// Recommended, but not required, is that this routine be preceded by balance().
	void eliminateHessenberg();

	/// \brief Computes the eigenvalues of the matrix.
	///
	/// Finds all eigenvalues of an upper Hessenberg matrix.
	/// On input the matrix can be exactly as output from eliminateHessenberg(). 
	/// \note On output the original matrix values are overwritten!
	void hqr(complex<FloatType> eigenvalues[3]);

	/// \brief fFinds the maximum eigenvalue of the matrix.
	FloatType maxEigenvalue() const;

	/// \brief Finds the minimum eigenvalue of the matrix.
	FloatType minEigenvalue() const;

	/// \brief Returns a string representation of this matrix.
	QString toString() const { 
		return QString("%1\n%2\n%3").arg(row(0).toString(), row(1).toString(), row(2).toString());
	}

	/// \brief Generates the identity matrix
	/// \return A matrix with all diagonal elements set to one and all off-diagonal elements set to zero.
	static Matrix3 identity() { return Matrix3(IDENTITY); }
	/// \brief Generates a rotation matrix around the X axis.
	/// \param angle The rotation angle in radians.
	static Matrix3 rotationX(FloatType angle);
	/// \brief Generates a rotation matrix around the Y axis.
	/// \param angle The rotation angle in radians.
	static Matrix3 rotationY(FloatType angle);
	/// \brief Generates a rotation matrix around the Z axis.
	/// \param angle The rotation angle in radians.
	static Matrix3 rotationZ(FloatType angle);
	/// \brief Generates a rotation matrix from an axis and an angle.
	/// \param rot The rotation specified in the Rotation structure.
	static Matrix3 rotation(const Rotation& rot);
	/// \brief Generates a rotation matrix from a quaternion.
	/// \param q The Quaternion describing the 3d rotation.
	static Matrix3 rotation(const Quaternion& q);
	/// \brief Generates a scaling matrix.
	static Matrix3 scaling(const Scaling& scaling);

	friend Vector3 operator*(const Matrix3& a, const Vector3& v);
	friend Point3 operator*(const Matrix3& a, const Point3& v);
	friend Matrix3 operator*(const Matrix3& a, const Matrix3& b);
	friend Matrix3 operator*(const Matrix3& a, FloatType s);

	friend class AffineTransformation;
	friend class Matrix4;
	
	/// \brief Adds a 3x3 matrix to a 3x3 Matrix.
	inline Matrix3& operator+=(const Matrix3& a) 
	{
		for(size_t i=0; i<3; ++i)
			for(size_t j=0; j<3; ++j)
				m[j][i] += a(i, j);
		return *this;
	}
	
private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) { ar & m; }	

	friend class boost::serialization::access;	
};

/// \brief Multiplies a 3x3 matrix with a Vector3.
inline Vector3 operator*(const Matrix3& a, const Vector3& v) 
{
	return Vector3(a.m[0][0]*v.X + a.m[1][0]*v.Y + a.m[2][0]*v.Z, 
				   a.m[0][1]*v.X + a.m[1][1]*v.Y + a.m[2][1]*v.Z, 
				   a.m[0][2]*v.X + a.m[1][2]*v.Y + a.m[2][2]*v.Z);
}

/// \brief Multiplies a 3x3 matrix with a Point3.
inline Point3 operator*(const Matrix3& a, const Point3& v)
{
	return Point3(a.m[0][0]*v.X + a.m[1][0]*v.Y + a.m[2][0]*v.Z, 
				  a.m[0][1]*v.X + a.m[1][1]*v.Y + a.m[2][1]*v.Z, 
				  a.m[0][2]*v.X + a.m[1][2]*v.Y + a.m[2][2]*v.Z);
}

/// \brief Multiplies a 3x3 matrix with a 3x3 Matrix.
inline Matrix3 operator*(const Matrix3& a, const Matrix3& b) 
{
	Matrix3 m;
	for(size_t i=0; i<3; i++) {
		for(size_t j=0; j<3; j++) {
			FloatType v(0);
			for(size_t k=0; k<3; k++)
				v += a(i, k) * b(k, j);
			m(i, j) = v;
		}
	}
	return m;
}

/// \brief Adds a 3x3 matrix to a 3x3 Matrix.
inline Matrix3 operator+(const Matrix3& a, const Matrix3& b) 
{
	Matrix3 m;
	for(size_t i=0; i<3; ++i)
		for(size_t j=0; j<3; ++j)
			m(i, j) = a(i, j) + b(i, j);
	return m;
}

/// \brief Multiplies a 3x3 matrix with a scalar value. 
/// Each element of the matrix is multiplied by the scalar value.
inline Matrix3 operator*(const Matrix3& a, FloatType s) 
{
	Matrix3 b;
	for(size_t i=0; i<3; i++)
		for(size_t j=0; j<3; j++)
			b.m[i][j] = a.m[i][j] * s;
	return b;
}
/// \brief Multiplies a 3x3 matrix with a scalar value. 
/// Each element of the matrix is multiplied by the scalar value.
inline Matrix3 operator*(FloatType s, const Matrix3& a) { return a * s; }

/// \brief Computes the dyadic product of two vectors.
inline Matrix3 Dyadic(const Vector3& u, const Vector3& v)
{
	Matrix3 m;
	for(size_t i=0; i<3; ++i)
		for(size_t j=0; j<3; ++j)
			m(i, j) = u[i] * v[j];
	return m;
}

/// \brief Computes the cross product matrix of a vector.
inline Matrix3 crossProductMatrix(const Vector3& v)
{
	return Matrix3(	   0, -v.Z,  v.Y,
					 v.Z,    0, -v.X,
					-v.Y,  v.X,    0);
}
				       
/// \brief Writes the matrix to a text output stream.
inline std::ostream& operator<<(std::ostream &os, const Matrix3& m)
{
	return os << m.row(0) << endl << m.row(1) << endl << m.row(2);
}

/// \brief Writes the vector to a logging stream.
inline LoggerObject& operator<<(LoggerObject& log, const Matrix3& m)
{
	for(size_t row = 0; row < 3; row++)
		log.space() << m(row, 0) << m(row, 1) << m(row, 2) << endl;
	return log;
} 

/// \brief Writes a matrix to a binary Qt output stream.
inline QDataStream& operator<<(QDataStream& stream, const Matrix3& m)
{
	return stream << m.column(0) << m.column(1) << m.column(2);
}

/// \brief Writes a matrix to a text Qt output stream.
inline QTextStream& operator<<(QTextStream& stream, const Matrix3& m)
{
	return stream << "(" << m.column(0) << " " << m.column(1) << " " << m.column(2) << ")";
}


/// \brief Reads a matrix from a binary input stream.
inline QDataStream& operator>>(QDataStream& stream, Matrix3& m)
{
	return stream >> m.column(0) >> m.column(1) >> m.column(2);
}

};	// End of namespace Base

Q_DECLARE_METATYPE(Base::Matrix3)
Q_DECLARE_TYPEINFO(Base::Matrix3, Q_PRIMITIVE_TYPE);

// We don't need versioning info for this type when serializing.
BOOST_CLASS_IMPLEMENTATION(Base::Matrix3, boost::serialization::object_serializable)

#endif // __MATRIX3_H
