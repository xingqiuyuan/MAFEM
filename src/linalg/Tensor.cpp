///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include <Base.h>
#include <linalg/Matrix3.h>
#include <linalg/Tensor.h>

namespace Base {

/******************************************************************************
* Computes the eigenvalues and optionally eigenvectors of this symmetric tensor.
******************************************************************************/
void SymmetricTensor2::eigenvalues(FloatType lambda[3], Matrix3* eigenvectors) const
{
	if(eigenvectors) *eigenvectors = IDENTITY;

	SymmetricTensor2 T2(*this);
	if(T2.a[3]*T2.a[3]+T2.a[4]*T2.a[4]+T2.a[5]*T2.a[5] > 1e3 * std::numeric_limits<FloatType>::min()) {

		FloatType delta = 1.e-20 * DoubleContraction(T2,T2); 
		for(size_t iter = 0; iter < 5; iter++) {
			// For large diagonal terms and small but nonzero off-diagonals,
			// do at least one sweep.
			T2.Givens(1, 0, eigenvectors);
			T2.Givens(2, 0, eigenvectors);
			T2.Givens(2, 1, eigenvectors);

			if(T2.a[3]*T2.a[3]+T2.a[4]*T2.a[4]+T2.a[5]*T2.a[5] <= delta)
				break;
		}
	}

	lambda[0] = T2.a[0];
	lambda[1] = T2.a[1];
	lambda[2] = T2.a[2];
}

/******************************************************************************
* Finds the maximum absolute eigenvalue of the symmetric tensor.
******************************************************************************/
FloatType SymmetricTensor2::maxEigenvalue() const
{
	using namespace std;

	FloatType lambda[3];
	eigenvalues(lambda);

	// Find maximum eigenvalue.
	FloatType dmax = 0.0;
	for(size_t i=0; i<3; i++)
		dmax = max(dmax, abs(lambda[i]));
	return dmax;
}

/******************************************************************************
* Finds the minimum absolute eigenvalue of the symmetric tensor.
******************************************************************************/
FloatType SymmetricTensor2::minEigenvalue() const
{
	using namespace std;

	FloatType lambda[3];
	eigenvalues(lambda);

	// Find minimum eigenvalue.
	FloatType dmin = FLOATTYPE_MAX;
	for(size_t i=0; i<3; i++)
		dmin = min(dmin, abs(lambda[i]));
	return dmin;
}

/******************************************************************************
* Perform Givens rotation of this symmetric tensor
******************************************************************************/
void SymmetricTensor2::Givens(size_t p, size_t q, Matrix3* eigenvectors)
{ 
	using namespace std;

	/* Givens transformation for row and column p and q
	See Matrix Computations, Golub & Van Loan, 2nd ed.,
	section 5.1.8 and section 8.5.2 */

	FloatType Apq, tau, t, s, c, taup, tauq;

	MAFEM_ASSERT(p<3);
	MAFEM_ASSERT(q<3);
	MAFEM_ASSERT(p!=q);

	if(p<q) swap(p,q);

	Apq = (*this)(p,q);
	if(abs(Apq) > 1e3 * numeric_limits<FloatType>::min()) {
		FloatType App = a[p]; //A(p,p)
		FloatType Aqq = a[q]; //A(q,q)

		tau = (Aqq - App)/(2*Apq);
		t = 1/(abs(tau)+sqrt(1+tau*tau));
		if(tau < 0) t = -t;
		c = 1/sqrt(1+t*t);
		s = t*c;

		FloatType Apq2cs = Apq*2*c*s;
		FloatType c2 = c*c;
		FloatType s2 = s*s;

		// Pre multiply by J.transpose() and post multiply by J.
		a[p] = App*c2 + Aqq*s2 - Apq2cs; // A(p,p) 
		a[q] = Aqq*c2 + App*s2 + Apq2cs; // A(q,q) 
		(*this)(p,q) = 0;

		if(q==0)
			if(p==1) {
				FloatType Arp = a[4]; //A(3,2)
				FloatType Arq = a[5]; //A(3,1)
				a[4] = Arp*c-Arq*s;
				a[5] = Arp*s+Arq*c;
			}
			else { // p==2
				FloatType Arp = a[4]; //A(3,2)
				FloatType Arq = a[3]; //A(2,1)
				a[4] = Arp*c-Arq*s;
				a[3] = Arp*s+Arq*c;
			}
		else { // p==2 && q==1
			FloatType Arp = a[5]; //A(3,1)
			FloatType Arq = a[3]; //A(2,1)
			a[5] = Arp*c-Arq*s;
			a[3] = Arp*s+Arq*c;
		}

		// Post multiply 'eigenvectors' matrix by J to update eigen vectors (as columns).
		if(eigenvectors) {
			taup = (*eigenvectors)(0,p); tauq = (*eigenvectors)(0,q);
			(*eigenvectors)(0,p) = c*taup-s*tauq; (*eigenvectors)(0,q) = s*taup+c*tauq;
			taup = (*eigenvectors)(1,p); tauq = (*eigenvectors)(1,q);
			(*eigenvectors)(1,p) = c*taup-s*tauq; (*eigenvectors)(1,q) = s*taup+c*tauq;
			taup = (*eigenvectors)(2,p); tauq = (*eigenvectors)(2,q);
			(*eigenvectors)(2,p) = c*taup-s*tauq; (*eigenvectors)(2,q) = s*taup+c*tauq;
		}
	}
}


};	// End of namespace Base
