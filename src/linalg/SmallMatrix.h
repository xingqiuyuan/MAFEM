///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __SMALLMATRIX_H
#define __SMALLMATRIX_H

#include "VectorN.h"

namespace Base {

/******************************************************************************
* The template class SmallAffineTransformation treats small size matrices with abitrary but 
* constant size. 
******************************************************************************/
template<size_t NR, size_t NC>
class SmallMatrix
{
public:
	/// Default constructor.
	/// Does NOT initialize the matrix entries.
	SmallMatrix() {}

	/// Initializes matrix entries to the given value.
    SmallMatrix(FloatType value) {
		for(size_t i=0; i<NR*NC; i++)
			a[i] = value;
	}

	/// Operator () (Non constant version).
	FloatType& operator()(size_t row, size_t col) { 
		MAFEM_ASSERT(row>=0 && row<NR && col>=0 && col<NC);
		return a[row*NC+col];
	}

	/// Operator () (Constant version).
	FloatType operator()(size_t row, size_t col) const { 
		MAFEM_ASSERT(row>=0 && row<NR && col>=0 && col<NC);
		return a[row*NC+col];
	}

	/// Multiply all matrix entries by scalar s.
    SmallMatrix<NR,NC>& operator*=(FloatType s) {
		for(size_t k=0; k<NR*NC; k++) a[k] *= s;
		return *this;
    }

	/// Returns the transposed matrix.
	SmallMatrix<NC, NR> Transposed() const {
        SmallMatrix<NC, NR> t;
		for(size_t i=0; i<NR; i++)
			for(size_t j=0; j<NC; j++)
				t(j, i) = (*this)(i, j);
        return t;
	}

	/// Return pointer to matrix as a C-array.
	FloatType* data() { return a; }

	/// Return pointer to matrix as a C-array.
	const FloatType* constData() const { return a; }

private:
    FloatType a[NR*NC];
};

/// Product of two matrices.
template<size_t NRa, size_t NCa, size_t NRb, size_t NCb>
inline SmallMatrix<NRa, NCb> operator*(const SmallMatrix<NRa, NCa>& a, const SmallMatrix<NRb, NCb>& b) 
{
	MAFEM_STATIC_ASSERT(NCa == NRb);
	SmallMatrix<NRa, NCb> c;
	for(size_t i=0; i<NRa; i++) {
		for(size_t j=0; j<NCb; j++) {
			FloatType v = 0.0;
			for(size_t k=0; k<NCa; k++) {
				v += a(i, k) * b(k, j);
			}
			c(i, j) = v;
		}
	}
	return c;
}

/// Sum of two matrices.
template<size_t NR, size_t NC>
inline SmallMatrix<NR, NC> operator+(const SmallMatrix<NR, NC>& a, const SmallMatrix<NR, NC>& b) 
{
	SmallMatrix<NR, NC> c;
	for(size_t i=0; i<NR*NC; i++)
		c.data()[i] = a.constData()[i] + b.constData()[i];
	return c;
}

/// Product of a matrix and a vector.
template<size_t NRa, size_t NCa, size_t NRb>
inline VectorN<FloatType, NRa> operator*(const SmallMatrix<NRa, NCa>& a, const VectorN<FloatType, NRb>& b) 
{
	MAFEM_STATIC_ASSERT(NCa == NRb);
	VectorN<FloatType, NRa> c;
	for(size_t i=0; i<NRa; i++) {
		FloatType v = 0.0;
		for(size_t k=0; k<NCa; k++) {
			v += a(i, k) * b(k);
		}
		c(i) = v;
	}
	return c;
}

/// Product of a scalar and a matrix.
template<size_t NR, size_t NC>
inline SmallMatrix<NR, NC> operator*(FloatType s, const SmallMatrix<NR, NC>& m) 
{
	SmallMatrix<NR, NC> b;
	for(size_t i=0; i<NR*NC; i++)
		b.data()[i] = m.constData()[i] * s;
	return b;
}

/// Writes the matrix to an output stream.
template<size_t NR, size_t NC>
inline std::ostream& operator<<(std::ostream &os, const SmallMatrix<NR, NC>& m) {
	using namespace std;
	for(size_t row = 0; row < NR; row++) {
		for(size_t col = 0; col < NC; col++) {
			os << m(row, col) << " ";
		}
		os << endl;
	}
	return os;
}

/// Writes the matrix to a logging stream.
template<size_t NR, size_t NC>
inline LoggerObject& operator<<(LoggerObject& log, const SmallMatrix<NR, NC>& m)
{
	for(size_t row = 0; row < NR; row++) {
		for(size_t col = 0; col < NC; col++) {
			log.space() << m(row, col);
		}
		log.nospace() << endl;
	}
	return log.space();
} 

/// Writes a matrix to an output stream.
template<size_t NR, size_t NC>
inline QDataStream& operator<<(QDataStream& stream, const SmallMatrix<NR, NC>& m)
{
	for(size_t i = 0; i < NC*NR; i++)
		stream << m.constData()[i];
	return stream;
}

/// Reads a matrix from an input stream.
template<size_t NR, size_t NC>
inline QDataStream& operator>>(QDataStream& stream, SmallMatrix<NR, NC>& m)
{
	for(size_t i = 0; i < NC*NR; i++)
		stream >> m.data()[i];
	return stream;
}

};	// End of namespace Base

#endif		// __SMALLMATRIX_H
