///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "mafem2python.h"

namespace Scripting {

using namespace boost::python;
using namespace Base;

template<class T>
struct matrix_wrapper 
{
	static FloatType get(const T& m, size_t i, size_t j) { return m(i,j); }
	static void set(T& m, size_t i, size_t j, FloatType v) { m(i,j) = v; }
};

template<class T>
struct vector_wrapper 
{
	static FloatType Length(const T& v) { return Base::Length(v); }
	static FloatType LengthSquared(const T& v) { return Base::LengthSquared(v); }
	static Vector3 Normalize(const T& v) { return Base::Normalize(v); }
	static Vector3 CrossProduct(const T& v, const T& v2) { return Base::CrossProduct(v, v2); }	
	static FloatType DotProduct(const T& v, const T& v2) { return Base::DotProduct(v, v2); }	
	static FloatType Get(const T& v, size_t i) { return v[i]; }
	static void Set(T& v, size_t i, FloatType value) { v[i] = value; }
};

void exportLinAlg()
{ 
	class_<Vector3>("Vector3", init< boost::python::optional<NullVector> >())
		.def(init<FloatType, FloatType, FloatType>())
		.def(init<FloatType>())
		.def_readwrite("X", &Vector3::X)
		.def_readwrite("Y", &Vector3::Y)
		.def_readwrite("Z", &Vector3::Z)
		.def(self + other<Vector3>())
		.def(self += other<Vector3>())
		.def(self - other<Vector3>())
		.def(self -= other<Vector3>())
		.def(self * FloatType())
		.def(self *= FloatType())
		.def(self / FloatType())
		.def(self /= FloatType())
		.def(Origin() + self)
		.def(self == other<Vector3>())
		.def(self != other<Vector3>())
		.add_property("Length", &vector_wrapper<Vector3>::Length)
		.add_property("LengthSquared", &vector_wrapper<Vector3>::LengthSquared)
		.def("Normalize", &vector_wrapper<Vector3>::Normalize, return_value_policy<return_by_value>())
		.def("CrossProduct", &vector_wrapper<Vector3>::CrossProduct, return_value_policy<return_by_value>())
		.def("DotProduct", &vector_wrapper<Vector3>::DotProduct)
		.def("__len__", &Vector3::size)
		.def("__getitem__", &vector_wrapper<Vector3>::Get)
		.def("__setitem__", &vector_wrapper<Vector3>::Set)
		.def("__str__", &Vector3::toString)
	;
	
	class_<Vector3I>("Vector3I", init< boost::python::optional<NullVector> >())
		.def(init<int, int, int>())
		.def(init<int>())
		.def_readwrite("X", &Vector3I::X)
		.def_readwrite("Y", &Vector3I::Y)
		.def_readwrite("Z", &Vector3I::Z)
		.def(self + other<Vector3I>())
		.def(self += other<Vector3I>())
		.def(self - other<Vector3I>())
		.def(self -= other<Vector3I>())
		.def(self * int())
		.def(self *= int())
		.def(self / int())
		.def(self /= int())
		.def(Origin() + self)
		.def(self == other<Vector3I>())
		.def(self != other<Vector3I>())
		.def("__len__", &Vector3I::size)
		.def("__getitem__", &vector_wrapper<Vector3I>::Get)
		.def("__setitem__", &vector_wrapper<Vector3I>::Set)
		.def("__str__", &Vector3I::toString)
	;

	class_<Point3>("Point3", init< boost::python::optional<Origin> >())
		.def(init<FloatType, FloatType, FloatType>())
		.def(init<FloatType>())
		.def_readwrite("X", &Point3::X)
		.def_readwrite("Y", &Point3::Y)
		.def_readwrite("Z", &Point3::Z)
		.def(self + other<Point3>())
		.def(self + other<Vector3>())
		.def(self - other<Vector3>())
		.def(self - other<Point3>())
		.def(self += other<Vector3>())
		.def(self -= other<Vector3>())
		.def(self * FloatType())
		.def(self / FloatType())
		.def(self - Origin())
		.def(self == other<Point3>())
		.def(self != other<Point3>())
		.def("__len__", &Point3::size)
		.def("__getitem__", &vector_wrapper<Point3>::Get)
		.def("__setitem__", &vector_wrapper<Point3>::Set)
		.def("__str__", &Point3::toString)
	;

	class_<Point3I>("Point3I", init< boost::python::optional<Origin> >())
		.def(init<int, int, int>())
		.def(init<int>())
		.def_readwrite("X", &Point3I::X)
		.def_readwrite("Y", &Point3I::Y)
		.def_readwrite("Z", &Point3I::Z)
		.def(self + other<Point3I>())
		.def(self + other<Vector3I>())
		.def(self - other<Vector3I>())
		.def(self - other<Point3I>())
		.def(self += other<Vector3I>())
		.def(self -= other<Vector3I>())
		.def(self * int())
		.def(self / int())
		.def(self - Origin())
		.def(self == other<Point3I>())
		.def(self != other<Point3I>())
		.def("__len__", &Point3I::size)
		.def("__getitem__", &vector_wrapper<Point3I>::Get)
		.def("__setitem__", &vector_wrapper<Point3I>::Set)
		.def("__str__", &Point3I::toString)
	;
	
	class_<Quaternion>("Quaternion", init< boost::python::optional<IdentityQuaternion> >())
		.def(init<const AffineTransformation&>())
		.def_readwrite("X", &Quaternion::X)
		.def_readwrite("Y", &Quaternion::Y)
		.def_readwrite("Z", &Quaternion::Z)
		.def_readwrite("W", &Quaternion::W)
		.def("Inverse", &Quaternion::inverse, return_value_policy<return_by_value>())
		.def(self == other<Quaternion>())
		.def(self != other<Quaternion>())
		.def("__str__", &Quaternion::toString)
	;	
	
	class_<Scaling>("Scaling", init< boost::python::optional<IdentityScaling> >())
		.def(init<Vector3>())
		.def("Inverse", &Scaling::inverse, return_value_policy<return_by_value>())
		.def(self == other<Scaling>())
		.def(self != other<Scaling>())
		.def("__str__", &Scaling::toString)
	;	

	class_<AffineTransformation>("AffineTransformation", init<>())
		.def(init<FloatType,FloatType,FloatType,FloatType,FloatType,FloatType,FloatType,FloatType,FloatType,FloatType,FloatType,FloatType>())
		.def(init<const Matrix3&>())
		.add_property("Determinant", &AffineTransformation::determinant)
		.add_property("Translation", 
			make_function(&AffineTransformation::getTranslation, return_internal_reference<>()),
			&AffineTransformation::setTranslation)
		.def("Inverse", &AffineTransformation::inverse, return_value_policy<return_by_value>())
		.def("GetColumn", &AffineTransformation::getColumn, return_internal_reference<>())
		.def("SetColumn", &AffineTransformation::setColumn)
		.def("Get", &matrix_wrapper<AffineTransformation>::get)
		.def("Set", &matrix_wrapper<AffineTransformation>::set)
		.def(self * other<AffineTransformation>())
		.def(self * other<Point3>())
		.def(self * other<Point3I>())
		.def(self * other<Vector3>())
		.def(self * other<Vector3I>())
		.def(self * other<FloatType>())
		.def("Identity", &AffineTransformation::identity, return_value_policy<return_by_value>())
		.staticmethod("Identity")
		.def("RotationX", &AffineTransformation::rotationX, return_value_policy<return_by_value>())
		.staticmethod("RotationX")
		.def("RotationY", &AffineTransformation::rotationY, return_value_policy<return_by_value>())
		.staticmethod("RotationY")
		.def("RotationZ", &AffineTransformation::rotationZ, return_value_policy<return_by_value>())
		.staticmethod("RotationZ")
		.def("RotationAxis", &AffineTransformation::rotationAxis, return_value_policy<return_by_value>())
		.staticmethod("RotationAxis")
		.def("RotationPoint", &AffineTransformation::rotationPoint, return_value_policy<return_by_value>())
		.staticmethod("RotationPoint")
		.def("FromQuaternion", (AffineTransformation (*)(const Quaternion&))&AffineTransformation::rotation, return_value_policy<return_by_value>())
		.staticmethod("FromQuaternion")
		.def("FromRotation", (AffineTransformation (*)(const Rotation&))&AffineTransformation::rotation, return_value_policy<return_by_value>())
		.staticmethod("FromRotation")
		.def("IsoScaling", (AffineTransformation (*)(FloatType))&AffineTransformation::scaling, return_value_policy<return_by_value>())
		.staticmethod("IsoScaling")
		.def("Scaling", (AffineTransformation (*)(const Base::Scaling& scaling))&AffineTransformation::scaling, return_value_policy<return_by_value>())
		.def("Scaling", (AffineTransformation (*)(double))&AffineTransformation::scaling, return_value_policy<return_by_value>())
		.def("Scaling", (AffineTransformation (*)(double, double, double))&AffineTransformation::scaling, return_value_policy<return_by_value>())
		.def("Scaling", (AffineTransformation (*)(Vector3&))&AffineTransformation::scaling, return_value_policy<return_by_value>())
		.staticmethod("Scaling")
		.def("Shear", &AffineTransformation::shear, return_value_policy<return_by_value>())
		.staticmethod("Shear")
		.def<AffineTransformation (*)(const Vector3&)>("Translation", &AffineTransformation::translation, return_value_policy<return_by_value>())
		.def<AffineTransformation (*)(double, double, double)>("Translation", &AffineTransformation::translation, return_value_policy<return_by_value>())
		.staticmethod("Translation")
		.def("__str__", &AffineTransformation::toString)
	;
	
	class_<Matrix3>("Matrix3", init<>())
		.add_property("Determinant", &Matrix3::determinant)
		.def("Inverse", &Matrix3::inverse, return_value_policy<return_by_value>())
		.def("GetColumn", &Matrix3::getColumn, return_internal_reference<>())
		.def("SetColumn", &Matrix3::setColumn)
		.def("Get", &matrix_wrapper<Matrix3>::get)
		.def("Set", &matrix_wrapper<Matrix3>::set)
		.def(self * other<Matrix3>())
		.def(self * other<Point3>())
		.def(self * other<Vector3>())
		.def(self * other<FloatType>())
		.def("__str__", &Matrix3::toString)
		.def("Identity", &Matrix3::identity, return_value_policy<return_by_value>())
		.staticmethod("Identity")
		.def("RotationX", &Matrix3::rotationX, return_value_policy<return_by_value>())
		.staticmethod("RotationX")
		.def("RotationY", &Matrix3::rotationY, return_value_policy<return_by_value>())
		.staticmethod("RotationY")
		.def("RotationZ", &Matrix3::rotationZ, return_value_policy<return_by_value>())
		.staticmethod("RotationZ")
		.def("Scaling", &Matrix3::scaling, return_value_policy<return_by_value>())
		.staticmethod("Scaling")
	;

	class_<Vector4>("Vector4", init< boost::python::optional<NullVector> >())
		.def(init<FloatType, FloatType, FloatType, FloatType>())
		.def("__len__", &Vector4::size)
		.def("__getitem__", &vector_wrapper<Vector4>::Get)
		.def("__str__", &Vector4::toString)		
	;
	
	class_<Box3>("Box3", init<>())
		.def(init<const Point3&, const Point3&>())
		.def(init<const Point3&, FloatType>())		
		.add_property("IsEmpty", &Box3::isEmpty)
		.add_property("Center", make_function(&Box3::center, return_value_policy<return_by_value>()))
		.add_property("Size", make_function((Vector3 (Box3::*)() const)&Box3::size, return_value_policy<return_by_value>()))
		.def("Contains", &Box3::contains)
		.def("ClassifyPoint", &Box3::classifyPoint)
		.def("__str__", &Box3::toString)		
	;
	
	class_<Rotation>("Rotation", init< boost::python::optional<NullRotation> >())
		.def(init<const Vector3&, FloatType>())
		.def(init<const AffineTransformation&>())
		.def(init<const Quaternion&>())
		.def_readwrite("Axis", &Rotation::axis)
		.def_readwrite("Angle", &Rotation::angle)
		.def("Inverse", &Rotation::inverse, return_value_policy<return_by_value>())
		.def("__str__", &Rotation::toString)
		//.def(self * other<const Rotation&>)
		.def(self += other<Rotation>())
		.def(self -= other<Rotation>())
		.def(self == other<Rotation>())
		.def(self != other<Rotation>())
	;
}

};
