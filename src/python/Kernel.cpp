///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "mafem2python.h"
#include "ContainerWrappers.h"
#include <simulation/Simulation.h>
#include <grain/CrystalLattice.h>
#include <grain/Grain.h>
#include <atoms/RepAtom.h>
#include <atoms/SamplingAtomCollection.h>
#include <mesh/Element.h>
#include <mesh/Mesh.h>
#include <mesh/MeshNodeCollection.h>
#include <material/EAMPotential.h>
#include <force/EnergyQCCalculator.h>
#include <force/EnergyQRCalculator.h>
#include <force/ForceQCCalculator.h>
#include <force/LatticeStaticsCalculator.h>
#include <force/CalculationResult.h>
#include <force/NumericalDerivativeCheck.h>
#include <minimizer/Minimizer.h>
#include <minimizer/GSLMinimizer.h>
#include <minimizer/FIREMinimizer.h>
#include <boundaryconditions/Group.h>
#include <boundaryconditions/Region.h>
#include <boundaryconditions/ForceBoundaryCondition.h>
#include <boundaryconditions/DisplacementBoundaryCondition.h>

namespace Scripting {

using namespace boost::python;
using namespace Base;
using namespace MAFEM;

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(Mesh_exportMeshToTecplot_overloads, exportMeshToTecplot, 1, 2);
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(Mesh_exportMeshToVTK_overloads, exportMeshToVTK, 1, 2);
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(Mesh_exportMeshToDump_overloads, exportMeshToDump, 1, 2);
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(Mesh_exportAtoms_overloads, exportAtoms, 1, 2);
//BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(Calculator_calculateCentrosymmetry_overloads, calculateCentrosymmetry, 1, 5);

void exportKernel()
{
	class_<SimulationResource, noncopyable>("SimulationResource", no_init)
		.add_property("Simulation", make_function(&SimulationResource::simulation, return_internal_reference<>()))
	;
	
	enum_<LatticeType>("LatticeType")
    	.value("SC", SC)
    	.value("FCC", FCC)
    	.value("BCC", BCC)
    ;
	
	class_<CrystalLattice, noncopyable>("CrystalLattice", init<LatticeType, const AffineTransformation&>())
		.add_property("Translation", make_function(&CrystalLattice::translation, return_value_policy<copy_const_reference>()))
		.add_property("LatticeType", &CrystalLattice::latticeType)
		.add_property("UnitCellVolume", &CrystalLattice::unitCellVolume)
		.add_property("MinEigenvalue", &CrystalLattice::minEigenvalue)		
	;

	class_<LatticeSite, noncopyable>("LatticeSite", init<CrystalLattice*, const Point3I&>())
		.def(init<CrystalLattice*, const Point3&>())
		.add_property("Indices", make_function(&LatticeSite::indices, return_value_policy<copy_const_reference>()))
		.add_property("Pos", make_function(&LatticeSite::pos, return_value_policy<copy_const_reference>()), &LatticeSite::setPos)		
		.add_property("Lattice", make_function(&LatticeSite::lattice, return_internal_reference<>()))
	;

	class_<Atom, noncopyable>("Atom", no_init)
		.add_property("Site", make_function(&Atom::site, return_internal_reference<>()))
		.add_property("Pos", make_function(&Atom::pos, return_value_policy<copy_const_reference>()), &Atom::setPos)		
		.add_property("DeformedPos", make_function(&Atom::deformedPos, return_value_policy<copy_const_reference>()), &Atom::setDeformedPos)
		.add_property("Displacement", make_function(&Atom::displacement, return_value_policy<return_by_value>()), &Atom::setDisplacement)
	;

	class_<RepAtom, bases<Atom>, noncopyable>("RepAtom", no_init)		
		.add_property("ClusterWeight", &RepAtom::clusterWeight, &RepAtom::setClusterWeight)
		.add_property("Index", &RepAtom::index)
		.add_property("PeriodicImageMaster", make_function(&RepAtom::periodicImageMaster, return_internal_reference<>()))
		.add_property("RealRepatom", make_function(&RepAtom::realRepatom, return_internal_reference<>()))
	;

	class_< QVector<RepAtom*> >("RepAtomVector", no_init)
		.def(QVector_readonly_indexing_suite< QVector<RepAtom*> >())		
	;
	
	class_<MeshNodeCollection, bases< QVector<RepAtom*> > >("MeshNodeCollection", no_init)		
		.def("FindAt", &MeshNodeCollection::findAt, return_internal_reference<>())
		
	;
	
	class_< array<RepAtom*, NEN> >("ElementVerticesArray", no_init)
		.def(boost_array_indexing_suite< array<RepAtom*, NEN> >())
	;
	
	class_<Element, noncopyable>("Element", no_init)	
		.add_property("Vertices", make_function(&Element::vertices, return_internal_reference<>()))
		.def("HasVertex", &Element::hasVertex)
		.def("VertexIndex", &Element::vertexIndex)
		.def("VertexLatticeIndices", make_function(&Element::vertexLatticeIndices, return_value_policy<copy_const_reference>()))
		.def("Neighbor", make_function(&Element::neighbor, return_internal_reference<>()))
		.def("HasNeighbor", &Element::hasNeighbor)
		.def("NeighborIndex", &Element::neighborIndex)
		.def("ContainsSite", &Element::containsSite)
		.def("ComputeBarycentric", &Element::computeBarycentric)	
		.add_property("Lattice", make_function(&Element::lattice, return_internal_reference<>()))
		.add_property("Det", &Element::det)
		.add_property("Volume", &Element::volume)
		.add_property("Marked", &Element::isMarked, &Element::setMarked)
	;

	class_<ElementCollection>("ElementCollection", no_init)		
		.def(QVector_readonly_indexing_suite< ElementCollection >())		
		.def("FindAt", &ElementCollection::findAt, return_internal_reference<>())
	;

	class_<SamplingAtom, bases<Atom>, noncopyable>("SamplingAtom", no_init)		
		.add_property("Element", make_function(&SamplingAtom::element, return_internal_reference<>()))
		.add_property("Barycentric", make_function(&SamplingAtom::barycentric, return_value_policy<return_by_value>()))
		.add_property("ClusterRepatom", make_function(&SamplingAtom::clusterRepatom, return_internal_reference<>()))
		.add_property("SamplingNode", make_function(&SamplingAtom::samplingNode, return_internal_reference<>()))
		.add_property("IsPassive", &SamplingAtom::isPassive)
		.add_property("IsActive", &SamplingAtom::isActive)
	;

	class_<SamplingAtomCollection>("SamplingAtomCollection", no_init)		
		.def(QVector_readonly_indexing_suite<SamplingAtomCollection>())		
		.def("FindAt", make_function(&SamplingAtomCollection::findAt, return_internal_reference<>()))
	;
	
	class_<Mesh, bases<SimulationResource>, noncopyable>("Mesh", no_init)
		.def("CreateNode", &Mesh::createNode, return_internal_reference<>())
		.def("Triangulate", &Mesh::triangulate)
		.def("GenerateInitialMesh", &Mesh::generateInitialMesh)
		.def("RefineLocally", &Mesh::RefineLocally)
		.def("ExportMeshToTetView", &Mesh::exportMeshToTetView)
		.def("ExportMeshToTecplot", &Mesh::exportMeshToTecplot, Mesh_exportMeshToTecplot_overloads())
		.def("ExportMeshToVTK", &Mesh::exportMeshToVTK, Mesh_exportMeshToVTK_overloads())
		.def("ExportMeshOnlyToVTK", &Mesh::exportMeshOnlyToVTK)
		.def("ExportMeshTriasToVTK", &Mesh::exportMeshTriasToVTK)
		.def("ExportSamplingAtomsToVTK", &Mesh::exportSamplingAtomsToVTK)
		.def("ExportMeshToDump", &Mesh::exportMeshToDump, Mesh_exportMeshToDump_overloads())
		.def("ExportAtoms", &Mesh::exportAtoms, Mesh_exportAtoms_overloads())
		.def("ExportDXAdump", &Mesh::exportDXAdump)
		.def("ExportDislocation", &Mesh::exportDislocation)
		.def("DeleteMarkedElements", &Mesh::deleteMarkedElements)
		.def("SplitMarkedElements", &Mesh::splitMarkedElements)
		.def("DeformationUpdate", &Mesh::deformationUpdate)
		.def("DeflectOnEvec", &Mesh::deflectOnEvec)
		.def("BuildSamplingClusters", &Mesh::buildSamplingClusters)
		.def("CheckTriangulation", &Mesh::checkTriangulation)
		.add_property("RepAtoms", make_function(&Mesh::repatoms, return_internal_reference<>()))
		.add_property("Nodes", make_function(&Mesh::nodes, return_internal_reference<>()))
		.add_property("Elements", make_function(&Mesh::elements, return_internal_reference<>()))
		.add_property("SamplingAtoms", make_function(&Mesh::samplingAtoms, return_internal_reference<>()))
		.def("Vertex", &Mesh::createVertex)
		.def("TriFace", &Mesh::createTriFace)
		.def("QuadFace", &Mesh::createQuadFace)
		.def("ImportVTK", &Mesh::importVTK)
		.def("AdaptiveRefinement", &Mesh::AdaptiveRefinement)
		.def("MaxRefinementIndicator", &Mesh::maxRefinementIndicator)
		.def("TrackNode", &Mesh::trackNode)
		.def("ActiveSamplingAtoms", &Mesh::activesampatoms)
		.def("RemoveSite", &Mesh::setSiteToBeRemoved , return_internal_reference<>())
		.def("TransformGroup", &Mesh::transformGroup)
		.def("TransformRegion", &Mesh::transformRegion)
	;

	class_<Region, shared_ptr<Region>, noncopyable>("Region", no_init)
	;

	class_<BoxRegion, bases<Region>, shared_ptr<BoxRegion>, noncopyable>("BoxRegion", init<const Box3&>())
		.def(init<const Point3&, const Point3&>())
		.def(init<FloatType,FloatType,FloatType,FloatType,FloatType,FloatType>())
		.def("Contains", &BoxRegion::contains)
	;

	class_<Group, bases<SimulationResource>, noncopyable>("Group", no_init)
		.add_property("Region", make_function(&Group::region, return_internal_reference<>()))
		.add_property("GroupIndex", &Group::groupIndex)
	;

	class_<ForceBoundaryCondition, bases<SimulationResource>, shared_ptr<ForceBoundaryCondition>, noncopyable>("ForceBoundaryCondition", no_init)
		.add_property("Group", make_function(&ForceBoundaryCondition::group, return_internal_reference<>()))
	;

	class_<IndentorForceField, bases<ForceBoundaryCondition>, shared_ptr<IndentorForceField>, noncopyable>("IndentorForceField", init<Group*,FloatType,FloatType,const Point3&>())
		.add_property("AppliedForce", make_function(&IndentorForceField::appliedForce, return_value_policy<copy_const_reference>()))		
		.add_property("Center", make_function(&IndentorForceField::center, return_value_policy<copy_const_reference>()), &IndentorForceField::setCenter)		
	;

	class_<PlanarForceField, bases<ForceBoundaryCondition>, shared_ptr<PlanarForceField>, noncopyable>("PlanarForceField", init<Group*,FloatType,const Point3&,const Vector3&>())
		.add_property("AppliedForce", make_function(&PlanarForceField::appliedForce, return_value_policy<copy_const_reference>()))		
		.add_property("Center", make_function(&PlanarForceField::center, return_value_policy<copy_const_reference>()), &PlanarForceField::setCenter)
		.add_property("Normal", make_function(&PlanarForceField::normal, return_value_policy<copy_const_reference>()), &PlanarForceField::setNormal)		
	;
	
	class_<DistributedLoad, bases<ForceBoundaryCondition>, shared_ptr<DistributedLoad>, noncopyable>("DistributedLoad", init<Group*,const Vector3&>())	
		.add_property("Force", make_function(&DistributedLoad::force, return_value_policy<copy_const_reference>()), &DistributedLoad::setForce)		
	;
	
	class_<DisplacementBoundaryCondition, bases<SimulationResource>, shared_ptr<DisplacementBoundaryCondition>, noncopyable>("DisplacementBoundaryCondition", no_init)
		.add_property("Group", make_function(&DisplacementBoundaryCondition::group, return_internal_reference<>()))
		
	;

	class_<FixAtoms, bases<DisplacementBoundaryCondition>, shared_ptr<FixAtoms>, noncopyable>("FixAtoms", init<Group*,bool,bool,bool>())
		.add_property("GroupForce", make_function(&FixAtoms::groupForce, return_value_policy<copy_const_reference>()))
		.add_property("GroupTorque", make_function(&FixAtoms::groupTorque, return_value_policy<copy_const_reference>()))
		.add_property("Displacement", make_function(&FixAtoms::displacement, return_value_policy<copy_const_reference>()))
		.add_property("Position", make_function(&FixAtoms::position, return_value_policy<copy_const_reference>()))
		.add_property("RelaxStrain", make_function(&FixAtoms::relaxstrain, return_value_policy<copy_const_reference>()))
	;

	class_<DirichletBC, bases<DisplacementBoundaryCondition>, shared_ptr<DirichletBC>, noncopyable>("DirichletBC", init<Group*,const Vector3&,AffineTransformation&>())
		.add_property("Displacement", make_function(&DirichletBC::displacement, return_value_policy<copy_const_reference>()), &DirichletBC::setDisplacement)
		.add_property("Rotation", make_function(&DirichletBC::rotation, return_value_policy<copy_const_reference>()), &DirichletBC::setRotation)
		.add_property("RefinementCriterion", make_function(&DirichletBC::refinementCriterion, return_value_policy<copy_const_reference>()), &DirichletBC::setRefinementCriterion)
		.add_property("GroupForce", make_function(&DirichletBC::groupForce, return_value_policy<copy_const_reference>()))
		.add_property("GroupTorque", make_function(&DirichletBC::groupTorque, return_value_policy<copy_const_reference>()))
	;


	class_<Grain, bases<SimulationResource>, shared_ptr<Grain>, noncopyable>("Grain", init<LatticeType, int, const AffineTransformation&>())
		.add_property("Lattice", make_function(&Grain::lattice, return_internal_reference<>()))
		.add_property("AtomType", &Grain::atomType, &Grain::setAtomType)
	;

	class_<GrainList>("GrainList", no_init)
		.def(QVector_readonly_indexing_suite<GrainList>())		
	;

	class_<EAMPotential, shared_ptr<EAMPotential>, noncopyable>("EAMPotential", init<const QString&, bool>())		
	;
	
	class_< array<bool, NDOF> >("PeriodicBCArray", no_init)
		.def(boost_array_indexing_suite< array<bool, NDOF> >())		
	;
	
	class_<SimulationSettings, noncopyable>("SimulationSettings", no_init)
		.def_readwrite("PeriodicBC", &SimulationSettings::periodicBC)
		.def_readwrite("ClusterRadius", &SimulationSettings::clusterRadius)
		.def_readwrite("ClusterSkinThickness", &SimulationSettings::clusterSkinThickness)
		.def_readwrite("NeighborSkinThickness", &SimulationSettings::neighborSkinThickness)
		.def_readwrite("KeepFullyAtomisticElements", &SimulationSettings::keepFullyAtomisticElements)
		.def_readwrite("LatticeStaticsMode", &SimulationSettings::latticeStaticsMode)
		.def_readwrite("QCForceMode", &SimulationSettings::QCForceMode)
		.def_readwrite("InitialRefinementLevel", &SimulationSettings::InitialRefinementLevel)
		.def_readwrite("OptimizeOctreeCells", &SimulationSettings::OptimizeOctreeCells)
		.def_readwrite("OctreeMesh", &SimulationSettings::octreeMesh)
		.def_readwrite("QhullMesh", &SimulationSettings::qhullMesh)
		.def_readwrite("TetgenMesh", &SimulationSettings::tetgenMesh)
		.def_readwrite("QuadratureMethod", &SimulationSettings::quadratureMethod)
		.def_readwrite("OctreeElementSearch", &SimulationSettings::octreeElementSearch)
		.def_readwrite("SegmentTreeElementSearch", &SimulationSettings::segmentTreeElementSearch)
		.def_readwrite("QRVolumeBasedWeight", &SimulationSettings::qrVolBasedWeightFactor)
		.def_readwrite("OpenMPThread", &SimulationSettings::openMPThread)
		.def_readwrite("IndentorLocationX", &SimulationSettings::indentLocationX)
		.def_readwrite("IndentorLocationY", &SimulationSettings::indentLocationY)
		.def_readwrite("IndentorLocationZ", &SimulationSettings::indentLocationZ)
	;
	
	class_<Simulation, shared_ptr<Simulation>, noncopyable>("Simulation", init<>())
		.add_property("Settings", make_function(&Simulation::settings, return_internal_reference<>()))
		.add_property("Grains", make_function(&Simulation::grains, return_internal_reference<>()))
		.add_property("GrainCount", &Simulation::grainCount)
		.add_property("Mesh", make_function(&Simulation::mesh, return_internal_reference<>()))
		.add_property("SimulationCell", make_function(&Simulation::simulationCell, return_internal_reference<>()), &Simulation::setSimulationCell)
		.add_property("DeformedSimulationCell", make_function(&Simulation::deformedSimulationCell, return_internal_reference<>()), &Simulation::setDeformedSimulationCell)
		.def("AddGrain", &Simulation::addGrain)
		.add_property("Potential", make_function(&Simulation::potential, return_internal_reference<>()), &Simulation::setPotential)		
		.add_property("AllGroup", make_function(&Simulation::allGroup, return_internal_reference<>()))
		.def("CreateGroup", &Simulation::createGroup, return_internal_reference<>())
		.def("BreakGroup", &Simulation::breakGroup, return_internal_reference<>())
		.def("Group", &Simulation::group, return_internal_reference<>())
		.def("RegisterDisplacementBoundaryCondition",&Simulation::registerDisplacementBoundaryCondition)
		.def("RegisterForceBoundaryCondition",&Simulation::registerForceBoundaryCondition)
	;

	class_<Calculator, shared_ptr<Calculator>, bases<SimulationResource>, noncopyable>("Calculator", no_init)
		.def("CalculateEnergyAndForces", &Calculator::calculateEnergyAndForces)
		.def("CalculateEnergy", &Calculator::calculateEnergy)
		.def("CalculateForces", &Calculator::calculateForces)
		.def("CalculateCentrosymmetry", &Calculator::calculateCentrosymmetry) //, Calculator_calculateCentrosymmetry_overloads())
		.def("CalculateStresses", &Calculator::calculateStresses)
		.def("CalculateStiffness", &Calculator::calculateStiffness)
		.def("CalculateEigenvalues", &Calculator::calculateEigenvalues)
	;
	
	class_<EnergyQCCalculator, shared_ptr<EnergyQCCalculator>, bases<Calculator>, noncopyable>("EnergyQCCalculator", init<Simulation*>())
	;

	class_<EnergyQRCalculator, shared_ptr<EnergyQRCalculator>, bases<Calculator>, noncopyable>("EnergyQRCalculator", init<Simulation*>())
	;

	class_<ForceQCCalculator, shared_ptr<ForceQCCalculator>, bases<Calculator>, noncopyable>("ForceQCCalculator", init<Simulation*>())
	;
	
	class_<LatticeStaticsCalculator, shared_ptr<LatticeStaticsCalculator>, bases<Calculator>, noncopyable>("LatticeStaticsCalculator", init<Simulation*>())
	;
	
	class_<NumericalDerivativeCheck, noncopyable>("NumericalDerivativeCheck", init<CalculationResult&, const shared_ptr<Calculator>&>())
		.def("CheckFirstDerivative", &NumericalDerivativeCheck::checkFirstDerivative)
		.def("CheckSecondDerivative", &NumericalDerivativeCheck::checkSecondDerivative)
//		.def("CheckSecondDerivativeOld", &NumericalDerivativeCheck::checkSecondDerivativeOld)
	;
	
	class_< QVector<FloatType> >("QFloat", no_init)
		.def(QVector_readonly_indexing_suite< QVector<FloatType> >())	
	;
	
	class_< QVector<Vector3> >("QVector3", no_init)
		.def(QVector_readonly_indexing_suite< QVector<Vector3> >())		
	;
	
	class_< QVector<Matrix3> >("QMatrix3", no_init)
		.def(QVector_readonly_indexing_suite< QVector<Matrix3> >())		
	;

	class_<CalculationResult, bases<SimulationResource> >("CalculationResult", init<>())
		.add_property("TotalEnergy", &CalculationResult::totalEnergy)
		.add_property("Forces", make_function((const QVector<Vector3>& (CalculationResult::*)() const)&CalculationResult::forces, return_internal_reference<>()))
		.add_property("GetEvecs", make_function((const QVector<Vector3>& (CalculationResult::*)() const)&CalculationResult::evecs, return_internal_reference<>()))
		.add_property("Evals", make_function((const QVector<FloatType>& (CalculationResult::*)() const)&CalculationResult::evals, return_internal_reference<>()))
		.def("SetEvec", &CalculationResult::setEvec)
		.def("SetNumEvals", &CalculationResult::setNumEvals)
	;

	class_<Minimizer, shared_ptr<Minimizer>, noncopyable>("Minimizer", no_init)
		.add_property("Calculator", make_function(&Minimizer::calculator, return_internal_reference<>()))
	;

	class_<GSLMinimizer, shared_ptr<GSLMinimizer>, bases<Minimizer>, noncopyable>("Minimizer", no_init)
		.def("Iterate", &GSLMinimizer::iterate)
		.add_property("IsConverged", &GSLMinimizer::isConverged)		
		.add_property("MinimumValue", &GSLMinimizer::minimumValue)		
		.add_property("ForceNorm", &GSLMinimizer::forceNorm)		
		.add_property("NumberOfForceEvaluations", &GSLMinimizer::numberOfForceEvaluations)
		.add_property("NumberOfEnergyEvaluations", &GSLMinimizer::numberOfEnergyEvaluations)
		.add_property("StatusString", &GSLMinimizer::statusString)
		.add_property("CalculationOutput", make_function(&GSLMinimizer::calculationOutput, return_internal_reference<>()))
	;

	class_<CGMinimizer, shared_ptr<CGMinimizer>, bases<GSLMinimizer>, noncopyable>("CGMinimizer", 
		init<const shared_ptr<Calculator>&, FloatType, boost::python::optional<FloatType, FloatType> >(
		args("calculator", "convergenceNorm", "initialStepSize", "tolerance")))
	;

	class_<SteepestDescentMinimizer, shared_ptr<SteepestDescentMinimizer>, bases<GSLMinimizer>, noncopyable>("SteepestDescentMinimizer", 
		init<const shared_ptr<Calculator>&, FloatType, boost::python::optional<FloatType, FloatType> >(
		args("calculator", "convergenceNorm", "initialStepSize", "tolerance")))
	;

	class_<BFGSMinimizer, shared_ptr<BFGSMinimizer>, bases<GSLMinimizer>, noncopyable>("BFGSMinimizer", 
		init<const shared_ptr<Calculator>&, FloatType, boost::python::optional<FloatType, FloatType> >(
		args("calculator", "convergenceNorm", "initialStepSize", "sigma")))
	;

	class_<FIREMinimizer, shared_ptr<FIREMinimizer>, bases<Minimizer>, noncopyable>("FIREMinimizer", 
		init<const shared_ptr<Calculator>&, FloatType, boost::python::optional<FloatType, FloatType> >(
		args("calculator", "convergenceNorm", "initialStepSize", "maxStepSize")))
		.def("Iterate", &FIREMinimizer::iterate)
		.def("Minimize", &FIREMinimizer::minimize)
		.add_property("IsConverged", &FIREMinimizer::isConverged)		
		.add_property("MinimumValue", &FIREMinimizer::minimumValue)		
		.add_property("ForceNorm", &FIREMinimizer::forceNorm)		
		.add_property("NumberOfForceEvaluations", &FIREMinimizer::numberOfForceEvaluations)
		.add_property("NumberOfEnergyEvaluations", &FIREMinimizer::numberOfEnergyEvaluations)
		.add_property("StatusString", &FIREMinimizer::statusString)
		.add_property("CalculationOutput", make_function(&FIREMinimizer::calculationOutput, return_internal_reference<>()))
	;
	
	def("SaveSimulation", &saveSimulation);
	def("LoadSimulation", &loadSimulation);
}

};
