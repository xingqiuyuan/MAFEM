///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "mafem2python.h"

namespace Scripting {

using namespace boost::python;

void exportLinAlg();
void exportKernel();

struct QString_to_python_str {
	static PyObject* convert(const QString& s)	{
		return boost::python::incref(boost::python::object(std::string(s.toLocal8Bit().constData())).ptr());
	}
};

struct QString_from_python_str 
{
	QString_from_python_str() {
		boost::python::converter::registry::push_back(
			&convertible,
			&construct,
			boost::python::type_id<QString>());
	}

	static void* convertible(PyObject* obj_ptr)	{
		if (!PyString_Check(obj_ptr)) return 0;
		return obj_ptr;
	}

	static void construct(PyObject* obj_ptr, boost::python::converter::rvalue_from_python_stage1_data* data) {
		const char* value = PyString_AsString(obj_ptr);
		if (value == 0) boost::python::throw_error_already_set();
		void* storage = (
			(boost::python::converter::rvalue_from_python_storage<QString>*)
			data)->storage.bytes;
		new (storage) QString(value);
		data->convertible = storage;
	}
};

void ExceptionTranslator(const Exception& ex) {
	PyErr_SetString(PyExc_RuntimeError, ex.message().toLocal8Bit().constData());
}
  
BOOST_PYTHON_MODULE(mafem)
{
	register_exception_translator<Exception>(&ExceptionTranslator);
	
	boost::python::to_python_converter<
		QString,
		QString_to_python_str>();
	QString_from_python_str();
	
	exportLinAlg();
	exportKernel();
}

};
