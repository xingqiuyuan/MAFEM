///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __MAFEM_ATOM_H
#define __MAFEM_ATOM_H

#include <MAFEM.h>
#include <boundaryconditions/Group.h>
#include "LatticeSite.h"

namespace MAFEM {

/**
 * \brief An atom.
 * 
 * This atom class stores the lattice site, the current world position
 * of the atom and type of the atom.
 * 
 * The atom's lattice site defines the atom's position in the undeformed state. 
 * 
 * \author Alexander Stukowski
 */
class Atom : boost::noncopyable
{
public:

	/// \brief Deserialization constructor.
	/// 
	/// This constructor should not be used. 
	Atom() {}
	
	/// \brief Constructs an atom located at the given lattice site.
	/// \param site		The lattice site where the new atom should be located.
	inline Atom(const LatticeSite& site) : _latticeSite(site), _deformedPos(site.pos()), _lastNeighborPos(site.pos()), _groupFlags(1){}

	/// \brief Return the lattice site where this atom is located (in the undeformaed state).
	inline const LatticeSite& site() const { return _latticeSite; }

	/// \brief Returns the world position of this atom (in the undeformed state).	
	inline const Point3& pos() const { return _latticeSite.pos(); }

	/// \brief Sets the position of the atom to a new value in the undeformed state.
	/// \note The stored lattice indices are not modified by this method.
	inline void setPos(const Point3& newPos) { _latticeSite.setPos(newPos); }

	/// \brief Returns the world position of this atom (in the deformed state).
	inline const Point3& deformedPos() const { return _deformedPos; }

	/// \brief Computes the displacement of this atom (= difference between deformed and undeformed state).
	inline Vector3 displacement() const { return deformedPos() - pos(); }

	/// \brief Changes the deformed position of the atom.
	inline void setDeformedPos(const Point3& newPos) { _deformedPos = newPos; }

	/// \brief Changes only a single component of the deformed position of the atom.
	/// \param k The vector component (between 0 and 2)
	/// \param newValue the new value to be assigned to the given vector component.
	inline void setDeformedPosComponent(size_t k, FloatType newValue) { _deformedPos[k] = newValue; }

	/// \brief Sets the displacement vector of this atom.
	/// \note This recomputes the deformed position by adding the given displacement vector to the reference position.
	inline void setDisplacement(const Vector3& displacement) { _deformedPos = pos() + displacement; }

	/// \brief Returns the atom's position at the last time the neighbors lists have been rebuilt.
	const Point3& lastNeighborPos() const { return _lastNeighborPos; }

	/// \brief Saves the atom's position at the last time the neighbors lists have been rebuilt.
	void saveLastNeighborPos() { _lastNeighborPos = _deformedPos; }
		
	/// Sets the given group flag to make this atom belong to the given group.
	inline void addToGroup(const Group* group) { _groupFlags |= (1 << group->groupIndex()); }

	/// Clears the given group flag to make this atom no longer belong to the given group.
	inline void removeFromGroup(const Group* group) { 
		MAFEM_ASSERT_MSG(group->groupIndex() != 0, "RepAtom::removeFromGroup", "You may now remove a repatom from the special ALL group.");
		_groupFlags &= ~(1 << group->groupIndex()); 
	}

	/// Returns whether the atoms belongs to the given group.
	inline bool belongsToGroup(const Group* group) const { return (_groupFlags & (1 << group->groupIndex())) != 0; }

	void setStressComponent(int idx, FloatType value) { stress[idx] = value;}
	
	FloatType getStressComponent(int idx) { return stress[idx];}

	void setAtomicVolume(FloatType vol) { atomicVolume = vol;}
	
	FloatType getAtomicVolume() {return atomicVolume;}
	
	void setAtomicIndex(int Aindex) { index =  Aindex;}
	
	int getAtomicIndex(){return index;}
		
protected:

	/// The lattice site where this atom is located in the undeformed reference state.
	LatticeSite _latticeSite;

	/// The deformed position of the atom.
	Point3 _deformedPos;
	
	/// Stores the atom's position at the last time the neighbors lists have been rebuilt.
	Point3 _lastNeighborPos;
	
	/// This field stores the groups to which this atom belongs to.
	unsigned int _groupFlags;

	/**//// stores stress per atom value (11,22,33,12,13,23)
	FloatType   stress[6];

	FloatType atomicVolume;
	
	int index;

private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) {
		ar & _latticeSite;
		ar & _deformedPos;
		ar & _lastNeighborPos;
		ar & _groupFlags;
    }

	friend class boost::serialization::access;
};

}; // End of namespace MAFEM

#endif // __MAFEM_ATOM_H
