///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "LatticeSite.h"
#include <grain/CrystalLattice.h>

namespace MAFEM {

/******************************************************************************
* Defines a lattice site.
* The indices of the lattice site are computed from the given world coordinates.
* If the world point is not exactly on a lattice site then it is rounded to the nearest one.
******************************************************************************/
LatticeSite::LatticeSite(CrystalLattice* lattice, const Point3& point) : _lattice(lattice)
{
	CHECK_POINTER(lattice);

	// Back transform world point to lattice space.
	Point3 u = lattice->inverseLatticeVectors() * point;

	// Round to integer position.
	Point3I r((int)floor(u.X+0.5), (int)floor(u.Y+0.5), (int)floor(u.Z+0.5));

	// Test if the point is exactly on the lattice site.
	_pos = lattice->latticeVectors() * r;
	if(_pos.equals(point, FLOATTYPE_EPSILON)) {
		_latticeIndices = r;
		return;
	}

	// Find the distance to the closest lattice site.
	FloatType r2best = FLOATTYPE_MAX;	
	Point3I s;
	for(s.X=r.X - 1; s.X <= r.X + 1; s.X++) {
		for(s.Y=r.Y - 1; s.Y <= r.Y + 1; s.Y++) {
			for(s.Z=r.Z - 1; s.Z <= r.Z + 1; s.Z++) {
				Point3 p2 = lattice->latticeVectors() * s;
				FloatType r2 = DistanceSquared(point, p2);
				if(r2 < r2best) {
					r2best = r2;
					_latticeIndices = s;					
				}
			}
		}
	}
	MAFEM_ASSERT(r2best != FLOATTYPE_MAX);

	// Compute actual position of closest lattice site.
	_pos = lattice->latticeVectors() * _latticeIndices;
}


}; // End of namespace MAFEM
