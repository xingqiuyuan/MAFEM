///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_ATOM_COLLECTION_BASE_H
#define __MAFEM_ATOM_COLLECTION_BASE_H

#include <MAFEM.h>
#include <atoms/LatticeSite.h>

namespace MAFEM {

/**
 * \brief Base class that stores a list of atoms and provides
 *        a fast lookup mechanism for lattice sites.
 * 
 * \author Alexander Stukowski
 */
template<class AtomType>
class AtomCollectionBase : public QVector<AtomType*>
{
public:

	/// \brief Default constructor that creates an empty collection.
	AtomCollectionBase() {}
	
	/// \brief Copy constructor.
	/// \note This copy constructor is only needed for the Q_FOREACH macro. It does
	///       not copy the hashtable, only the plain list of atoms.
	AtomCollectionBase(const AtomCollectionBase<AtomType>& other) : QVector<AtomType*>(other) {}

	/// \brief Returns the atom that is located on the given lattice site.
	/// \return The atom on the given lattice site or NULL if the site is not occupied.
	AtomType* findAt(const LatticeSite& site) const {
		for(typename QVector<LatticePopulation>::const_iterator pop = latticePopulations.begin(); pop != latticePopulations.end(); ++pop) {
			if(pop->lattice == site.lattice()) {
				AtomType* atom = pop->findAt(site.indices());
				if(atom) {
					MAFEM_ASSERT(atom->site().lattice() == site.lattice() && atom->site().indices() == site.indices());
					return atom;
				}
				break;
			}
		}
		return NULL;
	}

	/// \brief Adds an atom to the collection.
	/// \param atom	The atom to be added to the collection.
	void push_back(AtomType* atom) {
		CHECK_POINTER(atom);
		QVector<AtomType*>::push_back(atom);
		insert_impl(atom);
	}

	/// Removes all nodes from the collection.
	void clear() {
		QVector<AtomType*>::clear();
		for(typename QVector<LatticePopulation>::iterator pop = latticePopulations.begin(); pop != latticePopulations.end(); ++pop)
			pop->clear();
	}
	
private:

	/// \brief Inserts an atom into the internal lookup table.
	/// \param atom	The atom to be added to the collection.
	void insert_impl(AtomType* atom) {
		// Find the lattice this node belongs to and add the node to the associated lookup table. 
		for(typename QVector<LatticePopulation>::iterator pop = latticePopulations.begin(); pop != latticePopulations.end(); ++pop) {
			if(pop->lattice == atom->site().lattice()) {
				pop->add(atom);
				return;
			}
		}
		// Add a new LatticePopulation entry for the new lattice.
		latticePopulations.resize(latticePopulations.size() + 1);
		latticePopulations.back().lattice = atom->site().lattice();
		latticePopulations.back().add(atom);
	}

    /// This key extractor extracts the lattice indices from an RepAtom instance.
	struct site_extractor {
		typedef Point3I result_type;
		const Point3I& operator()(const AtomType& a) const { return a.site().indices(); }
		const Point3I& operator()(const AtomType* a) const { return a->site().indices(); }
	};

	/// Definition of a hashtable for atoms.
	typedef boost::multi_index::multi_index_container<
		AtomType*,
		boost::multi_index::indexed_by<
			boost::multi_index::hashed_unique<
				site_extractor
			>
		>
	>
	site_locator;

	/// Stores the population of one lattice.
	struct LatticePopulation {
		CrystalLattice* lattice;	///< The lattice.		
		site_locator locator;		///< A hash table that stores all atoms.

		/// \brief Returns the atom that is located on the lattice site with the given indices.
		/// \return The atom on the given lattice site or NULL if the site is not occupied.
		AtomType* findAt(const Point3I& indices) const {
			typename site_locator::iterator iter = locator.find(indices);
			if(iter == locator.end()) return NULL;
			return *iter;
		}

		/// \brief Creates an entry in the lookup table.
		/// \param atom The atom that is placed on the lattice.
		void add(AtomType* atom) {
			MAFEM_ASSERT(lattice == atom->site().lattice());
			locator.insert(atom);
		}

		/// \brief Clears the lookup table.
		void clear() { locator.clear(); }
	};

	QVector<LatticePopulation> latticePopulations;
	
private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) {
    	// Save/load nodes from the vector.
    	ar & boost::serialization::base_object< QVector<AtomType*> >(*this);
    	// After loading, also insert them into the internal lookup tables.
    	if(typename Archive::is_loading() == true) {
    		for(typename QVector<AtomType*>::iterator iter = this->begin(); iter != this->end(); iter++) 
    			insert_impl(*iter);    		
    	} 
    }
	friend class boost::serialization::access;
};

}; // End of namespace MAFEM

#endif // __MAFEM_ATOM_COLLECTION_BASE_H
