///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "SamplingAtomCollection.h"
#include <simulation/Simulation.h>
#include <mesh/Mesh.h>
#include <atoms/SamplingAtom.h>
#include <material/EAMPotential.h>

namespace MAFEM {

/// Controls how many neighbor list pointers are stored in one memory page.
const int SamplingAtomCollection::neighborListPageSize = 65536;


/******************************************************************************
* This rebuilds the neighbor lists for the sampling atoms. 
******************************************************************************/
void SamplingAtomCollection::buildNeighborLists(Simulation* sim)
{
	MsgLogger() << "  Building atom neighbor lists." << endl;
	
	const FloatType cutoffRadius = sim->potential()->cutoffRadius + sim->settings().neighborSkinThickness;
	const FloatType cutoffRadiusSquared = cutoffRadius * cutoffRadius;
	MsgLogger() << "    Neighbor list cutoff radius:" << cutoffRadius << endl;	
	
	if(cutoffRadius <= 0.0)
		throw Exception("Neighbor cutoff radius must be positive.");	

	// Measure the time it taskes to build the neighbor lists.
	timer neighborTimer;	

	// Clear previous neighbor list memory.
	_memoryPages.clear();
	
	// Copy the simulation cell information for quick access.
	const AffineTransformation simCell = sim->deformedSimulationCell();
	if(simCell.determinant() <= 1e-5f)
		throw Exception("Simulation cell is degenerate.");
	const AffineTransformation simCellInverse = sim->inverseDeformedSimulationCell();
	
	// Calculate the number of bins required in each spatial direction.
	int binDim[3] = {1,1,1};
	const AffineTransformation m = AffineTransformation::scaling(cutoffRadius) * simCellInverse;
	Matrix3 binCell;
	for(int i=0; i<NDOF; i++) {
		binDim[i] = (int)(Length(simCell.column(i)) / cutoffRadius);
		binDim[i] = min(binDim[i], (int)(1.0 / Length(m.column(i))));
		binDim[i] = min(binDim[i], 50);
		binDim[i] = max(binDim[i], 1);
		binCell.column(i) = simCell.column(i) / (FloatType)binDim[i];
	}
	
	/// An 3d array of bins. Each bin is a linked list of atoms.
	typedef multi_array<SamplingAtom*, NDOF> BinsArray;
	typedef BinsArray::iterator iterator1;
	typedef subarray_gen<BinsArray,2>::type::iterator iterator2;
	typedef subarray_gen<BinsArray,1>::type::iterator iterator3;
	BinsArray bins(extents[binDim[0]][binDim[1]][binDim[2]]);
	
	// Clear bins.
	for(iterator1 iter1 = bins.begin(); iter1 != bins.end(); ++iter1)
		for(iterator2 iter2 = (*iter1).begin(); iter2 != (*iter1).end(); ++iter2)
			for(iterator3 iter3 = (*iter2).begin(); iter3 != (*iter2).end(); ++iter3)
				(*iter3) = NULL;	
	
	// Put all atoms into bins.
	Q_FOREACH(SamplingAtom* atom, sim->mesh().samplingAtoms()) {
		
		// Transform atom position from absolute coordinates to reduced coordinates.
		Point3 reducedp = simCellInverse * atom->deformedPos();
		
		int indices[3];
		for(int k=0; k<NDOF; k++) {
			// Determine the atom's bin from its position.
			indices[k] = (int)(reducedp[k] * binDim[k]);
			// Clamp atom position to be inside simulation cell.
			if(indices[k] < 0) indices[k] = 0;
			else if(indices[k] >= binDim[k]) indices[k] = binDim[k]-1; 
		}
		
		// Put atom into its bin.
		SamplingAtom** binList = &bins[indices[0]][indices[1]][indices[2]];
		atom->_nextInBin = *binList;
		*binList = atom;		
	}
	
	int memoryPagePosition = neighborListPageSize;
	SamplingAtom** memoryPagePointer = NULL;
	
	int nactiveAtoms = 0;
	int ntotalneighbors = 0;
	int numNeighbors;
	int maxNumNeighbors = 0;
	const bool QCForceMode = sim->settings().QCForceMode;
	
	// Generate neighbor list for each active sampling atom.
	Q_FOREACH(SamplingAtom* atom1, sim->mesh().samplingAtoms()) {
		
		// The neighbor lists are only generated for active sampling atoms unless
		// the force-based QC scheme is enabled. In this case we need the neighbor lists
		// for the passive atoms as well.
		if(atom1->isPassive() && !QCForceMode) continue;
		
		nactiveAtoms++;

		// Allocate memory for the neighbor list for the current atom.
		if(memoryPagePosition > neighborListPageSize - MAFEM_MAXIMUM_NEIGHBOR_COUNT) {
			_memoryPages.push_back(shared_array<SamplingAtom*>(new SamplingAtom*[neighborListPageSize]));
			memoryPagePointer = _memoryPages.back().get();
			memoryPagePosition = 0;
		}
		atom1->_neighborsBegin = memoryPagePointer;
		
		// Calculate the bin the atom is located in and in what other bins we have to look for neighbors.
		
		// Transform atom position from absolute coordinates to reduced coordinates.
		Point3 reducedp = simCellInverse * atom1->deformedPos();		
		int indices_min[3], indices_max[3];	
		for(int k=0; k<NDOF; k++) {
			// Determine the atom's bin from its position.
			int index = (int)(reducedp[k] * binDim[k]);
			indices_min[k] = min(max(index-1, 0), binDim[k]-1);
			indices_max[k] = min(max(index+1, 0), binDim[k]-1);
		}
		
		numNeighbors = 0;
		
		// Loop over each of the neighbor bins.
		int indices[3];
		for(indices[0] = indices_min[0]; indices[0] <= indices_max[0]; indices[0]++) {
			for(indices[1] = indices_min[1]; indices[1] <= indices_max[1]; indices[1]++) {
				for(indices[2] = indices_min[2]; indices[2] <= indices_max[2]; indices[2]++) {
					SamplingAtom* bin = bins[indices[0]][indices[1]][indices[2]];
					// Loop over all atoms in the current bin.
					for(SamplingAtom* atom2 = bin; atom2 != NULL; atom2 = atom2->_nextInBin) {
						if(atom2 == atom1) continue;
							
						const FloatType distSquared = DistanceSquared(atom1->deformedPos(), atom2->deformedPos());
						if(distSquared > cutoffRadiusSquared) continue;
						
						// Insert atom 2 into the neighbor list of atom 1.
						if(memoryPagePointer - atom1->_neighborsBegin >= MAFEM_MAXIMUM_NEIGHBOR_COUNT)
							throw Exception("Neighbor list overflow. Atom has too many neighbors.");
						MAFEM_ASSERT(memoryPagePosition < neighborListPageSize);
						memoryPagePosition++;
						*memoryPagePointer++ = atom2;
						ntotalneighbors++;
						numNeighbors++;
					}
				}
			}
		}
		
		if(numNeighbors > maxNumNeighbors)
			maxNumNeighbors = numNeighbors;
		
		atom1->_neighborsEnd = memoryPagePointer;
	}
	
	// Save the current position of each repatom to detect the next time the neighbor lists have to be rebuilt.
	Q_FOREACH(RepAtom* repatom, sim->mesh().repatoms()) {
		repatom->saveLastNeighborPos();
	}
	
	MsgLogger() << "    Total time for neighbor lists:" << neighborTimer.elapsed() << "sec." << endl;
	MsgLogger() << "    Average number of neighbors per atom:" << FloatType(ntotalneighbors) / FloatType(nactiveAtoms) << endl;
	MsgLogger() << "    Maximum number of neighbors per atom:" << maxNumNeighbors << endl;
	MsgLogger() << "    Allocated memory for neighbor lists:" << (_memoryPages.size() * neighborListPageSize * sizeof(SamplingAtom*) / 1024) << "KB" << endl;
}

}; // End of namespace MAFEM
