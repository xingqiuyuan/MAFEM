///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_LATTICE_SITE_H
#define __MAFEM_LATTICE_SITE_H

#include <MAFEM.h>
#include <grain/CrystalLattice.h>

namespace MAFEM {

/**
 * \brief Describes a single site in a perfect crystal lattice.
 * 
 * A lattice site is completely defined by three integer indices.
 * An addition to the indicess the LatticeSite class stores
 * the site position in world space (as a Point3) and a pointer
 * to the lattice to which the site belongs.
 * 
 * \author Alexander Stukowski
 */
class LatticeSite
{
public:

	/// \brief Deserialization constructor.
	/// 
	/// This constructor should not be used. 
	LatticeSite() {}
	
	/// \brief Defines a lattice site by three integer indices.
	///
	/// The position of the lattice site in world space is computed from the lattice vectors and the indices.
	///
	/// \param lattice		The associated lattice that defines the mapping from indices to world space.
	/// \param indices		The three indices specifying the site in the lattice.
	inline LatticeSite(CrystalLattice* lattice, const Point3I& indices) : _lattice(lattice), _latticeIndices(indices) {
		CHECK_POINTER(lattice);
		// Transform the site from index space to world space.
		setPos(lattice->latticeVectors() * Point3(indices.X, indices.Y, indices.Z));
	}

	/// \brief Defines a lattice site by world position.
	///
	/// The indices of the lattice site are computed from the given world coordinates.
	/// If the world point is not exactly on a lattice site then it is rounded to the nearest one.
	///
	/// \param lattice		The associated lattice that defines the mapping from indices to world space.
	/// \param point		A point given in world coordinates.
	LatticeSite(CrystalLattice* lattice, const Point3& point);

	/// \brief Returns the lattice indices of this site.
	inline const Point3I& indices() const { return _latticeIndices; }

	/// \brief Returns the location of the lattice site in world space.
	inline const Point3& pos() const { return _pos; }

	/// \brief Sets the world position of the site to a new value.
	/// \note The stored indices are not modified by this method.
	inline void setPos(const Point3& newPos) { _pos = newPos; }

	/// \brief Returns the associated lattice.
	inline CrystalLattice* lattice() const { return _lattice; }

protected:

	/// The associated lattice that provides the mapping from indices to world space.
	CrystalLattice* _lattice;

	/// The three indices that identify this site in the associated CrystalLattice.
	Point3I _latticeIndices;

	/// The location of the lattice site in the world coordinate system.
	Point3 _pos;

	
private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) {
		ar & _lattice;
		ar & _latticeIndices;
		ar & _pos;
    }

	friend class boost::serialization::access;	
};


}; // End of namespace MAFEM

#endif // __MAFEM_LATTICE_SITE_H

