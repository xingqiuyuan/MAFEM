///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_FLAGS_H
#define __MAFEM_FLAGS_H

namespace MAFEM {

/// The maximum number of flags
#define MAX_NUM_FLAGS 32

/******************************************************************************
* This is a handy class to subclass off of when you're designing a 
* class with flags. 
* It contains one private data member, an unsigned int, with the flag info. 
* It then implements a bunch of handy flag-related functions. 
*
* Note that this struct has no constructor. Thta means you have to initialize
* the flags from the constructor of your derived class. 
******************************************************************************/
class ObjectWithFlags
{
public:
    /// Default constructor that sets all flags to zero.
	ObjectWithFlags() : flags(0) {}

    /// Constructor that sets the given flags.
	ObjectWithFlags(unsigned int f) : flags(f) {}

    /// Copy Constructor.
	ObjectWithFlags(const ObjectWithFlags& f) : flags(f.flags) {}

	/// Sets flags. Each bit that is set in f is assigned the given value.
	void SetFlag(unsigned int f, bool value = true) { if(value) flags |= f; else flags &= ~f; }

	/// Clears flags. Each bit that is set in f is cleared.
	void ClearFlag(unsigned int f) { flags &= ~f; }

	/// Returns true if any of the bits set in f are set in this flag object; false if none of them are.
	bool GetFlag(unsigned int f) const { return (flags & f) != 0; }

	/// Sets flags to 0, clearing all flag bits.
	void ClearAllFlags() { flags = 0; }

	/// Returns the unsigned int that stores flags.
	unsigned int GetFlags() const { return flags; }

	/// Copies all flags bits over from f.
	void CopyFlags(unsigned int f) { flags = f; }

private:

	/// The flag bits.
    unsigned int flags;

    template<class Archive>
        void serialize(Archive &ar, const unsigned int version) {
			ar & flags;
		}

    friend class boost::serialization::access;

};

};

#endif // __MAFEM_FLAGS_H
