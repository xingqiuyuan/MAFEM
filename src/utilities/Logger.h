///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __LOGGER_H
#define __LOGGER_H

namespace Base {

class LoggerObject
{
private:
    QTextStream ts;
	bool _space;
	bool active;
	LoggerObject(const LoggerObject&) {}
	
public:
    inline LoggerObject(QIODevice *device, bool enabled = true) : ts(device), _space(true), active(enabled) {}
    
    inline bool isEnabled() const { return active; }
    inline void setEnabled(bool b) { active = b; }     
    
    inline LoggerObject &space() { _space = true; if(isEnabled()) ts << " "; return *this; }
    inline LoggerObject &nospace() { _space = false; return *this; }
    inline LoggerObject &maybeSpace() { if(_space && isEnabled()) ts << " "; return *this; }

    inline LoggerObject &operator<<(QChar t) { if(isEnabled()) ts << "\'" << t << "\'"; return maybeSpace(); }
    inline LoggerObject &operator<<(bool t) { if(isEnabled()) ts << (t ? "true" : "false"); return maybeSpace(); }
    inline LoggerObject &operator<<(char t) { if(isEnabled()) ts << t; return maybeSpace(); }
    inline LoggerObject &operator<<(signed short t) { if(isEnabled()) ts << t; return maybeSpace(); }
    inline LoggerObject &operator<<(unsigned short t) { if(isEnabled()) ts << t; return maybeSpace(); }
    inline LoggerObject &operator<<(signed int t) { if(isEnabled()) ts << t; return maybeSpace(); }
    inline LoggerObject &operator<<(unsigned int t) { if(isEnabled()) ts << t; return maybeSpace(); }
    inline LoggerObject &operator<<(signed long t) { if(isEnabled()) ts << t; return maybeSpace(); }
    inline LoggerObject &operator<<(unsigned long t) { if(isEnabled()) ts << t; return maybeSpace(); }
    inline LoggerObject &operator<<(qint64 t) { if(isEnabled()) ts << QString::number(t); return maybeSpace(); }
    inline LoggerObject &operator<<(quint64 t) { if(isEnabled()) ts << QString::number(t); return maybeSpace(); }
    inline LoggerObject &operator<<(float t) { if(isEnabled()) ts << t; return maybeSpace(); }
    inline LoggerObject &operator<<(double t) { if(isEnabled()) ts << t; return maybeSpace(); }
    inline LoggerObject &operator<<(const char* t) { if(isEnabled()) ts << QString::fromAscii(t); return maybeSpace(); }
    inline LoggerObject &operator<<(const QString & t) { if(isEnabled()) ts << "\"" << t << "\""; return maybeSpace(); }
    inline LoggerObject &operator<<(const QLatin1String &t) { if(isEnabled()) ts << "\""  << t.latin1() << "\""; return maybeSpace(); }
    inline LoggerObject &operator<<(const QByteArray & t) { if(isEnabled()) ts  << "\"" << t << "\""; return maybeSpace(); }
    inline LoggerObject &operator<<(const void * t) { if(isEnabled()) ts << t; return maybeSpace(); }
    inline LoggerObject &operator<<(QTextStreamFunction f) {
        if(isEnabled()) ts << f;
        return *this;
    }

    inline LoggerObject &operator<<(QTextStreamManipulator m) { if(isEnabled()) ts << m; return *this; }
};

template <class T>
inline LoggerObject &operator<<(LoggerObject& log, const QList<T> &list)
{
    log.nospace() << "(";
    for (Q_TYPENAME QList<T>::size_type i = 0; i < list.count(); ++i) {
        if (i)
            log << ", ";
        log << list.at(i);
    }
    log << ")";
    return log.space();
}

template <typename T>
inline LoggerObject &operator<<(LoggerObject& log, const QVector<T> &vec)
{
    log.nospace() << "QVector";
    return operator<<(log, vec.toList());
}

template <class aKey, class aT>
inline LoggerObject &operator<<(LoggerObject& log, const QMap<aKey, aT> &map)
{
    log.nospace() << "QMap(";
    for (typename QMap<aKey, aT>::const_iterator it = map.constBegin();
         it != map.constEnd(); ++it) {
        log << "(" << it.key() << ", " << it.value() << ")";
    }
    log << ")";
    return log.space();
}

template <class T1, class T2>
inline LoggerObject &operator<<(LoggerObject& log, const QPair<T1, T2> &pair)
{
    log.nospace() << "QPair(" << pair.first << "," << pair.second << ")";
    return log.space();
}

/// Writes the current date/time to the output stream for logging purposes.
QTextStream& logdate(QTextStream& s);

extern LoggerObject __msg_logger;
extern LoggerObject __verbose_msg_logger;

/// Returns the global logging stream.
/// Everything written to this stream is sent to stderr.
inline LoggerObject& MsgLogger() { return __msg_logger; }

/// Returns the global logging stream for verbose messages.
/// Everything written to this stream is sent to stderr if verbose messages are turned on.
inline LoggerObject& VerboseLogger() { return __verbose_msg_logger; }

};

#endif // __LOGGER_H
