///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
/******************************************************************************
* Defines macros for debugging purposes.
******************************************************************************/

#ifndef __DEBUGGING_H
#define __DEBUGGING_H

/******************************************************************************
* Activate the debug heap.
******************************************************************************/
#if defined(_DEBUG) && defined(Q_CC_MSVC)  // Microsoft Visual C++
#include <crtdbg.h>
#ifndef DEBUG_NEW
#define DEBUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#endif
#else
#ifndef DEBUG_NEW
#define DEBUG_NEW new
#endif
#endif

namespace Base {

/******************************************************************************
* This macro performs a runtime-time check.
******************************************************************************/
#define MAFEM_ASSERT Q_ASSERT

/******************************************************************************
* This macro performs a runtime-time check.
******************************************************************************/
#define MAFEM_ASSERT_MSG Q_ASSERT_X

/******************************************************************************
* This macro performs a compile-time check.
******************************************************************************/
#define MAFEM_STATIC_ASSERT(condition) BOOST_STATIC_ASSERT(condition)

/******************************************************************************
* This macro validates a memory pointer in debug mode.
* If the given pointer does not point to a valid position in memory then
* the debugger is activated.
******************************************************************************/
#ifdef _DEBUG

	#ifdef Q_CC_MSVC  // Microsoft Visual C++

		template <typename T>
		inline bool _IsValidPointer(T* objPointer) { 
			return _CrtIsValidPointer(objPointer, sizeof(T), true); 
		}
		
		#define CHECK_POINTER(pointer) MAFEM_ASSERT_MSG(_IsValidPointer(pointer), "CHECK_POINTER", "Invalid object pointer.");

	#else		// Non Microsoft compiler

		#define CHECK_POINTER(pointer) MAFEM_ASSERT_MSG((pointer) != NULL, "CHECK_POINTER", "Invalid object pointer.");

	#endif

#else	// in release mode

	#define CHECK_POINTER(pointer)

#endif

// Checks if a floating-point number is valid.
template<typename T>
inline bool _finite(T f) { 
	return f != numeric_limits<T>::infinity() && 
		f != numeric_limits<T>::signaling_NaN() &&
		f != numeric_limits<T>::quite_NaN(); 
}
	
};	// End of namespace Base

#endif // __DEBUGGING_H
