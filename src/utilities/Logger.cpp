///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include <Base.h>
#include <utilities/Logger.h>

namespace Base {
	
QIODevice* GetStdErrFile() {
	static QFile stderrFile;
	static bool initialized = false;
	if(!initialized) {
		stderrFile.open(stderr, QIODevice::WriteOnly);
		initialized = true;
	}
	return &stderrFile;
}
	
LoggerObject __msg_logger(GetStdErrFile());
LoggerObject __verbose_msg_logger(GetStdErrFile(), false);

/// Writes the current date/time to the output stream for logging purposes.
QTextStream& logdate(QTextStream& s) 
{
	return s << '[' << QTime::currentTime().toString(Qt::ISODate) << "] ";
}

};	// End of namespace Base
