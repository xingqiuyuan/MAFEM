/***********************************************************************\
|                                                                       |
| Multiscale Atomistic Finite Element Method (MAFEM) Package.           |
|                                                                       |
| Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>            |
| All rights reserved.                                                  |
|                                                                       |
| Initial developer: Alexander Stukowski <alex@stukowski.de>            |
| Modifications contributed by                                          |
|   Nirav Prajapati <nirav.prajapati@rub.de>                            |
|   Bernhard Eidel <bernhard.eidel@googlemail.com>                      |
|   Benedikt Zier <benediktzier@yahoo.de>                               |
|                                                                       |
| No redistribution and use in source and binary forms, with or without |
| modification, without prior permission by all contributing authors.   |
|                                                                       |
\***********************************************************************/

#ifndef __MAFEM_NUMERICAL_DERIVATIVE_CHECK_H
#define __MAFEM_NUMERICAL_DERIVATIVE_CHECK_H

#include <MAFEM.h>
#include "CalculationResult.h"
#include "Calculator.h"
//#include "FEAST/matrix_io.h"
// #include "FEAST/stopwatch.h"

namespace MAFEM {

class NumericalDerivativeCheck : boost::noncopyable
{
public:

	/// Constructor.
	/// Initializes the object.
	NumericalDerivativeCheck(CalculationResult& output, const shared_ptr<Calculator>& calculator) : output0(output), calculator(calculator)
	{
		//atoms = calculator.Atoms();
		F.params = this;
		symmetry = true;
	}
		
	/// Computes the numerical first order derivatives with respect to all degrees of freedom
	/// and compares them to the analytical ones.
	/// Throws an exception if they are not equal.
	void checkFirstDerivative(double h)
	{
		MsgLogger() << "Checking analytical and numerical first derivative." << endl;
		
//		progress_display progress(calculator->mesh().nodes().size() * NDOF);

		/// Calculate Energy and Forces.
		calculator->calculateEnergyAndForces(output0);
		
//		MsgLogger() << "analytical, numerical, abserr, relative difference" << endl;

		F.function = &Function1;
		double errorRMS = 0;
		int numFails = 0;
		for (int a1 = 0; a1 < calculator->mesh().nodes().size(); ++a1)
		{
			atom1 = calculator->mesh().nodes()[a1];
			for (dof1 = 0; dof1 < NDOF; ++dof1)
			{
				/// Compute gradient numerically.
				double numerical, abserr;
				double x = atom1->deformedPos()[dof1];
				gsl_deriv_central(&F, x, h, &numerical, &abserr);
				if (output0.reforces()[atom1->index()][dof1])
					numerical = 0;
				
				double analytical = -output0.forces()[atom1->index()][dof1];
				double relerr = 0;
				if (numerical)
					relerr = abs((analytical-numerical)/numerical);
				errorRMS += pow(relerr, 2);
//				MsgLogger() << analytical << ", " << numerical << ", " << abserr << ", " << relerr*100 << " %" << endl;
				MAFEM_ASSERT_MSG(std::abs(numerical - analytical) <= abserr, "NumericalDerivativeCheck::Check1()", "Numerical derivative check failed."); 
				if (std::abs(numerical - analytical) > abserr)
				{			
					MsgLogger() << "Numerical derivative check failed." << endl;
					MsgLogger() << "Analytical value = " << analytical << endl;
					MsgLogger() << "Numerical value = " << numerical << " (with estimated error " << abserr << ")" << endl;
					MsgLogger() << "at atom " << a1 << " (DOF " << dof1 << ")" << endl;
					++numFails;
				}
//				++progress;
			}
		}
		errorRMS = sqrt(errorRMS / (calculator->mesh().nodes().size() * NDOF));
		MsgLogger() << "RMS relerr =" << errorRMS << endl;
		MsgLogger() << "Number of fails =" << numFails << endl;
	}

	/// Computes the numerical second order derivatives with respect to all degrees of freedom
	/// and compares them to the analytical ones.
	/// Throws an exception if they are not equal.
	void checkSecondDerivative(double h, double e)
	{
//		cout << "Checking analytical and numerical second derivative. (h = " << h << ")" << endl;
		
		calculator->calculateEnergyAndForces(output0);

		/// Calculate analytical Stiffness.
		//calculator->calculateStiffness(output0);
		
		size_t numNodes = calculator->mesh().nodes().size();
		
		MsgLogger() << logdate << "Building stiffness matrix. (h = " << h << ", e = " << e << ")" << endl;
		progress_display progress(numNodes);
//		StopWatch buildTime;
//		buildTime.Start();
		
		/// Allocate Stiffness Matrix
		if (output0.numericalCRSStiffnessMatrix.N > 0)
			output0.numericalCRSStiffnessMatrix.deallocate_elements();
		output0.numericalCRSStiffnessMatrix.allocate_elements(numNodes * NDOF, 1);

		/// Build Stiffness Matrix
		Vector3 negGradF;
		for (size_t a2 = 0; a2 < numNodes; ++a2)
		{
			atom2 = calculator->mesh().nodes()[a2];
			for (dof2 = 0; dof2 < NDOF; ++dof2)
			{
//				disturbAtomForward(h);
				disturbAtomCentral(h);
				
				for (size_t a1 = 0; a1 < numNodes; ++a1)
				{
//					cout << output0.forces()[a1] << ", " << output1.forces()[a1] << endl;
//					negGradF = (output0.forces()[a1] - output1.forces()[a1]) / h;
					negGradF = (output2.forces()[a1] - output1.forces()[a1]) / (2 * h);
					
					for (dof1 = 0; dof1 < NDOF; ++dof1)
						if (abs(negGradF[dof1]) > e)
							output0.numericalCRSStiffnessMatrix.insertValue(negGradF[dof1], NDOF * a1 + dof1, NDOF * a2 + dof2); // CRS
				}
			}
			++progress;
		}
		
//		buildTime.Stop();
//		cout << "Build Time: " << buildTime.GetDuration() << " s" << endl;

		/// Apply displacement boundary conditions.
		std::vector<int> dirichletNodes;
		for (size_t i = 0; i < numNodes; ++i)
			for (DisplacementBoundaryConditionList::const_iterator displBC = calculator->simulation()->displacementBC().constBegin(); displBC != calculator->simulation()->displacementBC().constEnd(); ++displBC)
				if (calculator->simulation()->mesh().repatoms()[i]->belongsToGroup((*displBC)->group()))
				{
					dirichletNodes.push_back(i);
					break;
				}
		int numDirichletNodes = dirichletNodes.size();
		cout << "Apply displacement boundary conditions for " << numDirichletNodes << " nodes." << endl;

		if (numDirichletNodes)
		{
			index_matrix dirichlet;
			dirichlet.allocate_and_clear(numDirichletNodes, 1 + NDOF);
			for (int i = 0; i < numDirichletNodes; ++i)
			{
				dirichlet.element[i][0] = dirichletNodes[i];
				for (DisplacementBoundaryConditionList::const_iterator displBC = calculator->simulation()->displacementBC().constBegin(); displBC != calculator->simulation()->displacementBC().constEnd(); ++displBC)
					if (calculator->simulation()->mesh().repatoms()[dirichletNodes[i]]->belongsToGroup((*displBC)->group()))
						for (int dof = 0; dof < NDOF; ++dof)
							dirichlet.element[i][dof + 1] |= (int)(*displBC)->isFixed(dof);
//							if((*displBC)->isFixed(dof))
//								dirichlet.element[i][dof + 1] = 1;
			}

			output0.numericalCRSStiffnessMatrix.dirichlet_boundary(dirichlet); // CRS
			dirichlet.deallocate();
		}
		output0.numericalCRSStiffnessMatrix.PrintInfo();
/*		
		matrix fullMatrix = output0.numericalCRSStiffnessMatrix.MakeFullMatrix();
		char *file_name;
		file_name = "/home/gk684/MAFEM_2013_09_01/InputFiles/Test/SmallCube/numericalStima.dat";
		//matrix_write_to_file(fullMatrix, file_name);

		ofstream file;
		file.open(file_name, ios::out);
		if (file.is_open())
		{
			file << std::setprecision(8);
			file << std::scientific;
			for(unsigned int i = 0 ; i < fullMatrix.M ; i++)
			{
				for(unsigned int  j = 0 ; j < fullMatrix.N; j++)
				{
					file << fullMatrix.element[i][j] <<" ";
				}
				file << "\n";
			}
			file.close();
		}
*/		
		/// Check symmetry
		output0.numericalCRSStiffnessMatrix.checkSymmetry();
	}

#if 0		
	void checkSecondDerivativeOld(FloatType h, bool lengthCriterion)
	{
		MsgLogger() << "Checking analytical and numerical second derivative." << endl;
		
		// Calculate Stiffness.
		calculator->calculateStiffness(output0);
		
		size_t numNodes = calculator->mesh().nodes().size();
		
		progress_display progress(numNodes);
		
		output0.numericalStiffnessMatrix().resize(pow(numNodes, 2));
		memset(output0.numericalStiffnessMatrix().data(), 0, output0.numericalStiffnessMatrix().size() * sizeof(Matrix3));
		
//		MsgLogger() << "analytical, numerical, abserr, relative difference" << endl;

		F.function = &Function2;
		
		FloatType cutoffRadius = calculator->simulation()->potential().get()->cutoffRadius;
		FloatType maxElementVolume = 0;
		FloatType maxElementEdgeLength = cutoffRadius / 2;
		if (calculator->mesh().elements().size())
		{
			MsgLogger() << "Elements found" << endl;
			for (int i = 0; i < calculator->mesh().elements().size(); ++i)
			{
				FloatType ElementVolume = calculator->mesh().elements()[i]->volume();
				if (ElementVolume > maxElementVolume)
					maxElementVolume = ElementVolume;
			}
			maxElementEdgeLength = 2 * pow(3 * maxElementVolume, 0.33333);
		}
		MsgLogger() << "Max Element Volume:" << maxElementVolume << endl;
		MsgLogger() << "Max Edge Length:" << maxElementEdgeLength << endl;
		MsgLogger() << "cutoff:" << cutoffRadius << endl;
		
		for (size_t a1 = 0; a1 < numNodes; ++a1)
		{
			atom1 = calculator->mesh().nodes()[a1];
//			for(size_t a2 = 0; a2 < numNodes; ++a2)
			for (size_t a2 = 0; a2 <= a1; ++a2)
			{
				atom2 = calculator->mesh().nodes()[a2];

				Matrix3 analyticalStiffness = output0.analyticalStiffnessMatrix()[a2 * numNodes + a1];
				Matrix3 numericalStiffness = Matrix3(NULL_MATRIX);
				
				if (lengthCriterion)
					if (Length(atom2->deformedPos() - atom1->deformedPos()) > (cutoffRadius + 2 * maxElementEdgeLength + 1e-3)) continue;
				
				for (dof1 = 0; dof1 < NDOF; ++dof1)
				{
					for (dof2 = 0; dof2 < NDOF; ++dof2)
					{
						// Compute gradient numerically.
						double numerical, abserr;
						double x = atom2->deformedPos()[dof2];
						
						numerical = (_Function2old(x + h) + output0.forces()[atom1->index()][dof1]) / h;	// Forward differential
//						numerical = (_Function2old(x + h) - _Function2old(x - h)) / (2 * h);					// Central differential
//						gsl_deriv_central(&F, x, h, &numerical, &abserr);								// GSL central differential
						
						numericalStiffness(dof1, dof2) = numerical;

						double analytical = analyticalStiffness(dof1, dof2);
/*
						MsgLogger() << analytical << ", " << numerical << ", " << abserr << ", ";
						if(numerical)
							MsgLogger() << abs((analytical-numerical)/numerical)*100 << " %";
						else

							MsgLogger() << "-";

						MsgLogger() << endl;
*/
					}
				}
/*
				for (int i = 0; i < NDOF; ++i)
				{
					for (int j = 0; j < i; ++j)
					{
//						if (abs((numericalStiffness(i, j) - numericalStiffness(j, i)) / (numericalStiffness(i, j) + numericalStiffness(j, i))) > 1e-5)
						if (abs(numericalStiffness(i, j) - numericalStiffness(j, i)) > 1e-3)
							MsgLogger() << "Numerical local Stiffnessmatrix is not symmetrical." << a1 << a2 << endl;
					}
				}
*/
				output0.numericalStiffnessMatrix()[a2 * numNodes + a1] = numericalStiffness;
				output0.numericalStiffnessMatrix()[a1 * numNodes + a2] = numericalStiffness.transposed();
			}
			++progress;
		}
	
		// Apply displacement boundary conditions.
		for (size_t i = 0; i < numNodes; ++i)
		{
			Vector3 reforces = output0.reforces()[i];
			if (Length(reforces))
			{
				for (int j = 0; j < NDOF; ++j)
				{
					if (reforces[j])
					{
						for (size_t k = 0; k < numNodes; ++k)
						{
							// Set affected colums and rows to 0.
							output0.numericalStiffnessMatrix()[i * numNodes + k].setColumn(j,Vector3(NULL_VECTOR));
							output0.numericalStiffnessMatrix()[k * numNodes + i].setRow(j,Vector3(NULL_VECTOR));
						}
						output0.numericalStiffnessMatrix()[i * numNodes + i](j,j) = 1;
					}
				}
			}
		}
		MsgLogger() << "Done." << endl;
		
		//Check symmetry
		int symmetryFails = 0;
		for (size_t i = 0; i < numNodes; ++i)
		{
			for (size_t j = 0; j < i; ++j)
			{
				for (dof1 = 0; dof1 < NDOF; ++dof1)
				{
					for(dof2 = 0; dof2 < NDOF; ++dof2)
					{
						FloatType diff = output0.numericalStiffnessMatrix()[j * numNodes + i](dof1, dof2) - output0.numericalStiffnessMatrix()[i * numNodes + j](dof2, dof1);
						if(abs(diff) > 1e-4)
						{
							++symmetryFails;
							MsgLogger() << abs(diff) << i << j << dof1 << dof2 << output0.numericalStiffnessMatrix()[j * numNodes + i](dof1, dof2) << output0.numericalStiffnessMatrix()[i * numNodes + j](dof2, dof1) << endl;	
						}
					}
				}
			}
		}
		MsgLogger() << "SymmetryFails: " << symmetryFails << endl;
	}
#endif

private:
	
	/// The calculator for the energy and the forces.
	shared_ptr<Calculator> calculator;
	
	/// The calculation result.
	CalculationResult& output0, output1, output2;

	/// This is needed by the GSL numerical derivative routine.
	gsl_function F;
	
	/// The repatoms.
	RepAtom *atom1, *atom2;
	
	/// The local stiffness matrix.
	Matrix3 localStiffness;
	
	/// The current degrees of freedom.
	int dof1, dof2;
	
	/// The symmetry of the stiffness matrix.
	bool symmetry;
	
	/// This is called by the GSL numerical derivative routine to evaluate the function at 
	/// a point x.
	static double Function1(double x, void* param)
	{
		NumericalDerivativeCheck* dcheck = (NumericalDerivativeCheck*)param;
		CHECK_POINTER(dcheck);
		return dcheck->_Function1(x);
	}

	/// This is called by the GSL numerical derivative routine to evaluate the function at 
	/// a point x.
	double _Function1(double x)
	{
		// Disturb position.
		Point3 pos = atom1->deformedPos();
		Point3 oldPos = pos;
		pos[dof1] = x;
		atom1->setDeformedPos(pos);
		calculator->mesh().deformationUpdate();
		
		// Calculate Energy.
		CalculationResult output;
		calculator->calculateEnergy(output);
		
		// Restore old position.
		atom1->setDeformedPos(oldPos);
		calculator->mesh().deformationUpdate();
		return output.totalEnergy();
	}

	/// This is called by the GSL numerical derivative routine to evaluate the function at 
	/// a point x.
	static double Function2(double x, void* param)
	{
		NumericalDerivativeCheck* dcheck = (NumericalDerivativeCheck*)param;
		CHECK_POINTER(dcheck);
		return dcheck->_Function2old(x);
	}

	/// This is called by the GSL numerical derivative routine to evaluate the function at 
	/// a point x.
	double _Function2old(double x)
	{
		// Disturb position.
		Point3 pos = atom2->deformedPos();
		Point3 oldPos = pos;
		pos[dof2] = x;
		atom2->setDeformedPos(pos);
		calculator->mesh().deformationUpdate();

#if 1
		// Calculate Energy and Forces.
		CalculationResult output;
		calculator->calculateEnergyAndForces(output);
		FloatType value = -output.forces()[atom1->index()][dof1];
#else		
		gsl_function inner;
		inner.function = &Function1;
		inner.params = this;
		double value, abserr;
		gsl_deriv_central(&inner, atom1->deformedPos()[dof1], _h, &value, &abserr);
#endif
		
		// Restore old position.
		atom2->setDeformedPos(oldPos);
		calculator->mesh().deformationUpdate();
		return value;
	}
	
	/// This is called to evaluate the function at the new point. 
	void disturbAtomForward(double h)
	{
		// Disturb position forward.
		Point3 pos = atom2->deformedPos();
		Point3 oldPos = pos;
		pos[dof2] += h;
		atom2->setDeformedPos(pos);
		calculator->mesh().deformationUpdate();

		// Calculate Energy and Forces.
		calculator->calculateEnergyAndForces(output1);
		
		// Restore old position.
		atom2->setDeformedPos(oldPos);
		calculator->mesh().deformationUpdate();
	}
	
	/// This is called to evaluate the function at two new points. 
	void disturbAtomCentral(double h)
	{
		// Save position.
		Point3 pos = atom2->deformedPos();
		Point3 oldPos = pos;
		
		// Disturb position forward.
		pos[dof2] += h;
		atom2->setDeformedPos(pos);
		calculator->mesh().deformationUpdate();

		// Calculate Energy and Forces.
		calculator->calculateEnergyAndForces(output1);
		
		// Disturb position backward.
		pos[dof2] -= 2 * h;
		atom2->setDeformedPos(pos);
		calculator->mesh().deformationUpdate();
		
		// Calculate Energy and Forces.
		calculator->calculateEnergyAndForces(output2);
		
		// Restore old position.
		atom2->setDeformedPos(oldPos);
		calculator->mesh().deformationUpdate();
	}
};

}; // End of namespace MAFEM

#endif // __MAFEM_NUMERICAL_DERIVATIVE_CHECK_H
