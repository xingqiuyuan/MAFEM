///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "EnergyQRCalculator.h"
#include <atoms/SamplingAtom.h>
#include <material/EAMPotential.h>

namespace MAFEM {

/******************************************************************************
 * This performs the full calculation of the forces.
 *****************************************************************************/
void EnergyQRCalculator::calculateEnergyAndForcesImpl(CalculationResult& output)
{


	int numThread = simulation()->settings().openMPThread;
	if(numThread > omp_get_max_threads()) numThread =  omp_get_max_threads();
	int count = 0;
	// For quick access to the potential.
	const EAMPotential* potential = simulation()->potential().get();
	const FloatType potentialCutoffRadiusSquared = square(potential->cutoffRadius);

	// For fast looping over the boundary conditions.	
	const ForceBoundaryConditionList::const_iterator forceBCbegin = simulation()->forceBC().constBegin();
	const ForceBoundaryConditionList::const_iterator forceBCend = simulation()->forceBC().constEnd();

	FloatType totalEnergy = 0;
	Vector3* forceArray = output.forces().data();

	// This array is used to temporarly store the gradients of the pair energy functional and the charge density functional for all neighbors.
	struct PartialGradient {
		const SamplingAtom* neighbor;
		Vector3 phiGradient;
		Vector3 rhoGradient;
	};

	PartialGradient partialGradients[MAFEM_MAXIMUM_NEIGHBOR_COUNT];
	int num = 0;
	//Q_FOREACH(const SamplingAtom* atom1, mesh().samplingAtoms()) {
#pragma omp parallel for private(partialGradients) num_threads(numThread)
	for(int i = 0; i < mesh().samplingAtoms().size(); i++){

		SamplingAtom* atom1 =  mesh().samplingAtoms()[i];
			
		// Skip passive atoms.
		if(atom1->isPassive()) continue;
		
		
		// The electron density and its derivative at the location of atom 1.
		FloatType rho = 0;
		Vector3 rhoGrad = NULL_VECTOR;
		
		// The derivative of the F functional.
		FloatType derivativeF;
		
		// The force acting on atom 1 and its energy. 
		Vector3 localInternalForce = NULL_VECTOR;
		FloatType localInternalEnergy = 0;
		
		// Loop over all neighbors.
		PartialGradient* pg = partialGradients;
		pg->phiGradient = NULL_VECTOR;
		pg->rhoGradient = NULL_VECTOR;
		Vector3 store = NULL_VECTOR;
		pg->neighbor = NULL;

		for(SamplingAtom** neighborIter = atom1->neighborsBegin(); neighborIter != atom1->neighborsEnd(); ++neighborIter) {

			
			
			MAFEM_ASSERT(pg - partialGradients < MAFEM_MAXIMUM_NEIGHBOR_COUNT);
			const SamplingAtom* atom2 = *neighborIter;

			
						
			// Compute distance vector to neighbor.
			
			const Vector3 r = atom1->deformedPos() - atom2->deformedPos();	
			const FloatType distSquared = LengthSquared(r);

			

			if(distSquared- FLOATTYPE_EPSILON >= potentialCutoffRadiusSquared) continue;			
			MAFEM_ASSERT_MSG(distSquared > FLOATTYPE_EPSILON, "EnergyQCCalculator::calculateEnergyAndForces()", "Atoms too close together.");
			const FloatType dist = sqrt(distSquared);
	
						
			// Calculate pair energy term.
			FloatType derivative1;
			const FloatType Z = potential->U_of_r.eval(dist, &derivative1);
			const FloatType pairEnergy = Z / dist;
			localInternalEnergy += pairEnergy * 0.5;

			// Compute gradient of pair energy.
			derivative1 = (derivative1 - pairEnergy)/distSquared;
			pg->phiGradient = (0.5 * derivative1) * r;
			MAFEM_ASSERT_MSG(isfinite(derivative1), "EnergyQCCalculator::calculateEnergyAndForces()", QString("The pair functional derivative is Not-a-Number. Pair distance was %1").arg(dist).toLatin1().constData());
			localInternalForce -= pg->phiGradient;
			
			// Compute the contribution of atom 2 to the electron density at the location of atom 1.
			FloatType rhoDerivative;
			rho += potential->Rho_of_r.eval(dist, &rhoDerivative);
			MAFEM_ASSERT(isfinite(rhoDerivative));
			pg->rhoGradient = (rhoDerivative / dist) * r;
			store = pg->rhoGradient;
			rhoGrad += pg->rhoGradient;	
			pg->neighbor = atom2;
			++pg;			
			
		} 
		
		num = 0;
		// Compute embedding energy term.
		MAFEM_ASSERT_MSG(rho >= 0.0, "EnergyQCCalculator::calculateEnergyAndForces()", "Negative electron density detected.");	
		localInternalEnergy += potential->F_of_Rho.eval(rho, &derivativeF);

		MAFEM_ASSERT(isfinite(derivativeF) && isfinite(rhoGrad.X) && isfinite(rhoGrad.Y) && isfinite(rhoGrad.Z));
		localInternalForce -= derivativeF * rhoGrad;
#pragma omp critical
		{
		// Calculate forces imposed on this sampling atom by the boundary conditions.
		for(ForceBoundaryConditionList::const_iterator forceBC = forceBCbegin; forceBC != forceBCend; ++forceBC) {
			if(atom1->belongsToGroup((*forceBC)->group())){
				localInternalEnergy += (*forceBC)->calculateEnergyAndForces(atom1, localInternalForce);
			}
		}

			// Distribute energy/forces on the current sampling atom to the mesh nodes.
			if(atom1->samplingNode()) {
				// The sampling atom is a mesh node itself.

				const RepAtom* repatom = atom1->samplingNode()->realRepatom();
				
				MAFEM_ASSERT(repatom->index() >= 0 && repatom->index() < output.forces().size());
				MAFEM_ASSERT(isfinite(atom1->weight()));
				forceArray[repatom->index()] += localInternalForce * atom1->weight();

			}
			else {
				// The sampling atom is located between the nodes.
				MAFEM_ASSERT(atom1->element() != NULL);
				Vector4 sfv = atom1->barycentric();

				for(int j=0; j<NEN; j++) {
					const RepAtom* vertex = atom1->element()->vertex(j)->realRepatom();
					const FloatType weight = sfv[j] * atom1->weight();
					MAFEM_ASSERT(isfinite(weight));
					MAFEM_ASSERT(vertex->index() >= 0 && vertex->index() < output.forces().size());
					forceArray[vertex->index()] += localInternalForce * weight;
				}
				
			}

			// Distribute energy/forces the current atom is excerting on its neighbor atoms.
			const PartialGradient* pgend = pg;

			for(pg = partialGradients; pg != pgend; ++pg) {
				const SamplingAtom* other = pg->neighbor;
				if(other->samplingNode() && other->element() == NULL ) {
					const RepAtom* otherRepatom = other->samplingNode()->realRepatom();
					forceArray[otherRepatom->index()] += atom1->weight() * pg->phiGradient;
					forceArray[otherRepatom->index()] += (derivativeF * atom1->weight()) * pg->rhoGradient;


				}
				else {
					MAFEM_ASSERT(other->element() != NULL);
					
					Vector4 sfv = other->barycentric();

					for(int v=0; v<NEN; v++) {

						const RepAtom* vertex = other->element()->vertex(v)->realRepatom();

						const FloatType weight = sfv[v] * atom1->weight();

						MAFEM_ASSERT(vertex->index() >= 0 && vertex->index() < output.forces().size());
						forceArray[vertex->index()] += weight * pg->phiGradient;
						forceArray[vertex->index()] += (derivativeF * weight) * pg->rhoGradient;

					}
					
				}
			}
			count++;

			totalEnergy += localInternalEnergy * atom1->weight();

		}
	}
	
	output.setTotalEnergy(totalEnergy);
}

/******************************************************************************
 * This performs only the calculation of the energy.
 *****************************************************************************/
void EnergyQRCalculator::calculateEnergyImpl(CalculationResult& output)
{

	//MsgLogger() << "Energy calculation." << endl;
	// For quick access to the potential.
	const EAMPotential* potential = simulation()->potential().get();
	const FloatType potentialCutoffRadiusSquared = square(potential->cutoffRadius);

	// For fast looping over the boundary conditions.	
	const ForceBoundaryConditionList::const_iterator forceBCbegin = simulation()->forceBC().constBegin();
	const ForceBoundaryConditionList::const_iterator forceBCend = simulation()->forceBC().constEnd();

	FloatType totalEnergy = 0;
	
	Q_FOREACH(const SamplingAtom* atom1, mesh().samplingAtoms()) {
		
		// Skip passive atoms.
		if(atom1->isPassive()) continue;
		
		// The electron density at the location of atom 1.
		FloatType rho = 0;
		
		FloatType localInternalEnergy = 0;
		
		// Loop over all neighbors.
		for(SamplingAtom** neighborIter = atom1->neighborsBegin(); neighborIter != atom1->neighborsEnd(); ++neighborIter) {
			const SamplingAtom* atom2 = *neighborIter;
		
			// Compute distance vector to neighbor.
			const Vector3 r = atom1->deformedPos() - atom2->deformedPos();			
			const FloatType distSquared = LengthSquared(r);
			if(distSquared >= potentialCutoffRadiusSquared) continue;			
			MAFEM_ASSERT_MSG(distSquared-FLOATTYPE_EPSILON > FLOATTYPE_EPSILON, "EnergyQCCalculator::calculateEnergy()", "Atoms too close together.");
			const FloatType dist = sqrt(distSquared);
			
			// Calculate pair energy term.
			const FloatType pairEnergy = 0.5 * potential->U_of_r.eval(dist) / dist;
			localInternalEnergy += pairEnergy;
			
			// Compute the contribution of atom 2 to the electron density at the location of atom 1.
			rho += potential->Rho_of_r.eval(dist);
		} 

		// Compute embedding energy term.
		MAFEM_ASSERT_MSG(rho >= 0.0, "EnergyQCCalculator::calculateEnergy()", "Negative electron density detected.");	
		localInternalEnergy += potential->F_of_Rho.eval(rho);

		// Calculate energy contribution for this sampling atom from the boundary conditions.
		for(ForceBoundaryConditionList::const_iterator forceBC = forceBCbegin; forceBC != forceBCend; ++forceBC) {
			if(atom1->belongsToGroup((*forceBC)->group()))
				localInternalEnergy += (*forceBC)->calculateEnergy(atom1);
		}

		// Get the central cluster atom.
		totalEnergy += localInternalEnergy * atom1->weight();
	}
	
	output.setTotalEnergy(totalEnergy);
}


}; // End of namespace MAFEM
 
