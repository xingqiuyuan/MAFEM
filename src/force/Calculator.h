///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_CALCULATOR_H
#define __MAFEM_CALCULATOR_H

#include <MAFEM.h>
#include <simulation/SimulationResource.h>
#include <simulation/Simulation.h>
#include "CalculationResult.h"

namespace MAFEM {

/**
 * \brief Base class for classes that calculate the energy/forces acting on the repatoms. 
 * 
 * \author Alexander Stukowski
 */
class Calculator : public SimulationResource, boost::noncopyable
{
public:
	
	/// \brief Constructor.
	/// \params sim The global simulation object.  
	Calculator(Simulation* sim) : SimulationResource(sim), _mesh(sim->mesh()) {}

	/// \brief Returns the QC mesh.
	inline Mesh& mesh() { return _mesh; }
	
	/// \brief This performs the full calculation of the forces and the energies.
	/// \param output The method will store the calculated forces in this object.
	void calculateEnergyAndForces(CalculationResult& output);

	/// \brief This performs only the calculation of the total energy.
	/// \param output The method will store the calculated energy in this object.
	void calculateEnergy(CalculationResult& output);

	/// \brief This performs the calculation of the forces only.
	/// \param output The method will store the calculated forces in this object.
	void calculateForces(CalculationResult& output);

//	void calculateCentroSymmetry(const Box3& box, CalculationResult& output, FloatType cutoff,FloatType maxVol, FloatType tol);
	
	void calculateCentrosymmetry(CalculationResult& output);
	
	void calculateStresses(CalculationResult& output,FloatType maxVol, FloatType tol);

	void calculateSelectedAtomStresses(CalculationResult& output,FloatType maxVol, FloatType tol);
	
	void calculateStiffness(CalculationResult& output);
	
	int getAffectedNodes(const SamplingAtom* atom_k, const SamplingAtom* atom_l, int* affectedNodes, double* phi_k, double* phi_l);
	
	bool checkPositiveDefiniteForm(CalculationResult& output);
	
	int calculateEigenvalues(CalculationResult& output, FloatType min, FloatType max, int n, char uplo);
	
	int kroneckerDelta(int i, int j);

protected:

	/// \brief This performs the full calculation of the forces and the energies.
	/// \param output The method will store the calculated forces in this object.
	virtual void calculateEnergyAndForcesImpl(CalculationResult& output) = 0;

	/// \brief This performs only the calculation of the total energy.
	/// \param output The method will store the calculated energy in this object.
	virtual void calculateEnergyImpl(CalculationResult& output) = 0;

	/// \brief This performs the calculation of the forces only.
	/// \param output The method will store the calculated forces in this object.
	virtual void calculateForcesImpl(CalculationResult& output) = 0;

private:
	
	/// The QC mesh.
	Mesh& _mesh;
};

}; // End of namespace MAFEM

#endif // __MAFEM_CALCULATOR_H
