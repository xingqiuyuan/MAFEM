///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_ELEMENT_H
#define __MAFEM_ELEMENT_H

#include <simulation/SimulationResource.h>
#include <atoms/RepAtom.h>

namespace MAFEM {

class Mesh;			// defined in Mesh.h
class BoundaryFace;
/**
 * \brief A simple edge type. It contains two pointers to the vertices of the edge.
 */
typedef std::pair<RepAtom*, RepAtom*> ElementEdge;

/**
 * \brief A tetrahedron element.
 * 
 * The QC mesh is made of these elements.
 * 
 * \author Alexander Stukowski
 */
class Element : public ObjectWithFlags
{
public:

/**//// The bit flags for this class.
	enum ElementFlags {
		SELECTION_FLAG = 1,
		ADAPTION_FLAG = (SELECTION_FLAG<<1),
		DELETED_FLAG = (ADAPTION_FLAG<<1),
		VISITED_FLAG = (DELETED_FLAG<<1),
		SMALL_FLAG = (VISITED_FLAG<<1),
	};

public:

	/// \brief Default constructur that does not initialize the element.
	Element() : _isMarked(false),nextElementInCell(NULL),devStrainSecInvL2Norm(0) {}
	
    /// \brief Constructor that computes several properties for the elements.
	Element(const array<RepAtom*, NEN>& vertices);

	///////////////////////////////////// Vertices /////////////////////////////////////

	/// \brief Returns the i-th vertex incident on this element.
	inline RepAtom* vertex(int i) const { 
		MAFEM_ASSERT(0 <= i && i < NEN);
		CHECK_POINTER(_vertices[i]);
		return _vertices[i];
	}

/**//// sets the vertex of the element
	inline void setVertex(int i,RepAtom* rep){
	        CHECK_POINTER(rep);
	        _vertices[i] = rep;
	}
    
	/// \brief Returns the list of vertices of this element.
	inline const array<RepAtom*, NEN>& vertices() const { return _vertices; }

	/// \brief Returns whether some given vertex is a vertex of this element.
	inline bool hasVertex(const RepAtom* v) const { 
		CHECK_POINTER(v);
		return (_vertices[0] == v || _vertices[1] == v || _vertices[2] == v || _vertices[3] == v); 
	}

	/// \brief Returns the index of a vertex in this element.
	/// \note The given vertex must be a vertex of this element.
	inline int vertexIndex(const RepAtom* v) const { 
		MAFEM_ASSERT(hasVertex(v));
		if(_vertices[0] == v) return 0; 
		if(_vertices[1] == v) return 1;
		if(_vertices[2] == v) return 2;
		return 3;
	}

	/// Returns the lattice indices of the i-th vertex of this repatom in the 
	/// lattice of the element's grain.
	inline const Point3I& vertexLatticeIndices(int i) const {
		MAFEM_ASSERT(0 <= i && i < NEN);
		return _vertices[i]->site().indices();
	}
	
	/// \brief Returns the three vertices that make up a face of the tetrahedron element.
	inline void faceVertices(int face, RepAtom** v1, RepAtom** v2, RepAtom** v3) const {
		MAFEM_ASSERT(face >= 0 && face < 4);
		*v1 = _vertices[elementFaceMap[face][0]];
		*v2 = _vertices[elementFaceMap[face][1]];
		*v3 = _vertices[elementFaceMap[face][2]];
	}
	
	/// Returns the index of the tetrahedron face that is bound by the three given vertices.
	/// Returns -1 if there is no such face.
	inline int findFace(RepAtom* faceVertices[3]) const { 
		int f = 0;
		for(int i=0; i<3; i++) {
			if(_vertices[0] == faceVertices[i]) f |= 1; 
			else if(_vertices[1] == faceVertices[i]) f |= 2;
			else if(_vertices[2] == faceVertices[i]) f |= 4;
			else if(_vertices[3] == faceVertices[i]) f |= 8;
			else return -1;
		}
		if(f == (1|2|4)) return 3;
		if(f == (1|2|8)) return 2;
		if(f == (1|4|8)) return 1;
		MAFEM_ASSERT(f == (2|4|8));
		return 0;
	}

	/// Returns the planes for the four sides of the tetrahedron element. The planes
	/// are stored in integer format as they are in the coordinate system
	/// of the crystal bravais lattice space.
	inline const Plane_3<int>& faceBravaisPlane(int index) const {
		MAFEM_ASSERT(index >= 0 && index < 4);
		return _facePlanes[index];
	}
	
	/// Returns the two vertices that make up an edge of the tetrahedron element.
	inline ElementEdge edge(int edgeIndex) const {
		MAFEM_ASSERT_MSG(edgeIndex >= 0 && edgeIndex < 6, "Element::edge()", "Edge index out of range.");
		return ElementEdge(_vertices[elementEdgeMap[edgeIndex][0]], _vertices[elementEdgeMap[edgeIndex][1]]);
	}
	
	///////////////////////////////////// Neighbors /////////////////////////////////////

	/// Returns the i-th neighbor element of this element or NULL if there is no neighbor.
	/// The i-th neighbor is opposite to the i-th vertex of this element.
	/// Precondition: 0 <= i <= 3
	inline Element* neighbor(int i) const { 
		MAFEM_ASSERT(0 <= i && i < NEN); 
		return _neighbors[i];
	}

	/// Returns true if n is a neighbor of this element.
	inline bool hasNeighbor(const Element* n) const {
		CHECK_POINTER(n);
		return (_neighbors[0] == n || _neighbors[1] == n || _neighbors[2] == n || _neighbors[3] == n); 
	}

	/// Returns the index of the given neighbor element in this element's list of neighbors.
	inline int neighborIndex(const Element* n) const { 
		MAFEM_ASSERT(hasNeighbor(n));
		if(_neighbors[0] == n) return 0;
		if(_neighbors[1] == n) return 1;
		if(_neighbors[2] == n) return 2;
		return 3;
	}

	/// Sets the i-th neighbor element of this element.
	/// The i-th neighbor is opposite to the i-th vertex of this element.
	/// Precondition: 0 <= i <= 3
	inline void setNeighbor(int i, Element* n) {
		MAFEM_ASSERT(0 <= i && i < NEN); 
        _neighbors[i] = n;
	}
	
/**//// Sets the values for the face direction fields.
	/// See 'faceDirections' field for more details.
	void SetFaceDirections(char face1, char face2, char face3, char face4) {
		faceDirections[0] = face1; faceDirections[1] = face2;
		faceDirections[2] = face3; faceDirections[3] = face4;
	}

/**//// Sets the values for the face direction fields.
	/// See 'faceDirections' field for more details.
	void SetFaceDirections(const array<char,4>& dir) { faceDirections = dir; }

/**//// Gets the values of the face direction fields.
	/// See 'faceDirections' field for more details.
	const array<char,4>& GetFaceDirections() const { return faceDirections; }

/**//// Returns the i-th boundary face of this element or NULL if the i-th side is an interior side.
	BoundaryFace* GetBoundaryFace(size_t i) const {
		MAFEM_ASSERT(0 <= i && i < 4);
		return boundaryFaces[i];
	}

/**//// Sets the i-th boundary face of this element.
	void SetBoundaryFace(size_t i, BoundaryFace* face) {
		MAFEM_ASSERT(0 <= i && i < 4);
		boundaryFaces[i] = face;
	}


	///////////////////////////////////// Attributes /////////////////////////////////////

	/// \brief Returns the lattice this element belongs to.
	inline CrystalLattice* lattice() const { return _lattice; }

	/// Returns the determinant of the element's Jacobian matrix.
	inline FloatType det() const { return _det; }

	/// Returns the volume of the tetrahedron element.
	inline FloatType volume() const { return _volume; }
	
	/// Indicates that the element has been marked.
	inline bool isMarked() const { return _isMarked; }

	/// Sets the value of the marking flag for this element.
	inline void setMarked(bool on) { _isMarked = on; }
	
	/// Determines whether this is an unnecessary element in a fully atomistic region.
	bool isUnnecessaryElement() const;
	
	/// \brief Tests whether a lattice site is inside the element.
	/// \param siteIndices The lattice indices that specify the site on the lattice of the element's grain.
	/// \return True if the specified indices are inside the element or on the element boundary.
	inline bool containsSite(const Point3I& siteIndices) const {
		for(int i=0; i<NEN; i++)
			if(_facePlanes[i].pointDistance(siteIndices) < 0) return false;
		return true;
	}

	inline int onFace(const Point3I& siteIndices) const {
		for(int i=0; i<NEN; i++)
			if(_facePlanes[i].pointDistance(siteIndices) == 0) return i;
		return -1;
	}

	/// \brief Computes the barycentric coordinates of the given point.
	/// \param p The world position of the point for which the barycentric coordinates are computes.
	/// \return The barycentric coordinates stored in a Vector4.
	///
	/// This method uses the undeformed reference configuration of the element.
	inline Vector4 computeBarycentric(const Point3& p) const {
#ifdef _DEBUG			
		Vector4 c;
		for(int i=0; i<NEN; i++)
			c[i] = _dsh[i][0]*p.X + _dsh[i][1]*p.Y + _dsh[i][2]*p.Z + _dsh[i][3];
		Point3 p_out = ORIGIN + (c[0]*(vertex(0)->pos()-ORIGIN) + c[1]*(vertex(1)->pos()-ORIGIN) + c[2]*(vertex(2)->pos()-ORIGIN) + c[3]*(vertex(3)->pos()-ORIGIN)); 
		if(!p.equals(p_out, FLOATTYPE_EPSILON)) {
			MsgLogger() << "ERROR in function Element::computeBarycentric():" << endl;
			MsgLogger() << "Input point p:" << p << endl;
			MsgLogger() << "Vertex1:" << vertex(0)->pos() << endl;
			MsgLogger() << "Vertex2:" << vertex(1)->pos() << endl;
			MsgLogger() << "Vertex3:" << vertex(2)->pos() << endl;
			MsgLogger() << "Vertex4:" << vertex(3)->pos() << endl;
			MsgLogger() << "Barycentric coordinates:" << c[0] << " " << c[1] << " " << c[2] << " " << c[3] << endl;
			MsgLogger() << "Derived point:" << p_out << endl;
			MAFEM_ASSERT_MSG(false, "Element::computeBarycentric", "Cannot compute barycentric coordinates of a point that is outside of the element.");
		}
		return c;
#else
		return Vector4(
			_dsh[0][0]*p.X + _dsh[0][1]*p.Y + _dsh[0][2]*p.Z + _dsh[0][3],
			_dsh[1][0]*p.X + _dsh[1][1]*p.Y + _dsh[1][2]*p.Z + _dsh[1][3],
			_dsh[2][0]*p.X + _dsh[2][1]*p.Y + _dsh[2][2]*p.Z + _dsh[2][3],
			_dsh[3][0]*p.X + _dsh[3][1]*p.Y + _dsh[3][2]*p.Z + _dsh[3][3]
		);
#endif
	}
	
	 /**//// Returns the next element in the linked list of elements that
		/// are in the same leaf cell of the element octree.
		Element* GetNextElementInCell() const { return nextElementInCell; }

	/**//// Sets the next element in the linked list of elements that
		/// are in the same leaf cell of the element octree.
		/// This method is called by the octree class to build the linked list.
		void SetNextElementInCell(Element* next) { nextElementInCell = next; }


	/**//// Bisects an edge of this element with the given point.
	/// The newly created second element is returned.
	Element* BisectElementEdge(int v1, int v2, RepAtom* splitPoint,Mesh* mesh);

	/**//// Indicates that the element has to be updated.
		bool IsDirty() const { return isDirty; }

	/**//// Sets the value of the dirty flag for this element.
	/// If the element is marked as dirty then all its nodes
	/// will be marked as dirty too.
	void SetDirty(bool dirty) {
		isDirty = dirty;
		if(isDirty) {
			for(int i=0; i<NEN; i++)
				vertex(i)->SetDirty(true);
		}
	}

	/**//// Returns the index of this element in the collection.
	int Index() const { return index; }

	/**//// Sets the index of this element in the collection.
	/// \note This is for internal use only!
	void SetIndex(int newIndex) { index = newIndex; }

	/**//// Return x, y and z partial derivatives of shape function associated to node i.
		/// Note: This may be called only after ComputeShapeFunction() has been invoked.
		const Vector3& GetDSh(int j) const {
				MAFEM_ASSERT(0 <= j && j < NEN);
				return *(const Vector3*) &_dsh[j];
		}

	/**//// sets the number of site belongs to the element
		void setNumSiteContains(int n) { numSiteContains = n;}

	/**//// returns the number of site belongs to the element
		int getNumSiteContains()  {return numSiteContains;}

	
	/**//// sets the number of site belongs to the element
		void setNumFaceAtoms(FloatType n) { numFaceAtoms = n;}

	/**//// returns the number of site belongs to the element
		FloatType getNumFaceAtoms()  {return numFaceAtoms;}
	

	/**//// sets the number of quadrature point belongs to the element
		void setNumQuadPoints(int n){ numQuadPoints = n;}

	/**//// returns the number of quadrature point belongs to the element
		int getNumQuadPoints() { return numQuadPoints;}

	/**//// stores the quadrature point indices
		void setQuadPoints(Point3I quadpoint){
			quadPoints.push_back(quadpoint);
		}
	/**//// returns the quadrature point indices
		Point3I getQuadPoint(int index){
			return quadPoints.at(index);
		}

	/**//// returns the quadrature atoms indices storage pointer
		vector<Point3I>* getQuadPointVector(){
			return &quadPoints;
		}

	/**//// stores the indices of all the atoms inside the element
		void setEleAtomSites(Point3I site){
			eleAtomsites.push_back(site);
		}
	/**//// returns the indices of the atom inside the element
		Point3I getEleAtomSites(int index){
			return eleAtomsites.at(index);
		}

	/**//// returns the element atoms indices storage pointer
		vector<Point3I>* getEleAtomSitesVector(){
			return &eleAtomsites;
		}

	/**//// returns the total number of quadrature points inside the element
		int numOfQuadPoints(){ return quadPoints.size();}

	/**//// sets the value of dev strain second invariant L2 norm
		void setDevStrainSecInvL2Norm(FloatType value){ devStrainSecInvL2Norm = value;}

	/**//// returns the value of dev strain second invariant L2 norm
		FloatType getDevStrainSecInvL2Norm(){ return devStrainSecInvL2Norm;}

private:

	/// Initialize the data fields of the class.
	void initialize();

	/// The four corners of the tetrahedron element.
	array<RepAtom*, NEN> _vertices;

	/// Contains the neighbors of the element.
	/// Neighbor i is on the side opposite to vertex i (0 <= i <= 3).
	array<Element*, NEN> _neighbors;

	/// The lattice this element belongs to.
	CrystalLattice* _lattice;

	/// Partial derivatives of shape function (in undeformed configuration).
	Vector4 _dsh[NEN];
	
	/// The determinant of this element's Jacobian matrix in the undeformed configuration.
	FloatType _det;

	/// The volume of this element in the undeformed reference configuration.
	/// Note: This is just 1/6 of det.
	FloatType _volume;

	/// The planes for the four sides of the tetrahedron element. They are needed
	/// to determine if a lattice site is inside the element. The planes
	/// are stored in integer format as they are in the coordinate system
	/// of the crystal bravais lattice space.
	Plane_3<int> _facePlanes[4];

	/// Indicates that the element has been marked.
	bool _isMarked;
	
	/// Lookup table for tetrahedron face vertices.
	static const int elementFaceMap[4][3];	

	/// Lookup table for tetrahedron edge vertices.
	static const int elementEdgeMap[6][2];

	/**//// The linked list of elements that are in the same leaf cell of the element octree.
		Element* nextElementInCell;

	/**//// Stores for each face of this tetrahedron the direction it is pointing to.
		/// This field is used only to speed up the neighbor finding for the elements.
		/// Possible values:
		///    0 - It's an interior face. This means that the adjacent element is in the same octree cell.
		///    1 - It's a transition face to the neighbor cell in +X direction.
		///    2 - It's a transition face to the neighbor cell in -X direction.
		///    3 - It's a transition face to the neighbor cell in +Y direction.
		///    4 - It's a transition face to the neighbor cell in -Y direction.
		///    5 - It's a transition face to the neighbor cell in +Z direction.
		///    6 - It's a transition face to the neighbor cell in -Z direction.
		array<char,4> faceDirections;

	/**//// Pointers to the original boundary faces if this element is at the grain boundary.
	BoundaryFace* boundaryFaces[4];

	/**//// number of lattice sites belongs to the element
	int numSiteContains;

	/**//// number of quadrature points belongs to element
		int numQuadPoints;

	/**//// stores the quadrature point indices
		vector<Point3I> quadPoints;

	/**//// stores the indices of all the atom inside the element
		vector<Point3I> eleAtomsites;

	/**//// Indicates that the element has to be updated.
		bool isDirty;

	/**//// The index of the element in the ElementCollection.
		int index;

	/**//// stores the value of dev strain second invariant L2 norm
		FloatType devStrainSecInvL2Norm;

		FloatType numFaceAtoms;

		
private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) {
		ar & boost::serialization::base_object<ObjectWithFlags>(*this);
    	ar & _vertices;
		//ar & _neighbors;
		ar & _lattice;
		ar & faceDirections;
		ar & boundaryFaces;
    	if(typename Archive::is_loading() == true) {
    		initialize();
    	}
    }		

	friend class boost::serialization::access;

public:

	template<class Archive>
    void serializeNeighbors(Archive &ar, const unsigned int version) {
		ar & _neighbors;
	}

public:

	template<class Archive>
    void serializeNextElementInCell(Archive &ar, const unsigned int version) {
		ar & nextElementInCell;
	}
};

}; // End of namespace MAFEM

// This allocator class ensures that all Element instances created by the Boost serialization
// code are allocated from the memory pool of the Mesh class. 
namespace boost {
namespace archive {
namespace detail {
#if buildLocationCluster
	template<>
	struct heap_allocation<MAFEM::Element>
	{
		static MAFEM::Element* invoke_new();
		static void invoke_delete(MAFEM::Element* p);
	    explicit heap_allocation(){
	        m_p = invoke_new();
	    }
	    ~heap_allocation(){
	        if(m_p) invoke_delete(m_p);
	    }
	    MAFEM::Element* get() const {
	        return m_p;
	    }
	    MAFEM::Element* release() {
	    	MAFEM::Element* p = m_p;
	        m_p = 0;
	        return p;
	    }
	private:
	    MAFEM::Element* m_p;
	};
#else
	template<>
	struct heap_allocator<MAFEM::Element>	
	{
		// Allocates a new Element instance from the memory pool in the Mesh class.
		static MAFEM::Element* invoke();	// Implementation is in MeshTriangulation.cpp
	};
#endif
};
};
};

#endif // __MAFEM_ELEMENT_H
