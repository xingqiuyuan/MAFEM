///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "Mesh.h"
#include <simulation/Simulation.h>
#include <linalg/BiconjugateGradient.h>

#include <CGAL/Simple_cartesian.h>
#include <CGAL/Orthogonal_k_neighbor_search.h>
#include <CGAL/Search_traits.h>
#include <CGAL/basic.h>
#include "voro/voro++.hh"

using namespace voro;

namespace CGAL {
  template <>
  struct Kernel_traits<MAFEM::RepAtom*> {
    struct Kernel {
      typedef FloatType FT;
      typedef FloatType RT;
    };
  };
}

namespace MAFEM {

struct Construct_coord_iterator {
  const FloatType* operator()(const RepAtom* p) const
  { return &p->pos().X; }

  const FloatType* operator()(const RepAtom* p, int) const
  { return &p->pos().X + NDOF; }

  const FloatType* operator()(const Point3& p) const
  { return &p.X; }

  const FloatType* operator()(const Point3& p, int) const
  { return &p.X + NDOF; }

  typedef const FloatType* result_type;
};

struct Construct_coord_iterator1 {
  /*const FloatType operator()(const Point3 p) const
  { return p.X; }

  const FloatType operator()(const Point3 p, int) const
  { return p.X + NDOF; }*/

  const FloatType* operator()(const Point3& p) const
  { return &p.X; }

  const FloatType* operator()(const Point3& p, int) const
  { return &p.X + NDOF; }

  typedef const FloatType* result_type;
};

struct Distance {
	typedef Point3 Query_item;

	FloatType transformed_distance(const Point3& p1, const RepAtom* p2) const {
		return DistanceSquared(p1, p2->pos());
	}

	template <class TreeTraits>
	FloatType min_distance_to_rectangle(const Point3& p, const CGAL::Kd_tree_rectangle<TreeTraits>& b) const {
		FloatType distance(0.0), h = p.X;
		if (h < b.min_coord(0)) distance += (b.min_coord(0)-h)*(b.min_coord(0)-h);
		if (h > b.max_coord(0)) distance += (h-b.max_coord(0))*(h-b.max_coord(0));
		h=p.Y;
		if (h < b.min_coord(1)) distance += (b.min_coord(1)-h)*(b.min_coord(1)-h);
		if (h > b.max_coord(1)) distance += (h-b.max_coord(1))*(h-b.max_coord(1)); //changed it was(h-b.min_coord(1) before in last bracket
		h=p.Z;
		if (h < b.min_coord(2)) distance += (b.min_coord(2)-h)*(b.min_coord(2)-h);
		if (h > b.max_coord(2)) distance += (h-b.max_coord(2))*(h-b.max_coord(2));
		return distance;
	}

	template <class TreeTraits>
	FloatType max_distance_to_rectangle(const Point3& p, const CGAL::Kd_tree_rectangle<TreeTraits>& b) const {
		FloatType h = p.X;

		FloatType d0 = (h >= (b.min_coord(0)+b.max_coord(0))/2.0) ?
                (h-b.min_coord(0))*(h-b.min_coord(0)) : (b.max_coord(0)-h)*(b.max_coord(0)-h);

		h=p.Y;
		FloatType d1 = (h >= (b.min_coord(1)+b.max_coord(1))/2.0) ?
		            (h-b.min_coord(1))*(h-b.min_coord(1)) : (b.max_coord(1)-h)*(b.max_coord(1)-h);
		h=p.Z;
		FloatType d2 = (h >= (b.min_coord(2)+b.max_coord(2))/2.0) ?
		            (h-b.min_coord(2))*(h-b.min_coord(2)) : (b.max_coord(2)-h)*(b.max_coord(2)-h);
		return d0 + d1 + d2;
	}

	FloatType new_distance(FloatType& dist, FloatType old_off, FloatType new_off, int /* cutting_dimension */) const {
		return dist + new_off*new_off - old_off*old_off;
	}

	FloatType transformed_distance(FloatType d) const { return d*d; }

	FloatType inverse_of_transformed_distance(FloatType d) { return std::sqrt(d); }

}; // end of struct Distance



struct Distance1 {
	typedef Point3 Query_item;

	FloatType transformed_distance(const Point3& p1, const Point3& p2) const {
		return DistanceSquared(p1, p2);
	}

	template <class TreeTraits>
	FloatType min_distance_to_rectangle(const Point3& p, const CGAL::Kd_tree_rectangle<TreeTraits>& b) const {
		FloatType distance(0.0), h = p.X;
		if (h < b.min_coord(0)) distance += (b.min_coord(0)-h)*(b.min_coord(0)-h);
		if (h > b.max_coord(0)) distance += (h-b.max_coord(0))*(h-b.max_coord(0));
		h=p.Y;
		if (h < b.min_coord(1)) distance += (b.min_coord(1)-h)*(b.min_coord(1)-h);
		if (h > b.max_coord(1)) distance += (h-b.max_coord(1))*(h-b.max_coord(1)); //changed it was(h-b.min_coord(1) before in last bracket
		h=p.Z;
		if (h < b.min_coord(2)) distance += (b.min_coord(2)-h)*(b.min_coord(2)-h);
		if (h > b.max_coord(2)) distance += (h-b.max_coord(2))*(h-b.max_coord(2));
		return distance;
	}

	template <class TreeTraits>
	FloatType max_distance_to_rectangle(const Point3& p, const CGAL::Kd_tree_rectangle<TreeTraits>& b) const {
		FloatType h = p.X;

		FloatType d0 = (h >= (b.min_coord(0)+b.max_coord(0))/2.0) ?
                (h-b.min_coord(0))*(h-b.min_coord(0)) : (b.max_coord(0)-h)*(b.max_coord(0)-h);

		h=p.Y;
		FloatType d1 = (h >= (b.min_coord(1)+b.max_coord(1))/2.0) ?
		            (h-b.min_coord(1))*(h-b.min_coord(1)) : (b.max_coord(1)-h)*(b.max_coord(1)-h);
		h=p.Z;
		FloatType d2 = (h >= (b.min_coord(2)+b.max_coord(2))/2.0) ?
		            (h-b.min_coord(2))*(h-b.min_coord(2)) : (b.max_coord(2)-h)*(b.max_coord(2)-h);
		return d0 + d1 + d2;
	}

	FloatType new_distance(FloatType& dist, FloatType old_off, FloatType new_off, int /* cutting_dimension */) const {
		return dist + new_off*new_off - old_off*old_off;
	}

	FloatType transformed_distance(FloatType d) const { return d*d; }

	FloatType inverse_of_transformed_distance(FloatType d) { return std::sqrt(d); }

}; // end of struct Distance

/******************************************************************************
* This generates the sampling clusters around the repatoms.
******************************************************************************/
void Mesh::buildSamplingClusters()
{
	if(simulation()->potential() == NULL)
		throw Exception("The interatomic potential has not been initialized.");
	
	// Measure the time it taskes to generate the sampling atoms.
	timer clusterTimer;	
	
	const SimulationSettings& settings = simulation()->settings();
	const AffineTransformation simCell = simulation()->simulationCell();
	const AffineTransformation inverseSimCell = simulation()->inverseSimulationCell();
	const array<bool,NDOF> pbc = settings.periodicBC;
	FloatType potentialCutoff = simulation()->potential()->cutoffRadius;
	bool rcheck = false;
	
	int index = 0;
	
	int numThread = simulation()->settings().openMPThread;
	if(numThread > omp_get_max_threads()) numThread = omp_get_max_threads();
	numThread = 1;

	// If the force-based QC scheme is enabled then we need more passive sampling atoms
	// because the forces on the active sampling atoms depends on the electron density at
	// their neighbor sites which in turn depends on the positions of the neighbors' neighbors.
	if(settings.QCForceMode) potentialCutoff *= 2.0;
	
	// The radius up to which sampling atoms are generated around repatoms.
	const FloatType largeRadius = settings.clusterRadius + settings.clusterSkinThickness + potentialCutoff;
	const FloatType largeRadiusSquared = square(largeRadius);

	MsgLogger() << logdate << "Building sampling clusters (inner_radius=" << settings.clusterRadius << "; outer_radius=" << largeRadius << ")." << endl;
	
	// Clear previous sampling clusters.
	if(!samplingAtoms().empty()) {
		MsgLogger() << "  Clearing previous sampling clusters." << endl;
		_samplingAtoms.clear();
		_samplingAtomPool.purge_memory();
		
		// Reset sampling atom counters.
		Q_FOREACH(RepAtom* repatom, repatoms()) {
			repatom->setNumSamplingAtoms(0);

		}
	}
	
	// Create a search tree for nearest node searching.
	typedef CGAL::Search_traits<FloatType, RepAtom*, const FloatType*, Construct_coord_iterator> Traits;
	typedef CGAL::Orthogonal_k_neighbor_search<Traits, Distance> K_neighbor_search;
	typedef K_neighbor_search::Tree Tree;

	// Insert all repatoms into the search tree.
	Tree tree(repatoms().begin(), repatoms().end());	
	
	// The first loop is over all grains and the second one over the lattice sites of each grain.
	Q_FOREACH(const shared_ptr<Grain>& grain, simulation()->grains()) {
		// Computer the bounding box of the simulation cell in lattice space.		
		Box3 simBox = simulation()->simulationCell() * Box3(Point3(0,0,0), Point3(1,1,1));
		Box3 latticeBoundingBoxFrac = grain->lattice().inverseLatticeVectors() * simBox.padBox(largeRadius);
		Box3I bbox;
		for(int k=0; k<NDOF; k++) {
			bbox.minc[k] = (int)floor(latticeBoundingBoxFrac.minc[k]);
			bbox.maxc[k] = (int)ceil(latticeBoundingBoxFrac.maxc[k]);
		}

		
		progress_display progress((bbox.sizeX()+1) * (bbox.sizeY()+1));
		
		// Iterate over all lattice sites. 
		//Point3I indices;
	#pragma omp parallel for  num_threads(numThread)		
		for(int x = bbox.minc.X; x <= bbox.maxc.X; x++) {
		#pragma omp parallel for  num_threads(numThread)		  
			for(int y = bbox.minc.Y; y <= bbox.maxc.Y; y++) {
			#pragma omp parallel for  num_threads(numThread)
				for(int z = bbox.minc.Z; z <= bbox.maxc.Z; z++) {
				  
				  
					
					Point3I indices(x,y,z);

					// Is there a node at this lattice site?
					LatticeSite site(&grain->lattice(), indices);

					rcheck = false;
					for(int i=0; i<_removeSite.size(); i++){
						if(_removeSite[i]->equals(site.pos(),FLOATTYPE_EPSILON)) { rcheck=true; break; }
					}

					if(rcheck) { continue;}
				    
					RepAtom* siteNode = nodes().findAt(site); 
					if(siteNode) {
						
						// If it is a repatom then create an active sampling atom at this site.
						if (siteNode->realRepatom() == siteNode) {
#pragma omp critical
	{							
							MAFEM_ASSERT(samplingAtoms().findAt(site) == NULL);							
							SamplingAtom* samplingAtom = static_cast<SamplingAtom*>(_samplingAtomPool.malloc());
							new (samplingAtom) SamplingAtom(site);
							samplingAtom->initialize(NULL, site.pos(), NULL_VECTOR, siteNode, siteNode, 0);
							samplingAtom->setAtomicIndex(index++);
							
							
							_samplingAtoms.push_back(samplingAtom);
							siteNode->setNumSamplingAtoms(siteNode->numSamplingAtoms() + 1);
							
	}							
							continue;
						}
					}
					
					// Wrap around position when periodic boundary conditions are applied.
					Point3 reducedPoint = simulation()->inverseSimulationCell() * site.pos();
					Point3 imagePos = site.pos();
					Vector3I imageShift;
					
					for(int k=0; k<NDOF; k++) {
						if(settings.periodicBC[k]) {
							imageShift[k] = (int) floor(reducedPoint[k] + FLOATTYPE_EPSILON);
							imagePos.X -= simCell.column(k).X * (imageShift[k]);
							imagePos.Y -= simCell.column(k).Y * (imageShift[k]);
							imagePos.Z -= simCell.column(k).Z * (imageShift[k]);
						}
						else imageShift[k] = 0;
					}
					
					
					// This is the wrapped around lattice site:
					LatticeSite imageSite(&grain->lattice(), imagePos);
					MAFEM_ASSERT_MSG(imageSite.pos().equals(imagePos, FLOATTYPE_EPSILON), "Mesh::buildSamplingClusters()", "Non-periodic lattice detected. Lattice is not compatible with periodic boundary conditions and simulation cell size.");
					MAFEM_ASSERT(imageShift != NULL_VECTOR || imageSite.pos() == site.pos());

					// Check if there is a node at the image position.
					RepAtom* imageNode = nodes().findAt(imageSite);
					
					// Find the element the current site is located in.
					Element* element;
#pragma omp critical
{					
					if(simulation()->settings().segmentTreeElementSearch) element = elements().findAt(imageSite);
					if(simulation()->settings().octreeElementSearch) element = FindElement(imageSite,grain);
}					
					
					// If the lattice site is not inside an element and not on a node than it is outside the crystal.
					if(!element && !imageNode) continue;
					if(imageNode) element = NULL;
					
					FloatType closestDistanceSquared;
					RepAtom* closestRepatom;					

#pragma omp critical
{					
					// Find the closest repatom.
					K_neighbor_search search(tree, site.pos(), 1);
					MAFEM_ASSERT(search.begin() != search.end());										
					closestDistanceSquared = search.begin()->second;
					closestRepatom = search.begin()->first;					
}					
					if(closestDistanceSquared > largeRadiusSquared) continue;
					MAFEM_ASSERT(abs(closestDistanceSquared - DistanceSquared(site.pos(), closestRepatom->pos())) <= FLOATTYPE_EPSILON); 
					
					RepAtom* samplingClusterRepatom = NULL;
					if(closestDistanceSquared <= square(settings.clusterRadius)){
						//MsgLogger() << "imageshift" << imageShift << site.pos() << endl;
						samplingClusterRepatom = closestRepatom;
						samplingClusterRepatom->setNumSamplingAtoms(samplingClusterRepatom->numSamplingAtoms() + 1);
					}
					
#pragma omp critical
{					
					//MsgLogger() << "passive******************" << closestDistanceSquared << square(settings.clusterRadius) << endl;
					MAFEM_ASSERT(samplingAtoms().findAt(site) == NULL);							
					SamplingAtom* samplingAtom = static_cast<SamplingAtom*>(_samplingAtomPool.malloc());
					new (samplingAtom) SamplingAtom(site);
					samplingAtom->initialize(element, imageSite.pos(), imageShift, samplingClusterRepatom, imageNode, closestDistanceSquared);
					samplingAtom->setAtomicIndex(index++);
					_samplingAtoms.push_back(samplingAtom);
					//if(samplingAtom->isPassive()) MsgLogger() << "passive******************" << closestRepatom->pos() << site.pos() << closestDistanceSquared << imageShift <<  square(settings.clusterRadius) << endl;
}			
				}

				++progress;
			}
		}
	}

	int count = 0;
	Q_FOREACH(const SamplingAtom* atom, samplingAtoms()) {
		if(atom->isActive()) count++;
	}
	MsgLogger() << "  count ******************************* " << count << endl;
	
	MsgLogger() << "  Removing duplicate sampling atoms." << endl;
	
	int max_x = (pbc[0]) ? 1 : 0;
	int max_y = (pbc[1]) ? 1 : 0;
	int max_z = (pbc[2]) ? 1 : 0;
	
	// Here we make sure that periodic images of active sampling atoms are not counted twice.
	// That is we find all groups of periodic images that belong together and then decide which one
	// of the sampling atoms is taken as an active atom. The decision is based on the  
	// distances of the sampling atoms to their respective central repatom.
	Q_FOREACH(SamplingAtom* samplingAtom, samplingAtoms()) {
		
		// Skip passive sampling atoms. We are only interested in active ones.
		if(samplingAtom->clusterRepatom() == NULL)
			continue;
		// Skip atoms inside the simulation cell here. They will be visited later
		// if they have a periodic image outside of the simulation cell.
		if(samplingAtom->imageShift() == NULL_VECTOR)
			continue;
		
		SamplingAtom* closestSamplingAtom = samplingAtom;
		RepAtom* closestRepatom = samplingAtom->clusterRepatom();
		FloatType closestDistance = samplingAtom->squaredDistanceToCenter();
		closestRepatom->setNumSamplingAtoms(closestRepatom->numSamplingAtoms() - 1);
		samplingAtom->setClusterRepatom(NULL, 0);
		
		Vector3I indices;
		for(indices.X = -max_x; indices.X <= max_x; indices.X++) {
			for(indices.Y = -max_y; indices.Y <= max_y; indices.Y++) {
				for(indices.Z = -max_z; indices.Z <= max_z; indices.Z++) {
		
					// Look for another sampling atom at the other side of the box.
					Point3 imagePos = samplingAtom->pos() - simulation()->simulationCell() * indices;
					LatticeSite imageSite(samplingAtom->site().lattice(), imagePos);
					MAFEM_ASSERT_MSG(imageSite.pos().equals(imagePos, FLOATTYPE_EPSILON), "Mesh::buildSamplingClusters()", "Non-periodic lattice detected. Lattice is not compatible with periodic boundary conditions and simulation cell size.");
			
					SamplingAtom* imageAtom = samplingAtoms().findAt(imageSite);
					if(imageAtom == NULL) continue;
					MAFEM_ASSERT(imageAtom->imageShift() == samplingAtom->imageShift() - indices);
					if(imageAtom->clusterRepatom() == NULL) continue;
					
					// If periodic boundary conditions are enabled then multiple sampling atoms could have been created at the same
					// lattice site but assigned to different repatoms. This has to be resolved by comparing the respective
					// distances to the repatoms. 

					MAFEM_ASSERT(imageAtom->clusterRepatom() != samplingAtom->clusterRepatom());
					if(closestDistance+FLOATTYPE_EPSILON > imageAtom->squaredDistanceToCenter()) {
						// The sampling atom is equally distant to two or more nodes.
						// This ambiguity has to be resolved using an additional criterium.
						// We use the number of sampling atoms per repatom to balance the weights.
						if(closestDistance-FLOATTYPE_EPSILON > imageAtom->squaredDistanceToCenter() || imageAtom->clusterRepatom()->numSamplingAtoms() <= closestRepatom->numSamplingAtoms()) {
							closestRepatom = imageAtom->clusterRepatom();
							closestDistance = imageAtom->squaredDistanceToCenter();
							closestSamplingAtom = imageAtom;
						}
					}
					CHECK_POINTER(imageAtom->clusterRepatom());
					imageAtom->clusterRepatom()->setNumSamplingAtoms(imageAtom->clusterRepatom()->numSamplingAtoms() - 1);
					imageAtom->setClusterRepatom(NULL, 0);
				}
			}
		}		
		samplingAtom->setClusterRepatom(closestRepatom, closestDistance);
		closestRepatom->setNumSamplingAtoms(closestRepatom->numSamplingAtoms() + 1);
	}

	// Count active/passive sampling atoms.
	int nactive = 0;
	Q_FOREACH(const SamplingAtom* atom, samplingAtoms()) {
		if(atom->isActive()) nactive++;
	}
	
	
			
	/// Assign active sampling atoms to groups based on their location.
	for(int g = 1; g < MAX_GROUP_COUNT; g++) {
		const Group* group = simulation()->group(g);
		if(!group) continue;
		if(!group->region()) {
			Q_FOREACH(SamplingAtom* atom, samplingAtoms()) {
				if(atom->isActive()) atom->addToGroup(group);
			}
		}
		else {		
			Q_FOREACH(SamplingAtom* atom, samplingAtoms()) {
				if(atom->isActive() && group->region()->contains(atom->pos())) 
					atom->addToGroup(group);
				else
					MAFEM_ASSERT(!atom->belongsToGroup(group));
			}
		}	
	}
	_activesampatoms = nactive;
	MsgLogger() << "  Total time for sampling cluster generation:" << clusterTimer.elapsed() << "sec." << endl;
	MsgLogger() << "  Number of active sampling atoms:" << nactive << endl;
	MsgLogger() << "  Number of passive sampling atoms:" << (samplingAtoms().size() - nactive) << endl;
	MAFEM_ASSERT_MSG(nactive <= repatoms().size() || simulation()->settings().clusterRadius > 0, "Mesh::buildSamplingClusters()", "Number of active sampling atoms is higher than number of repatoms.");
	
	// The sampling weights have to be recalculated now.
	//
	calculateClusterWeights();
	//calculateShapeFunctionBasedClusterWeights();
	
	// And the neighbor lists have to be rebuilt as well.
	_samplingAtoms.buildNeighborLists(simulation());
}

/******************************************************************************
* This recalculates the deformed positions of all sampling atoms.	
* This method should be called after all repatoms have been moved
* to their new positions.
******************************************************************************/
void Mesh::deformationUpdate()
{
	const FloatType nnThreshold = simulation()->settings().neighborSkinThickness / 2.0;
	const FloatType clusterThreshold = simulation()->settings().clusterSkinThickness / 2.0;
	FloatType maxAtomMovement = 0;
	
	Q_FOREACH(const shared_ptr<DisplacementBoundaryCondition>& displBC, simulation()->displacementBC())
		displBC->updatePositions(simulation()->mesh());

	// Let all PBC image mesh nodes follow their master repatom.
	Q_FOREACH(RepAtom* node, nodes())
	{
		if(node->periodicImageMaster() != NULL)
		{
			// Compute image shift vector.
			Vector3 reducedDelta = simulation()->inverseSimulationCell() * (node->pos() - node->periodicImageMaster()->pos());
			node->setDeformedPos(node->periodicImageMaster()->deformedPos() + simulation()->deformedSimulationCell() * reducedDelta);
		}
		else
		{
			// Detect large movements that trigger neighbor list building.
			Vector3 motion = node->deformedPos() - node->lastNeighborPos();
			maxAtomMovement = max(maxAtomMovement, abs(motion.X));
			maxAtomMovement = max(maxAtomMovement, abs(motion.Y));
			maxAtomMovement = max(maxAtomMovement, abs(motion.Z));

			// Detect large movements that exceed the sampling cluster skin thickness.
			//Vector3 displacement = node->displacement();
			//if(abs(displacement.X) > clusterThreshold || abs(displacement.Y) > clusterThreshold || abs(displacement.Z) > clusterThreshold)
			//	throw Exception(QString("Large repatom movement detected. The displacement of repatom %1 is %2. Please increase the cluster skin thickness.").arg(node->index()).arg(displacement.toString()));
		}
	}
	
	// Recompute sampling atom positions.
	Q_FOREACH(SamplingAtom* atom, samplingAtoms()) {
		Point3 p(ORIGIN + simulation()->deformedSimulationCell() * atom->imageShift());
		
		if(atom->samplingNode() != NULL) {
			p.X += atom->samplingNode()->deformedPos().X;
			p.Y += atom->samplingNode()->deformedPos().Y;
			p.Z += atom->samplingNode()->deformedPos().Z;
		}
		else if(atom->element() != NULL) {
			// Compute the deformed position using linear interpolation of the four element nodes.
			
			for(int j=0; j<NEN; j++) {
				const Point3& d = atom->element()->vertex(j)->deformedPos();
				p.X += d.X * atom->barycentric()[j];
				p.Y += d.Y * atom->barycentric()[j];
				p.Z += d.Z * atom->barycentric()[j];
			}
		}
		else {
			MAFEM_ASSERT_MSG(false, "Mesh::deformationUpdate()", "Detected dangling sampling atom.");
		}
		atom->setDeformedPos(p);
	}
	
	if(maxAtomMovement > nnThreshold) {
		MsgLogger() << "Atomic movement exceeded neighbor list skin thickness. Old neighbor lists become invalid. Maximum motion was" << maxAtomMovement << endl;
		_samplingAtoms.buildNeighborLists(simulation());
	}
}

void Mesh::resetPositions()
{
	for (int i = 0; i < samplingAtoms().size(); ++i)
		samplingAtoms()[i]->setPos(samplingAtoms()[i]->deformedPos());
}

/******************************************************************************
* Calculates the weights for all sampling clusters.
******************************************************************************/
void Mesh::calculateClusterWeights()
{
	using namespace ublas;
	MsgLogger() << "  Calculating cluster weights." << endl;
	
	// Measure the time it taskes to calculate the weights.
	timer weightTimer;
	
	// Allocate the vector that contains the number of sites belonging to each repatom. 
	ublas::vector<int> n(repatoms().size());

	// This is used to count the number of active sampling atoms.
	int nactiveSamplingAtoms = 0;
	
	// Assign indices to repatoms.
	for(int i=0; i<repatoms().size(); i++) {
		RepAtom* repatom = repatoms()[i];
		repatom->setIndex(i);		
		n(i) = repatom->numSamplingAtoms();
		nactiveSamplingAtoms += repatom->numSamplingAtoms();
	}	
	
	// This is used to count the total number of lattice sites in the solid.
	int numberOfLatticeSites = repatoms().size();
	
	const AffineTransformation simCell = simulation()->simulationCell();
	const AffineTransformation inverseSimCell = simulation()->inverseSimulationCell();
	const array<bool,NDOF> pbc = simulation()->settings().periodicBC;
	int max_x = (pbc[0]) ? 1 : 0;
	int max_y = (pbc[1]) ? 1 : 0;
	int max_z = (pbc[2]) ? 1 : 0;

	bool rcheck = false;

	// The first loop is over all grains and the second one over the lattice sites of each grain.
	Q_FOREACH(const shared_ptr<Grain>& grain, simulation()->grains()) {
		// Computer the bounding box of the simulation cell in lattice space.
		Box3 latticeBoundingBoxFrac = (grain->lattice().inverseLatticeVectors() * simulation()->simulationCell()) * Box3(Point3(0,0,0), Point3(1,1,1));
		Box3I bbox;
		for(int k=0; k<NDOF; k++) {
			bbox.minc[k] = (int)floor(latticeBoundingBoxFrac.minc[k]);
			bbox.maxc[k] = (int)ceil(latticeBoundingBoxFrac.maxc[k]);
		}
		
		// Iterate over all lattice sites. 
		Point3I indices;
		for(indices.X = bbox.minc.X; indices.X <= bbox.maxc.X; indices.X++) {
			for(indices.Y = bbox.minc.Y; indices.Y <= bbox.maxc.Y; indices.Y++) {
				for(indices.Z = bbox.minc.Z; indices.Z <= bbox.maxc.Z; indices.Z++) {
					
					// Find the element the current site is located in.

					LatticeSite site(&grain->lattice(), indices);
					/*rcheck = false;
					for(int i=0; i<_removeSite.size(); i++){
						if(_removeSite[i]->equals(site.pos(),FLOATTYPE_EPSILON)) { rcheck=true; break; }
					}

					if(rcheck) { continue;}*/

					Element* element;
					if(simulation()->settings().segmentTreeElementSearch) element = elements().findAt(site);
					if(simulation()->settings().octreeElementSearch) element = FindElement(site,grain);
					if(!element) continue;
					
					// The sites which are occupied by repatoms have already been accounted for.
					// Make sure we don't count them twice.
					RepAtom* siteNode = nodes().findAt(site); 
					if(siteNode) continue;
					// Here we have to deal only with sites that are in the interior of elements.
					
					// Do not count sites that are on the periodic simulation cell boundary.
					Point3 reducedPoint = inverseSimCell * site.pos();
					if(pbc[0] && reducedPoint.X >= 1.0-FLOATTYPE_EPSILON) continue;
					if(pbc[1] && reducedPoint.Y >= 1.0-FLOATTYPE_EPSILON) continue;
					if(pbc[2] && reducedPoint.Z >= 1.0-FLOATTYPE_EPSILON) continue;

					numberOfLatticeSites++;
					
					// We now check if the lattice site is occupied by a sampling atom. If this is the case
					// then the site is already counted to the sampling atom's cluster. 
					// But due to periodic boundary conditions we might miss the active sampling atom.
					// We therefore have to look also at the opposite cell sides for an active sampling atom.
					bool ignore = false;
					for(int ix=-max_x; ix<=max_x && !ignore; ix++) {
						for(int iy=-max_z; iy<=max_y && !ignore; iy++) {
							for(int iz=-max_z; iz<=max_z; iz++) {
								LatticeSite opposite_site(&grain->lattice(), site.pos() + simCell * Vector3I(ix, iy, iz));
								MAFEM_ASSERT(opposite_site.pos().equals(site.pos() + simCell * Vector3I(ix, iy, iz), FLOATTYPE_EPSILON));
								const SamplingAtom* samplingAtom = samplingAtoms().findAt(opposite_site);
								if(samplingAtom && samplingAtom->isActive()) {
									ignore = true;
									break;
								}
							}
						}							
					}
					if(ignore)	// We have found an active sampling atom at a pbc image site of the current site.
						continue;
				
					// Now comes the Voronoi method.
					// Find the closest element vertex.
					RepAtom* closestVertex = element->vertex(0);
					FloatType closestDistance = DistanceSquared(site.pos(), closestVertex->pos());
					
					for(int i=1; i<NEN; i++) {
						RepAtom* vertex = element->vertex(i);
						FloatType distance = DistanceSquared(site.pos(), vertex->pos());
						if(distance >= closestDistance) 
							continue;						
						closestDistance = distance;
						closestVertex = vertex;
					}
					n(closestVertex->realRepatom()->index()) += 1;					
					MAFEM_ASSERT(n(closestVertex->realRepatom()->index()) >= closestVertex->realRepatom()->numSamplingAtoms());
				}
			}
		}
	}

	// Save cluster weights in RepAtom field.
	ublas::vector<int>::const_iterator niter = n.begin();
	for(QVector<RepAtom*>::const_iterator repatom = repatoms().constBegin(); repatom != repatoms().constEnd(); ++repatom, ++niter) {
		(*repatom)->setClusterWeight((FloatType)*niter / (FloatType)(*repatom)->numSamplingAtoms());
		if((*repatom)->clusterWeight() < 1.0) {
			MsgLogger() << "WARNING: Detected cluster weight less than 1.0 at repatom" << (*repatom)->index() << ".";
			MsgLogger() << *niter << "lattice sites represented by" << (*repatom)->numSamplingAtoms() << "atoms." << endl;
		}
	}
	n.clear();
	
	// Save sampling atom weights in SamplingAtom field.
	// Each sampling atom in one cluster gets the same weight.
	int nactive = 0;
	for(QVector<SamplingAtom*>::const_iterator sampling_atom = samplingAtoms().constBegin(); sampling_atom != samplingAtoms().constEnd(); ++sampling_atom) {
		if((*sampling_atom)->isActive()) {
			(*sampling_atom)->setWeight((*sampling_atom)->clusterRepatom()->clusterWeight());
			nactive++;
		}
	}

	MAFEM_ASSERT(nactiveSamplingAtoms == nactive);
	
	MsgLogger() << "    Total time for cluster weight calculation:" << weightTimer.elapsed() << "sec." << endl;
	MsgLogger() << "    Total number of lattice sites in crystal:" << numberOfLatticeSites << endl;
	MsgLogger() << "    Active sampling atoms / lattice sites ratio:" << FloatType(nactiveSamplingAtoms) / FloatType(numberOfLatticeSites) * 100.0 << "%" << endl;
	
//#ifdef _DEBUG
	// Check sampling weights.
	FloatType latticeSum = 0;
	Q_FOREACH(const SamplingAtom* a, samplingAtoms()) {
		if(a->isPassive()) continue;
		latticeSum += a->weight();
		MAFEM_ASSERT_MSG(a->weight() > 0, "Mesh::calculateClusterWeights()", "Non-positive cluster weight detected.");
	}
	
	NumOfLatticeSites = numberOfLatticeSites;
	MsgLogger() << "    Sum of cluster weights:" << latticeSum << nactiveSamplingAtoms << numberOfLatticeSites << endl;
	//MAFEM_ASSERT(abs(latticeSum - (FloatType)numberOfLatticeSites) < FLOATTYPE_EPSILON);
//#endif

	if(nactiveSamplingAtoms > numberOfLatticeSites)
		throw Exception("Fatal error: Number of active sampling atoms exceeds total number of lattice sites.");
}

void Mesh::calculateShapeFunctionBasedClusterWeights()
{
	using namespace ublas;
	MsgLogger() << "  Calculating cluster weights." << endl;

	// Measure the time it taskes to calculate the weights.

	// Allocate the vector that contains the number of sites belonging to each repatom.
	ublas::matrix<FloatType> A(repatoms().size(),repatoms().size());
	ublas::vector<FloatType> n(repatoms().size());
	ublas::vector<FloatType> b(repatoms().size());
	// This is used to count the number of active sampling atoms.
	int nactiveSamplingAtoms = 0;

	// Assign indices to repatoms.
	for(int i=0; i<repatoms().size(); i++) {
		for(int j=0; j<repatoms().size(); j++) {
			if(i==j) { A(i,j) = 1.0;}
			else{ A(i,j) = 0.0; }
		}
		n(i) = 1.0;
		b(i) = 0.0;
		repatoms()[i]->setIndex(i);
	}

	// This is used to count the total number of lattice sites in the solid.
	int numberOfLatticeSites = repatoms().size();

	const AffineTransformation simCell = simulation()->simulationCell();
	const AffineTransformation inverseSimCell = simulation()->inverseSimulationCell();
	const array<bool,NDOF> pbc = simulation()->settings().periodicBC;

	// The first loop is over all grains and the second one over the lattice sites of each grain.
	Q_FOREACH(const shared_ptr<Grain>& grain, simulation()->grains()) {
		// Computer the bounding box of the simulation cell in lattice space.
		Box3 latticeBoundingBoxFrac = (grain->lattice().inverseLatticeVectors() * simulation()->simulationCell()) * Box3(Point3(0,0,0), Point3(1,1,1));
		Box3I bbox;
		for(int k=0; k<NDOF; k++) {
			bbox.minc[k] = (int)floor(latticeBoundingBoxFrac.minc[k]);
			bbox.maxc[k] = (int)ceil(latticeBoundingBoxFrac.maxc[k]);
		}

		// Iterate over all lattice sites.
		Point3I indices;
		for(indices.X = bbox.minc.X; indices.X <= bbox.maxc.X; indices.X++) {
			for(indices.Y = bbox.minc.Y; indices.Y <= bbox.maxc.Y; indices.Y++) {
				for(indices.Z = bbox.minc.Z; indices.Z <= bbox.maxc.Z; indices.Z++) {

					// Find the element the current site is located in.
					LatticeSite site(&grain->lattice(), indices);
					Element* element;
					if(simulation()->settings().segmentTreeElementSearch) element = elements().findAt(site);
					if(simulation()->settings().octreeElementSearch) element = FindElement(site,grain);
					if(!element) continue;

					// The sites which are occupied by repatoms have already been accounted for.
					// Make sure we don't count them twice.
					RepAtom* siteNode = nodes().findAt(site);
					if(siteNode) continue;
					// Here we have to deal only with sites that are in the interior of elements.

					numberOfLatticeSites++;

					Vector4 baryc =  element->computeBarycentric(site.pos());

					for(int i=0; i<NEN; i++) {
						RepAtom* vertex = element->vertex(i);
						n(vertex->realRepatom()->index()) += baryc[i];
					}

					SamplingAtom* samp =  samplingAtoms().findAt(site);
					if(!samp) continue;
					if(samp->isPassive()) continue;

					baryc = samp->barycentric();

					for(int i=0; i<NEN; i++) {
						RepAtom* vertex = element->vertex(i);
						A(vertex->realRepatom()->index(),samp->clusterRepatom()->realRepatom()->index()) += baryc[i];
					}

				}
			}
		}
	}

	/*for(int i=0; i<repatoms().size(); i++) {
		for(int j=0; j<repatoms().size(); j++) {
			MsgLogger() << A(i,j) << "";
		}
		MsgLogger() << "" << endl;
	}*/

	MsgLogger() << "Solving system of linear equations...." << endl;
	size_t iter = bicg_solve(A, b, n, 1000, 1e-6 * FLOATTYPE_EPSILON);
	MsgLogger() << iter << "iterations" << endl;
	for(QVector<RepAtom*>::const_iterator repatom = repatoms().constBegin(); repatom != repatoms().constEnd(); ++repatom) {
		(*repatom)->setClusterWeight(b((*repatom)->index()));

	}

	// Save sampling atom weights in SamplingAtom field.
	// Each sampling atom in one cluster gets the same weight.
	int nactive = 0;
	for(QVector<SamplingAtom*>::const_iterator sampling_atom = samplingAtoms().constBegin(); sampling_atom != samplingAtoms().constEnd(); ++sampling_atom) {
		if((*sampling_atom)->isActive()) {
			(*sampling_atom)->setWeight((*sampling_atom)->clusterRepatom()->clusterWeight());
			nactive++;
		}
	}

	A.clear();
	b.clear();
	n.clear();

}


void Mesh::buildQuadPoints()
{

	int elementFaceMap[4][3] = {
	{1, 3, 2}, {0, 2, 3}, {0, 3, 1}, {0, 1, 2}
	};

	int elementEdgeMap[6][2] = {
		{0, 1}, {0, 2}, {0, 3}, {1, 2}, {1, 3}, {2, 3}
	};
	if(simulation()->potential() == NULL)
		throw Exception("The interatomic potential has not been initialized.");

	Box3 simBox = simulation()->simulationCell() * Box3(Point3(0,0,0),Point3(1,1,1));
	int numThread = simulation()->settings().openMPThread;
	if(numThread > omp_get_max_threads()) numThread =  omp_get_max_threads();

	const SimulationSettings& settings = simulation()->settings();
	const AffineTransformation simCell = simulation()->simulationCell();
	const AffineTransformation inverseSimCell = simulation()->inverseSimulationCell();
	const array<bool,NDOF> pbc = settings.periodicBC;
	FloatType potentialCutoff = simulation()->potential()->cutoffRadius;

	// The radius up to which sampling atoms are generated around repatoms.
	const FloatType largeRadius = settings.clusterSkinThickness + potentialCutoff;
	const FloatType largeRadiusSquared = square(largeRadius);
	int maxQuadPoints = 4;
	// Clear previous sampling atoms.
	if(!samplingAtoms().empty()) {
		MsgLogger() << "  Clearing previous sampling atoms." << endl;
		_samplingAtoms.clear();
		_samplingAtomPool.purge_memory();

		// Reset sampling atom counters.
		Q_FOREACH(RepAtom* repatom, repatoms()) {
			repatom->setNumSamplingAtoms(0);
			repatom->setClusterWeight(0.0);
		}
	}

	for(int i = 0; i < repatoms().size(); i++) {
		repatoms()[i]->setIndex(i);
	}

	//// assigning the quadrature points to the element


	for(int i=0; i< elements().size(); i++){
		elements()[i]->SetIndex(i);
		elements()[i]->setNumSiteContains(0);
		elements()[i]->setNumQuadPoints(0);
		elements()[i]->setNumFaceAtoms(0);

	}

int numlattices = repatoms().size();
int check = -1;
int val = 0;
	Q_FOREACH(const shared_ptr<Grain>& grain, simulation()->grains()) {

		Box3 latticeBoundingBoxFrac = (grain->lattice().inverseLatticeVectors() * simulation()->simulationCell()) * Box3(Point3(0,0,0), Point3(1,1,1));
		Box3I bbox;
		for(int k=0; k<NDOF; k++) {
			bbox.minc[k] = (int)floor(latticeBoundingBoxFrac.minc[k]);
			bbox.maxc[k] = (int)ceil(latticeBoundingBoxFrac.maxc[k]);
		}
//		progress_display progress((bbox.sizeX()+1) * (bbox.sizeY()+1));
		// Iterate over all lattice sites.
/*		Point3I indices;
		for(indices.X = bbox.minc.X; indices.X <= bbox.maxc.X; indices.X++) {
			for(indices.Y = bbox.minc.Y; indices.Y <= bbox.maxc.Y; indices.Y++) {
				for(indices.Z = bbox.minc.Z; indices.Z <= bbox.maxc.Z; indices.Z++) {*/
	
	#pragma omp parallel for reduction(+:numlattices) num_threads(numThread)		
		for(int x = bbox.minc.X; x <= bbox.maxc.X; x++) {
		#pragma omp parallel for reduction(+:numlattices) num_threads(numThread)		  
			for(int y = bbox.minc.Y; y <= bbox.maxc.Y; y++) {
			#pragma omp parallel for reduction(+:numlattices) num_threads(numThread)
				for(int z = bbox.minc.Z; z <= bbox.maxc.Z; z++) {
				  
				  
					
					Point3I indices(x,y,z);

					LatticeSite site(&grain->lattice(), indices);

					RepAtom* siteNode = nodes().findAt(site);
					if(siteNode) continue;




					Element* element;
#pragma omp critical
{
					if(simulation()->settings().segmentTreeElementSearch) element = elements().findAt(site);
					if(simulation()->settings().octreeElementSearch) element = FindElement(site,grain);
}

					if(!element) continue;


#pragma omp critical
{



					numlattices+=1;
					val = simBox.classifyPoint(site.pos());
					check = element->onFace(site.indices());
					
					QVector<Element*> shared = elements().findContainingElements(site);
					if(val==0 && shared.size() <= 1){
						element->setNumFaceAtoms(element->getNumFaceAtoms()+1);
					}
					
					if(check==-1){ element->setNumSiteContains(element->getNumSiteContains()+1);
						element->setEleAtomSites(indices);
				   	}
					
					if(check!=-1 && shared.size() > 1 ){
						FloatType value = 1.0/shared.size();
						for(int i = 0; i < shared.size(); i++){
							shared.at(i)->setNumFaceAtoms(shared.at(i)->getNumFaceAtoms()+value);
						}	
					}
					shared.clear();
}
				}
//				++progress;
			}
		}
	}

#pragma omp parallel for  num_threads(numThread)
	for(int i=0; i< elements().size(); i++){


#pragma omp critical
{
		if(elements()[i]->getNumSiteContains() <= maxQuadPoints){
			elements()[i]->setNumQuadPoints(elements()[i]->getNumSiteContains());
		}else{
			elements()[i]->setNumQuadPoints(maxQuadPoints);
		}
}
	}



	

//progress_display progress1(elements().size());
#pragma omp parallel for  num_threads(numThread)
	for(int i = 0; i < elements().size(); i++){
//			++progress1;
			Element* element = elements()[i];

			int numquadpoints = element->getNumQuadPoints();

			int numsitecontains = element->getNumSiteContains();

			int count  = 0;

			if(numsitecontains >0  and numsitecontains <= numquadpoints){

				for(int j = 0; j < element->getEleAtomSitesVector()->size(); j++){

				element->setQuadPoints(element->getEleAtomSites(j));

				}

			}



			if(numsitecontains > numquadpoints){


				for(int j = 0; j < NEN; j++){

					RepAtom* rep = element->vertex(j);



					FloatType vx = ( element->vertex(elementFaceMap[j][0])->pos().X + element->vertex(elementFaceMap[j][1])->pos().X + element->vertex(elementFaceMap[j][2])->pos().X ) / 3.0;

					FloatType vy = ( element->vertex(elementFaceMap[j][0])->pos().Y + element->vertex(elementFaceMap[j][1])->pos().Y + element->vertex(elementFaceMap[j][2])->pos().Y ) / 3.0;

					FloatType vz = ( element->vertex(elementFaceMap[j][0])->pos().Z + element->vertex(elementFaceMap[j][1])->pos().Z + element->vertex(elementFaceMap[j][2])->pos().Z ) / 3.0;

					Vector3 p(vx,vy,vz);

					Vector3 q((p.X - rep->pos().X)/ 3.0 ,(p.Y - rep->pos().Y) / 3.0,(p.Z - rep->pos().Z)/ 3.0);

					Point3 pos(q.X + rep->pos().X,q.Y + rep->pos().Y,q.Z + rep->pos().Z);

					int index = 0;

					FloatType mindist = 1e8;



						for(int k = 0; k < element->getEleAtomSitesVector()->size(); k++){


							LatticeSite site(element->lattice(), element->getEleAtomSites(k));

							FloatType sitedist = DistanceSquared(site.pos(), pos);

							if(mindist >= sitedist){

								bool check = false;
								for(int n = 0; n < element->numOfQuadPoints(); n++){
								
									if(site.indices() == element->getQuadPoint(n)) {check = true;}
								}

								if(check) continue;

								index = k;

								mindist = sitedist;

							}
						}
						element->setQuadPoints(element->getEleAtomSites(index));
				}
			}
		}

	// Create a search tree for nearest node searching.
	typedef CGAL::Search_traits<FloatType, Point3, const FloatType*, Construct_coord_iterator1> Traits1;
	typedef CGAL::Orthogonal_k_neighbor_search<Traits1, Distance1> K_neighbor_search1;
	typedef K_neighbor_search1::Tree Tree1;

	vector<Point3> inputList;

	for(int i = 0; i < repatoms().size(); i++)
	{
		inputList.push_back(repatoms()[i]->pos());
	}

	for(int e=0; e < elements().size(); e++)
	{
		Element* element =  elements()[e];
		for(int n = 0; n < element->numOfQuadPoints(); n++ ){
			LatticeSite quadsite(element->lattice(), element->getQuadPoint(n));
			inputList.push_back(quadsite.pos());	
		}
	}

	Tree1 tree1(inputList.begin(), inputList.end());
	// Insert all repatoms into the search tree.
	//Tree tree(repatoms().begin(), repatoms().end());	

	// The first loop is over all grains and the second one over the lattice sites of each grain.
	Q_FOREACH(const shared_ptr<Grain>& grain, simulation()->grains()) {
		// Computer the bounding box of the simulation cell in lattice space.		
		Box3 simBox = simulation()->simulationCell() * Box3(Point3(0,0,0), Point3(1,1,1));
		Box3 latticeBoundingBoxFrac = grain->lattice().inverseLatticeVectors() * simBox.padBox(largeRadius);
		Box3I bbox;
		for(int k=0; k<NDOF; k++) {
			bbox.minc[k] = (int)floor(latticeBoundingBoxFrac.minc[k]);
			bbox.maxc[k] = (int)ceil(latticeBoundingBoxFrac.maxc[k]);
		}
		progress_display progress((bbox.sizeX()+1) * (bbox.sizeY()+1));
		
		// Iterate over all lattice sites. 
		//Point3I indices;
	#pragma omp parallel for  num_threads(numThread)		
		for(int x = bbox.minc.X; x <= bbox.maxc.X; x++) {
		#pragma omp parallel for  num_threads(numThread)		  
			for(int y = bbox.minc.Y; y <= bbox.maxc.Y; y++) {
			#pragma omp parallel for  num_threads(numThread)
				for(int z = bbox.minc.Z; z <= bbox.maxc.Z; z++) {
				  
				  
					
					Point3I indices(x,y,z);

					// Is there a node at this lattice site?
					LatticeSite site(&grain->lattice(), indices);

					RepAtom* siteNode = nodes().findAt(site); 
					if(siteNode) {
					  
						// If it is a repatom then create an active sampling atom at this site.
						if (siteNode->realRepatom() == siteNode) {
#pragma omp critical
{							
							MAFEM_ASSERT(samplingAtoms().findAt(site) == NULL);							
							SamplingAtom* samplingAtom = static_cast<SamplingAtom*>(_samplingAtomPool.malloc());
							new (samplingAtom) SamplingAtom(site);
							samplingAtom->initialize(NULL, site.pos(), NULL_VECTOR, siteNode, siteNode, 0);
							_samplingAtoms.push_back(samplingAtom);
							siteNode->setNumSamplingAtoms(siteNode->numSamplingAtoms() + 1);
}							
							continue;
						}
					}
					
					// Wrap around position when periodic boundary conditions are applied.
					Point3 reducedPoint = simulation()->inverseSimulationCell() * site.pos();
					Point3 imagePos = site.pos();
					Vector3I imageShift;
					
					for(int k=0; k<NDOF; k++) {
						if(settings.periodicBC[k]) {
							imageShift[k] = (int) floor(reducedPoint[k] + FLOATTYPE_EPSILON);
							imagePos.X -= simCell.column(k).X * (imageShift[k]);
							imagePos.Y -= simCell.column(k).Y * (imageShift[k]);
							imagePos.Z -= simCell.column(k).Z * (imageShift[k]);
						}
						else imageShift[k] = 0;
					}
					
					
					// This is the wrapped around lattice site:
					LatticeSite imageSite(&grain->lattice(), imagePos);
					MAFEM_ASSERT_MSG(imageSite.pos().equals(imagePos, FLOATTYPE_EPSILON), "Mesh::buildSamplingClusters()", "Non-periodic lattice detected. Lattice is not compatible with periodic boundary conditions and simulation cell size.");
					MAFEM_ASSERT(imageShift != NULL_VECTOR || imageSite.pos() == site.pos());

					// Check if there is a node at the image position.
					RepAtom* imageNode = nodes().findAt(imageSite);
					
					// Find the element the current site is located in.
					Element* element;
#pragma omp critical
{					
					if(simulation()->settings().segmentTreeElementSearch) element = elements().findAt(imageSite);
					if(simulation()->settings().octreeElementSearch) element = FindElement(imageSite,grain);
}					
					
					// If the lattice site is not inside an element and not on a node than it is outside the crystal.
					if(!element && !imageNode) continue;
					if(imageNode) element = NULL;

Element* el;
#pragma omp critical
{					
					if(simulation()->settings().segmentTreeElementSearch) el = elements().findAt(site);
					if(simulation()->settings().octreeElementSearch) el = FindElement(site,grain);
}					
					FloatType closestDistanceSquared;
			
					RepAtom* closestRepatom;	
					RepAtom* samplingClusterRepatom = NULL;

/*					int val =-1;
					int index = 0;
					if(el){
#pragma omp critical
{
						for(int n = 0; n < el->numOfQuadPoints(); n++ ){
							if(site.indices() == el->getQuadPoint(n)) { 
								samplingClusterRepatom = el->vertex(0); break;
							}
						}

						int check = el->onFace(site.indices());
					
						if(el->getNumSiteContains()==0){

							samplingClusterRepatom = el->vertex(0);

							if(val==-1) val = simBox.classifyPoint(site.pos());
					
							QVector<Element*> del = elements().findContainingElements(site);
							FloatType v = 0;
							if(del.size() > 1){
								v = 1.0/del.size();
								for(int k = 0; k < del.size(); k++){
									del.at(k)->setNumFaceAtoms(del.at(k)->getNumFaceAtoms()-v);
								}
							}
							del.clear();
							if(val== 0 && el->isMarked()==false) {
								el->setNumFaceAtoms(el->getNumFaceAtoms()-1);
								el->setMarked(true);
							}
						}
}
					}*/
					int val =-1;
					int index = 0;
					
#pragma omp critical
{
					if(el){
						for(int n = 0; n < el->numOfQuadPoints(); n++ ){
							if(site.indices() == el->getQuadPoint(n)) { 
								samplingClusterRepatom = el->vertex(0); break;
							}
						}
					}
					if(!samplingClusterRepatom) {
						bool find = false;
						int index = 0;
						QVector<Element*> del = elements().findContainingElements(site);
						
						FloatType v = 0;
						for(int d = 0; d < del.size(); d++){
							if(del.at(d)->numOfQuadPoints()==0) {
								samplingClusterRepatom = del.at(d)->vertex(0);
								find = true;
								index = d;
								break;
							}
						}
						if(find){
							if(del.size() > 1){
								v = 1.0/del.size();
								for(int k = 0; k < del.size(); k++){
									del.at(k)->setNumFaceAtoms(del.at(k)->getNumFaceAtoms()-v);
								}
							}
							val = simBox.classifyPoint(site.pos());
							if(val== 0 && del.size() <=1 && del.at(index)->isMarked()==false) {
								del.at(index)->setNumFaceAtoms(del.at(index)->getNumFaceAtoms()-1);
								del.at(index)->setMarked(true);
							}
						}
						del.clear();
					}
}		
					if(!samplingClusterRepatom) {
#pragma omp critical
{
					K_neighbor_search1 search1(tree1, site.pos(), 1);
					MAFEM_ASSERT(search1.begin() != search1.end());										
					closestDistanceSquared = search1.begin()->second;
}

					if(closestDistanceSquared > largeRadiusSquared) continue;

					}
				
#pragma omp critical
{					
					
					MAFEM_ASSERT(samplingAtoms().findAt(site) == NULL);							
					SamplingAtom* samplingAtom = static_cast<SamplingAtom*>(_samplingAtomPool.malloc());
					new (samplingAtom) SamplingAtom(site);
					samplingAtom->initialize(element, imageSite.pos(), imageShift, samplingClusterRepatom, imageNode, 0);
					_samplingAtoms.push_back(samplingAtom);
					
}			
				}

				++progress;
			}
		}
	}

inputList.clear();



			MsgLogger() << "Removing duplicate sampling atoms." << endl;

			int max_x = (pbc[0]) ? 1 : 0;
			int max_y = (pbc[1]) ? 1 : 0;
			int max_z = (pbc[2]) ? 1 : 0;

			// Here we make sure that periodic images of active sampling atoms are not counted twice.
			// That is we find all groups of periodic images that belong together and then decide which one
			// of the sampling atoms is taken as an active atom. The decision is based on the
			// distances of the sampling atoms to their respective central repatom.
			Q_FOREACH(SamplingAtom* samplingAtom, samplingAtoms()) {

				// Skip passive sampling atoms. We are only interested in active ones.
				if(samplingAtom->clusterRepatom() == NULL)
					continue;
				// Skip atoms inside the simulation cell here. They will be visited later
				// if they have a periodic image outside of the simulation cell.
				if(samplingAtom->imageShift() == NULL_VECTOR)
					continue;

				SamplingAtom* closestSamplingAtom = samplingAtom;
				RepAtom* closestRepatom = samplingAtom->clusterRepatom();
				FloatType closestDistance = samplingAtom->squaredDistanceToCenter();
				closestRepatom->setNumSamplingAtoms(closestRepatom->numSamplingAtoms() - 1);
				samplingAtom->setClusterRepatom(NULL, 0);

				Vector3I indices;
				for(indices.X = -max_x; indices.X <= max_x; indices.X++) {
					for(indices.Y = -max_y; indices.Y <= max_y; indices.Y++) {
						for(indices.Z = -max_z; indices.Z <= max_z; indices.Z++) {

							// Look for another sampling atom at the other side of the box.
							Point3 imagePos = samplingAtom->pos() - simulation()->simulationCell() * indices;
							LatticeSite imageSite(samplingAtom->site().lattice(), imagePos);
							MAFEM_ASSERT_MSG(imageSite.pos().equals(imagePos, FLOATTYPE_EPSILON), "Mesh::buildSamplingClusters()", "Non-periodic lattice detected. Lattice is not compatible with periodic boundary conditions and simulation cell size.");

							SamplingAtom* imageAtom = samplingAtoms().findAt(imageSite);
							if(imageAtom == NULL) continue;
							MAFEM_ASSERT(imageAtom->imageShift() == samplingAtom->imageShift() - indices);
							if(imageAtom->clusterRepatom() == NULL) continue;

							// If periodic boundary conditions are enabled then multiple sampling atoms could have been created at the same
							// lattice site but assigned to different repatoms. This has to be resolved by comparing the respective
							// distances to the repatoms.

							MAFEM_ASSERT(imageAtom->clusterRepatom() != samplingAtom->clusterRepatom());
							if(closestDistance+FLOATTYPE_EPSILON > imageAtom->squaredDistanceToCenter()) {
								// The sampling atom is equally distant to two or more nodes.
								// This ambiguity has to be resolved using an additional criterium.
								// We use the number of sampling atoms per repatom to balance the weights.
								if(closestDistance-FLOATTYPE_EPSILON > imageAtom->squaredDistanceToCenter() || imageAtom->clusterRepatom()->numSamplingAtoms() <= closestRepatom->numSamplingAtoms()) {
									closestRepatom = imageAtom->clusterRepatom();
									closestDistance = imageAtom->squaredDistanceToCenter();
									closestSamplingAtom = imageAtom;
								}
							}
							CHECK_POINTER(imageAtom->clusterRepatom());
							imageAtom->setClusterRepatom(NULL, 0);
						}
					}
				}
				samplingAtom->setClusterRepatom(closestRepatom, closestDistance);
				closestRepatom->setNumSamplingAtoms(closestRepatom->numSamplingAtoms() + 1);
			}


			// Count active/passive sampling atoms.


			


			/// Assign active sampling atoms to groups based on their location.
			for(int g = 1; g < MAX_GROUP_COUNT; g++) {
				const Group* group = simulation()->group(g);
				if(!group) continue;
				if(!group->region()) {
					Q_FOREACH(SamplingAtom* atom, samplingAtoms()) {
						if(atom->isActive()) atom->addToGroup(group);
					}
				}
				else {
					Q_FOREACH(SamplingAtom* atom, samplingAtoms()) {
						if(atom->isActive() && group->region()->contains(atom->pos()))
							atom->addToGroup(group);
						else
							MAFEM_ASSERT(!atom->belongsToGroup(group));
					}
				}
			}


		int nactive = 0;
		Q_FOREACH(const SamplingAtom* atom, samplingAtoms()) {
			if(atom->isActive()) nactive++;
		}

		MsgLogger() << "Active sampling atoms: " << nactive << endl;
			_activesampatoms = nactive;

			FloatType totsite = 0.0;
			int totnode = 0;



#pragma omp parallel for num_threads(numThread)
		for(int i = 0; i < samplingAtoms().size(); i++){
			SamplingAtom* samp = samplingAtoms()[i];

			if(samp->isActive()){ 
				if(samp->samplingNode()){ 
					samp->samplingNode()->setClusterWeight(1.0); 
					samp->setWeight(samp->samplingNode()->clusterWeight());
				}else{

					samp->setWeight(1.0);
				}
			}
		}




#pragma omp parallel for num_threads(numThread)
		for(int e=0; e < elements().size(); e++){

				Element* el = elements()[e];
				
				Point3 v1 = el->vertex(0)->deformedPos();
				Point3 v2 = el->vertex(1)->deformedPos();
				Point3 v3 = el->vertex(2)->deformedPos();
				Point3 v4 = el->vertex(3)->deformedPos();

				voronoicell v;
				v.init_tetrahedron(v1.X,v1.Y,v1.Z,v2.X,v2.Y,v2.Z,v3.X,v3.Y,v3.Z,v4.X,v4.Y,v4.Z);
				Point3 cent;
				v.centroid(cent.X,cent.Y,cent.Z);

	
				FloatType volume = v.volume();
				Plane3 pl1(v2,v3,v4,true);
				Plane3 pl2(v1,v3,v4,true);
				Plane3 pl3(v1,v2,v4,true);
				Plane3 pl4(v1,v2,v3,true);


				FloatType dist1 = abs(pl1.pointDistance(cent));
				FloatType dist2 = abs(pl2.pointDistance(cent));
				FloatType dist3 = abs(pl3.pointDistance(cent));
				FloatType dist4 = abs(pl4.pointDistance(cent));

				FloatType x_min,x_max,y_min,y_max,z_min,z_max;
				x_min = x_max = el->vertex(0)->deformedPos().X;
				y_min = y_max = el->vertex(0)->deformedPos().Y;
				z_min = z_max = el->vertex(0)->deformedPos().Z;

				for (int i = 1;  i < 4; i++){

					if(x_min>el->vertex(i)->deformedPos().X) x_min = el->vertex(i)->deformedPos().X;
					if(y_min>el->vertex(i)->deformedPos().Y) y_min = el->vertex(i)->deformedPos().Y;
					if(z_min>el->vertex(i)->deformedPos().Z) z_min = el->vertex(i)->deformedPos().Z;

					if(x_max<el->vertex(i)->deformedPos().X) x_max = el->vertex(i)->deformedPos().X;
					if(y_max<el->vertex(i)->deformedPos().Y) y_max = el->vertex(i)->deformedPos().Y;
					if(z_max<el->vertex(i)->deformedPos().Z) z_max = el->vertex(i)->deformedPos().Z;

				}

				 const int n_x=1,n_y=1,n_z=1;

				 FloatType tol = FLOATTYPE_EPSILON;
				 container con(x_min-cent.X-tol,x_max-cent.X+tol,y_min-cent.Y-tol,y_max-cent.Y+tol,z_min-cent.Z-tol,z_max-     cent.Z+tol,n_x,n_y,n_z,false,false,false,8);


				 wall_plane p1(v.normals(0,0),v.normals(0,1),v.normals(0,2),dist3);con.add_wall(p1);
				 wall_plane p2(v.normals(1,0),v.normals(1,1),v.normals(1,2),dist4);con.add_wall(p2);
				 wall_plane p3(v.normals(2,0),v.normals(2,1),v.normals(2,2),dist1);con.add_wall(p3);
				 wall_plane p4(v.normals(3,0),v.normals(3,1),v.normals(3,2),dist2);con.add_wall(p4);


				 con.put(0,v1.X-cent.X,v1.Y-cent.Y,v1.Z-cent.Z);
				 con.put(1,v2.X-cent.X,v2.Y-cent.Y,v2.Z-cent.Z);
				 con.put(2,v3.X-cent.X,v3.Y-cent.Y,v3.Z-cent.Z);
				 con.put(3,v4.X-cent.X,v4.Y-cent.Y,v4.Z-cent.Z);

				for(int i=0; i < el->getNumQuadPoints(); i++){

					LatticeSite site(el->lattice(), el->getQuadPoint(i));	
					SamplingAtom* samp = samplingAtoms().findAt(site);
					con.put(4+i,samp->deformedPos().X-cent.X,samp->deformedPos().Y-cent.Y,samp->deformedPos().Z-cent.Z);

				}

				FloatType val = (el->getNumFaceAtoms()+el->getNumSiteContains()-el->getNumQuadPoints());

				FloatType diff = abs(v.volume()-con.sum_cell_volumes());

				if(el->getNumSiteContains()==0){			


					
					FloatType value = el->getNumFaceAtoms();
					
				        el->setNumFaceAtoms(0.0);

					for(int j=0; j<4;j++){
						
#pragma omp critical
{
						el->vertex(j)->realRepatom()->setClusterWeight(el->vertex(j)->realRepatom()->clusterWeight()+(value*(con.cellVolume(j)/volume)));
						SamplingAtom* samp = samplingAtoms().findAt(el->vertex(j)->realRepatom()->site());
						samp->setWeight(el->vertex(j)->realRepatom()->clusterWeight());
}
					}
					con.clear();
					continue;

				}

				for(int j=0; j<4;j++){
#pragma omp critical
{			
					el->vertex(j)->realRepatom()->setClusterWeight(el->vertex(j)->realRepatom()->clusterWeight()+(val*(con.cellVolume(j)/volume)));
					SamplingAtom* samp = samplingAtoms().findAt(el->vertex(j)->realRepatom()->site());
					samp->setWeight(el->vertex(j)->realRepatom()->clusterWeight());
}
				}

				
				for(int i=0; i < el->getNumQuadPoints(); i++){

#pragma omp critical
{   				        
					LatticeSite site(el->lattice(), el->getQuadPoint(i));	
					SamplingAtom* samp = samplingAtoms().findAt(site);
					samp->setWeight(samp->weight()+(val*(con.cellVolume(4+i)/volume)));
}
			
				}	

				
/*				if(el->getNumSiteContains()==0) continue;

				for(int i=0; i < el->getNumQuadPoints(); i++){

#pragma omp critical
{   				        
					LatticeSite site(el->lattice(), el->getQuadPoint(i));	
					SamplingAtom* samp = samplingAtoms().findAt(site);
					samp->setWeight(samp->weight()+(1.0*val/el->getNumQuadPoints()));
}
			
				}	*/

				/*con.draw_particles("tetrahedron_p.gnu");
				con.draw_cells_gnuplot("tetrahedron_v.gnu");
				con.print_custom("%v","volume");*/
				con.clear();	
			    
		}

#pragma omp parallel for reduction(+:totsite) num_threads(numThread)
		for(int i = 0; i < samplingAtoms().size(); i++){
			SamplingAtom* samp = samplingAtoms()[i];

			if(samp->isActive()){ 
  				totsite+= samp->weight();
			}
		}



/*/// without cluster weight


		for(int i = 0; i < samplingAtoms().size(); i++){
			SamplingAtom* samp = samplingAtoms()[i];
			if(samp->isActive()){
				if(samp->samplingNode()){ samp->samplingNode()->setClusterWeight(samp->samplingNode()->clusterWeight()+1.0); samp->setWeight(samp->samplingNode()->clusterWeight()); totsite+=samp->weight(); totnode+=1;}
				else { samp->setWeight((1.0 * (samp->element()->getNumSiteContains()+samp->element()->getNumFaceAtoms()))/ samp->element()->getNumQuadPoints());  totsite += samp->weight(); 

				}
			}
		}
		/*for(int i = 0; i < elements().size(); i++){
			 MsgLogger() << "shared atoms" << numlattices << elements()[i]->getNumFaceAtoms()<< elements()[i]->getNumSiteContains()  << endl;
		}*/
	
		MsgLogger() << "Number of total lattice site: " << numlattices << endl;
		MsgLogger() << "Summation of weight: " << totsite <<  endl;

		for(int i=0; i < elements().size(); i++){
			elements()[i]->getEleAtomSitesVector()->clear();
			elements()[i]->getQuadPointVector()->clear();
		}

		// And the neighbor lists have to be rebuilt as well.
		_samplingAtoms.buildNeighborLists(simulation());		
}
}; // End of namespace MAFEM
