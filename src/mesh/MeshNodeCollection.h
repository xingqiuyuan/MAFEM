///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_MESH_NODE_COLLECTION_H
#define __MAFEM_MESH_NODE_COLLECTION_H

#include <MAFEM.h>
#include <atoms/RepAtom.h>
#include <atoms/AtomCollectionBase.h>

namespace MAFEM {

/**
 * \brief Stores the nodes of a mesh.
 * 
 * \author Alexander Stukowski
 */
class MeshNodeCollection : public AtomCollectionBase<RepAtom>
{
public:

	/// \brief Default constructor that creates an empty collection.
	MeshNodeCollection() {}
	
	/// \brief Copy constructor.
	/// \note This copy constructor is only needed for the Q_FOREACH macro. It does
	///       not copy the hashtable, only the plain list of nodes.
	MeshNodeCollection(const MeshNodeCollection& other) : AtomCollectionBase<RepAtom>(other) {}
	
private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) {
    	ar & boost::serialization::base_object< AtomCollectionBase<RepAtom> >(*this);
    }
	friend class boost::serialization::access;	
};

}; // End of namespace MAFEM

#endif // __MAFEM_MESH_NODE_COLLECTION_H
