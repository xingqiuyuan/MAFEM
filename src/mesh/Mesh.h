///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_MESH_H
#define __MAFEM_MESH_H

#include <MAFEM.h>
#include <simulation/SimulationResource.h>
#include <mesh/Element.h>
#include <mesh/MeshNodeCollection.h>
#include <mesh/ElementCollection.h>
#include <atoms/RepAtom.h>
#include <atoms/SamplingAtom.h>
#include <atoms/SamplingAtomCollection.h>


namespace MAFEM {


class CalculationResult;	// defined in CalculationResult.h

/**
 * \brief Stores the mesh that connects the representative atoms.
 * 
 * \author Alexander Stukowski
 */
class Mesh : public SimulationResource, boost::noncopyable
{
private:
	
	/// \brief Default constructor that creates an empty mesh.
	Mesh() : SimulationResource(), _samplingAtomPool(sizeof(SamplingAtom)){}

public:
	
	/// \brief Creates a new mesh node at the given lattice site and inserts it into the mesh.
	/// \param site The lattice site where the new node should be located. 
	RepAtom* createNode(const LatticeSite& site);
	
	/// \brief Create a new element and adds it to the mesh.
	/// \param vertices The four vertices of the tetrahedron element.
	/// \return The new element.
	Element* createElement(const array<RepAtom*, NEN>& vertices);

	/// \brief Adds an existing element to the mesh.
	/// \param element The element to be added. This method will create a copy of the Element structure.
	/// \return The newly allocated Element instance.
	Element* addElement(const Element& element);
	
	/// \brief Tries to add a new node to the mesh.
	/// \return The newly added new or NULL if no new node could be added at the given site for some reason.
	RepAtom* tryToAddNode(const LatticeSite& site);
	
	/// \brief Creates the mesh that connects the repatoms.
	void triangulate();
/**//// \brief Creates the mesh using Tetgen
	void triangulateTetgen();
	
/**//// \brief Creates the Octree mesh
	void octreeMesh();

/**////\brief updates the Mesh
	void UpdateMesh(int cluster);

	/// \brief Checks the topology of the mesh. Throws an exception if the mesh is not valid. 
	void checkTriangulation();
	
	/// \brief Builds the initial mesh and checks whether the mesh is compatible with any 
	///        periodic boundary conditions.
	void generateInitialMesh();
	
	void resetPositions();
	
	/// \brief This generates the sampling clusters around the repatoms.
	void buildSamplingClusters();

	/// \brief Calculates the weights for all sampling clusters.
	void calculateClusterWeights();
	
/**//// \brief Calculates the weights for all sampling clusters.
	void calculateShapeFunctionBasedClusterWeights();

	/// \brief This recalculates the deformed positions of all sampling atoms.	
	///        This method should be called after all repatoms have been moved
	///        to their new positions.
	void deformationUpdate();
	
	void transformGroup(const Group* group, const AffineTransformation& transformation)
	{
		for (size_t i = 0; i < repatoms().size(); ++i)
		{
			RepAtom* repatom = repatoms()[i];
			if (repatom->belongsToGroup(group))
				repatom->setDeformedPos(transformation * repatom->deformedPos());
		}
	};
	
	void transformRegion(const shared_ptr<Region>& region, const AffineTransformation& transformation)
	{
		for (size_t i = 0; i < repatoms().size(); ++i)
		{
			RepAtom* repatom = repatoms()[i];
			if (region->contains(repatom->deformedPos()))
				repatom->setDeformedPos(transformation * repatom->deformedPos());
		}
	};
	
	void deflectOnEvec(CalculationResult* output, int index, FloatType scale);
	
	/// \brief Exports the QC mesh to the given file for visualization with the program TetView.	
	void exportMeshToTetView(const QString& filename);

	/// \brief Exports the QC mesh to the given file for visualization with the Tecplot program.
	/// \param filename The path of the file to write.
	/// \param forces Optional force calculation output that should be included in the exported data.
	void exportMeshToTecplot(const QString& filename, CalculationResult* forces = NULL);
	
	/// \brief Exports the QC mesh to the given file for visualization with ParaView.
	/// \param filename The path of the file to write.
	/// \param forces Optional force calculation output that should be included in the exported data.
	void exportMeshToVTK(const QString& filename, CalculationResult* forces = NULL);
	void exportMeshOnlyToVTK(const QString& filename);
	void exportMeshTriasToVTK(const QString& filename);
	
	/// \brief Exports the sampling atoms to the given file for visualization with ParaView.
	/// \param filename The path of the file to write.
	/// \param forces Optional force calculation output that should be included in the exported data.
	void exportSamplingAtomsToVTK(const QString& filename, CalculationResult* forces);

/**//// Exports the QC mesh for visualization in Element centered Tecplot format
	void exportMeshToTecplotWithCellData(const QString& filename, CalculationResult* forces = NULL);

	/// \brief Exports the repatom/sampling atoms to the given LAMMPS dump file for visualization with the OVITO program.
	/// \param filename The path of the file to write.
	/// \param forces Optional force calculation output that should be included in the exported data.
	void exportMeshToDump(const QString& filename, CalculationResult* forces = NULL);

	void exportAtoms(const QString& filename, CalculationResult* forces = NULL);
	
	void exportDXAdump(const QString& filename, const Box3& box, CalculationResult* forces = NULL);
	
	void exportDislocation(const QString& filename, const Box3& box);

	/// \brief Exports the repatoms to the given LAMMPS data file.
	/// \param filename The path of the file to write.
	void exportMeshToData(const QString& filename);
	
	/// \brief Returns the list of all representative atoms.
	inline QVector<RepAtom*>& repatoms() { return _repatoms; }

/**//// \brief Returns the list of all mesh nodes.
	inline MeshNodeCollection& nodes() { return _nodes; }
	
/**//// \brief Returns the list of all elements in the mesh.
	inline ElementCollection& elements() { return _elements; }

/**//// \brief Returns the list of all octree elements in the mesh.
	inline QVector<ElementOctree*>& octrees(){ return _octrees;}

/**//// \brief Returns the list of all sampling atoms.
	inline SamplingAtomCollection& samplingAtoms() { return _samplingAtoms; }
	
	/// \brief Deletes all elements that have been marked for deletion.
	void deleteMarkedElements();
	
	/// \brief Splits those elements marked for refinement.
	/// \return True if the marked elements have been split,
	///         False if there have been no marked elements or if the
	///         marked elements cannot be refined any further. 
	bool splitMarkedElements();

/**//// returns the total number of active sampling atom value
	int activesampatoms() { return _activesampatoms;}

/**//// returns the maximum refinement indicator value
	FloatType maxRefinementIndicator() { return  _maxRefIndicator;}

/**//// tracks the Node displacement during the simulation
	Point3 trackNode(int nodeNum);

	/**//// finds the element for given site through octree structure
	Element* FindElement(const LatticeSite& site, const shared_ptr<Grain>& grain);


	///Octree development method

/**//// creates the vertex for the octree structure
		void createVertex(int grainIndex,Point3 p);

/**//// creates the triangular face for the octree structure
		void createTriFace(int grainIndex, int face1, int face2, int face3);

/**//// creates the quadratic face for the octree structure
		void createQuadFace(int grainIndex, int face1, int face2, int face3, int face4);
		
		/// Creates Faces from VTK file
		void importVTK(const QString& filename, int grainIndex, const AffineTransformation& transformation, bool faceInversion);

/**//// Locally refines the mesh in the given bounding box.
	/// @param box		Specifies the region where the mesh should be refined.
	/// @param toLevel	The octree level. Level 0 is totally refined.
		void RefineLocally(const Box3& box, int toLevel = 0);

/**//// Refines the mesh if necessary.
		bool AdaptiveRefinement(const Box3& box,FloatType tolerance);
		
/**//// Quadrature Rule(QR) method
		void buildQuadPoints();

		void setSiteToBeRemoved(const Point3& site);

		void selectMethod();


private:
	
	/// Stores pointers to all the representative atoms (repatoms).
	QVector<RepAtom*> _repatoms;

	/// Stores pointers to all the mesh nodes.
	/// This includes all repatoms from the array above plus their periodic images required for the triangulation.
	MeshNodeCollection _nodes;
	
	/// The memory pool for representative atoms / mesh nodes.
	object_pool<RepAtom> _repatomPool;

	/// Stores pointers to all the elements in the mesh.
	ElementCollection _elements;

/**//// stores the element octrees
	 QVector<ElementOctree*> _octrees;

	/// The memory pool for elements.
	object_pool<Element> _elementPool;

	/// The memory pool for sampling atoms.
	pool<> _samplingAtomPool;

	/// Stores pointers to all the sampling atoms.
	SamplingAtomCollection _samplingAtoms;
	
/**////stores max refinement indicator value
	FloatType _maxRefIndicator;

/**//// stores number of active sampling atoms
	int _activesampatoms;

	QVector<Point3*> _removeSite;
	object_pool<Point3> _removeSitePool;
	
	/**//// stores number of lattice sites in crystal
	int NumOfLatticeSites;

	friend class Simulation;

private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) {
    	ar & boost::serialization::base_object<SimulationResource>(*this);
    	ar & _octrees;
    	ar & _repatoms;
		ar & _nodes;
		ar & _elements;
		ar & _removeSite;
		Q_FOREACH(Element* element, elements()){
			element->serializeNeighbors(ar, version);
			element->serializeNextElementInCell(ar, version); }

    	if(typename Archive::is_loading() == true) {

		selectMethod();
    		// Rebuild sampling atoms.
    		//buildSamplingClusters();
    		//buildQuadPoints();
    		deformationUpdate();
    	}
    }

	friend class boost::serialization::access;
#if buildLocationCluster
	friend class boost::archive::detail::heap_allocation<RepAtom>;
	friend class boost::archive::detail::heap_allocation<Element>;
#else
	friend class boost::archive::detail::heap_allocator<RepAtom>;
	friend class boost::archive::detail::heap_allocator<Element>;
#endif
};

}; // End of namespace MAFEM

#endif // __MAFEM_MESH_H
