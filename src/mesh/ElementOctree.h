///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_ELEMENT_OCTREE_H
#define __MAFEM_ELEMENT_OCTREE_H

#include <simulation/SimulationResource.h>
#include <grain/CrystalLattice.h>
#include <utilities/Flags.h>
#include <boundaryconditions/DisplacementBoundaryCondition.h>

namespace MAFEM {

class Grain;			// defined in Grain.h
class RepAtom;			// defined in FENode.h
class Element;			// defined in Element.h
class ElementOctree;	// defined below
class Mesh;			// defined in Mesh.h
class GrainBoundary;	// defined in GrainBoundary.h
class LatticeSite;		// defined in LatticeSite.h
class CrystalLattice;

typedef Point_3<int> OctreeLocationCode;

/******************************************************************************
* A node in the octree.
******************************************************************************/
class ElementOctreeNode : public ObjectWithFlags,SimulationResource
{
public:

	/// The bit flags for this class.
	enum OctreeNodeFlags {
		IS_LEAF = (1<<0),				// Indicates that this is a leaf node with no sub nodes.
		MODIFIED_MESH = (1<<1),			// Indicates that this cell does not have the standard meshing anymore.
		ELEMENT_LINKS_VALID = (1<<2),	// Indicates that the element neighbor links of this cell are up to date.
		IS_INTERIOR_CELL = (1<<3),		// Indicates that the cell is completely inside the grain.
		IS_EXTERIOR_CELL = (1<<4),		// Indicates that the cell is completely outside the grain.
	};

public:

	/// The parent cell or NULL if this is the root node.
	ElementOctreeNode* parent;

	/// The owner of this node.
	ElementOctree* tree;

	/// The bounding box of the octree cell in lattice space.
	Box3I box;

	/// The tree level this node is in the hierarchy. The smallest cells have level 0.
	unsigned int level;

	/// The x,y and z location codes of this cell.
	OctreeLocationCode locationCode;

	/// The center point of the cubic cell.
	Point3I center;

	/// The eight child nodes of this node.
	ElementOctreeNode* subnodes[8];

	/// Pointer to the first element in the linked list of elements of this (leaf) cell.
	Element* elements;

	/// Pointer to the first node in the linked list of nodes that belong to this (leaf) cell.
	RepAtom* meshnodes;

	/// The points that make up the Bravais cell.
	RepAtom* bravaisPoints[15];

public:

	/// Returns true if this is a leaf node with no sub nodes.
	bool IsLeaf() const { return GetFlag(IS_LEAF); }

	/// Returns true if this the root node.
	bool IsRoot() const { return parent == NULL; }

	/// Subdivides this node if it is a leaf node.
	void Subdivide();

	/// Subdivides this cell if it intersects the boundary.
	void RefineBoundary(GrainBoundary* boundary, unsigned int toLevel);

	/// Creates the elements in the mesh for this cell.
	void CreateElements(unsigned int meshLevel);

	/// Creates the repatoms that make up the Bravais cell.
	void CreateBravaisPoints();

	/// Deletes all elements contained in this cell.
	void PurgeElements();

	/// Returns a neighbor of this cell that is on the same tree level as this one.
	ElementOctreeNode* GetNeighbor(const Vector3I& direction, bool createOnDemand);

	/// Updates the neighbor fields for all elements below this node.
	void LinkElements();

	/// Subdivides those cells that contain at least one element that
	/// is flagged for adaption.
	/// Returns true if at least one cell has been subdivided.
	bool RefineFlaggedElements();

	/// Returns the Bravais cell type.
	
	LatticeType GetLatticeType();


private:

	/// Create the i-th subnode.
	void CreateSubNode(unsigned int index, const Box3I& bb);

	/// Create a single tetrahedron element inside this octree cell.
	Element* CreateElement(RepAtom* a1, RepAtom* a2, RepAtom* a3, RepAtom* a4, int fd1, int fd2, int fd3, int fd4);

	/// Splits any tetetrahedra in this cell whos edges are bisected by the given point.
	void InsertSplitPoint(RepAtom* point);

	/// Searches this cell for an element that has the three given vertices.
	Element* FindElementFace(Element* faceElement, RepAtom* faceVertices[3], int& side);

	/// Transforms the given point from reference to deformed configuration using linear interpolation.
	Point3 ComputeDeformedPosition(const LatticeSite& site) const;

	template<class Archive>
		    void serialize(Archive &ar, const unsigned int version) {
				ar & parent;
				ar & tree;
				ar & level;
				ar & locationCode;
				ar & box;
				ar & center;
				ar & subnodes;
				ar & elements;
				ar & meshnodes;
				ar & bravaisPoints;
				ar & boost::serialization::base_object<ObjectWithFlags>(*this);

			}

		friend class boost::serialization::access;

};

/******************************************************************************
* An octree structure that organizes the mesh elements in cubic cells.
* There is one element octree per grain.
******************************************************************************/
class ElementOctree : public SimulationResource
{
public:
    
	/// Constructor.
	ElementOctree();

/**//// sets the simulation
	void setSimulation(Simulation* simulation){ sim = simulation; }

	void setGrainIndex(int index) {grainIndex = index;}

	/// Creates the initial octree.
	void CreateInitialMesh();

	/// Returns the root node of the octree.
	ElementOctreeNode* GetRoot() const { return root; }

	/// Returns the simulation
	Simulation* GetSimulation() const { return sim;}

	/// Returns the grain Index
	int GrainIndex() const {return grainIndex; }

	/// Returns a node in the tree.
	ElementOctreeNode* GetNodeAtLevel(OctreeLocationCode locationCode, unsigned int level, bool createOnDemand = false);

	/// Returns the leaf node that is closest to the given location code.
	ElementOctreeNode* GetLeafNode(OctreeLocationCode locationCode, unsigned int stopAtLevel);

	/// Returns the smallest node in the hierarchy that contains the given location code.
	ElementOctreeNode* GetSmallestNode(const OctreeLocationCode& locationCode);

	/// Subdivides those cells that contain at least one element that
	/// is flagged for adaption.
	/// Returns true if at least one cell has been subdivided.
	bool RefineFlaggedElements();

	/// This is called after cells have been added or removed.
	/// Fills all octree cells with elements and links them together.
	void OnTreeChanged();

	/// Locally subdivides the octree until the given boundary is matched.
	void RefineBoundary(GrainBoundary* boundary, unsigned int toLevel);

	/// Returns the element that constains the given lattice site.
	Element* FindElement(const LatticeSite& site);
	
	QVector<ElementOctreeNode*>& octreeNodes() { return nodePool; }

protected:

	////////////////////////////////// Internal methods ///////////////////////////////

	/// Allocates a new octree node.
	ElementOctreeNode* AllocNewNode();

	/// Inserts a new FENode into the mesh.
	RepAtom* InsertMeshNode(const Point3& pos, OctreeLocationCode locationCode);

	/// Inserts a new FENode into the mesh.
	RepAtom* InsertMeshNode(const Point3& pos);

	/// Applies periodic boundary conditions.
	bool MakeCodeInside(OctreeLocationCode& code) const;

protected:

	/// The memory pool for octree nodes.
	//pool<> nodePool;
	QVector<ElementOctreeNode*> nodePool;

	/// The root node of the octree.
	ElementOctreeNode* root;

	/// grain Index
	int grainIndex;

	/// The Simulation
	Simulation* sim;

	template<class Archive>
	    void serialize(Archive &ar, const unsigned int version) {
			//ar & boost::serialization::base_object<SimulationResource>(*this);
			ar & root;
			ar & grainIndex;
			ar & nodePool;
			ar & sim;
		}

	friend class boost::serialization::access;
	friend class ElementOctreeNode;
};

};

#endif // __MAFEM_ELEMENT_OCTREE_H
