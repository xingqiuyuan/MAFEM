///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_GRAIN_H
#define __MAFEM_GRAIN_H

#include <MAFEM.h>
#include <simulation/SimulationResource.h>
#include "CrystalLattice.h"
#include "GrainBoundary.h"

namespace MAFEM {

/**
 * \brief A crystal grain.
 * 
 * \author Alexander Stukowski
 */
class Grain : public SimulationResource
{
public:
	
	/// \brief Constructs a grain with the given properties.
	/// \param latticeType Specifies the type of lattice to assign to this grain.
	/// \param atomType The atom type to assign the atoms of this grain.
	/// \param bravaisSystem Specifies the transformation of the lattice vectors.
	Grain(LatticeType latticeType, int atomType, const AffineTransformation& bravaisSystem);
	
	/// \brief Returns a reference to the lattice of this grain.
	CrystalLattice& lattice() { return _lattice; }

	/// \brief Specifies the material the grain is made of.
	/// \returns The atom type of the atoms this grain is made of.
	int atomType() const { return _atomType; }
	
	/// \brief Sets the material the grain is made of.
	/// \param atomType The atom type of the atoms this grain is made of.
	void setAtomType(int atomType) { _atomType = atomType; }

/**//// Returns a reference to the shape of the grain.
	GrainBoundary& Boundary() { return boundary; }


private:

	/// The lattice of this grain.
	CrystalLattice _lattice;

	/// Specifies the atom type of the grain's atoms.
	int _atomType;

/**//// The boundary shape of the grain.
	GrainBoundary boundary;

private:  // serialization code

	/// Deserialization constructor.
	Grain() {}

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) {
    	ar & boost::serialization::base_object<SimulationResource>(*this);
    	ar & _lattice;
    	ar & _atomType;
    	ar & boundary;
    }

	friend class boost::serialization::access;
};
	
}; // End of namespace MAFEM

#endif // __MAFEM_GRAIN_H
