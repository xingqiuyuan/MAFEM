///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// 
// Copyright (C) 2005, Alexander Stukowski
//
// E-mail:  alex@stukowski.de
// Web:     www.stukowski.com
//
///////////////////////////////////////////////////////////////////////////////

#include "MAFEM.h"
#include "Base.h"
#include "GrainBoundary.h"
#include <simulation/Simulation.h>
#include <grain/Grain.h>

namespace MAFEM {

/******************************************************************************
* Computes the normal vector of the face.
******************************************************************************/
void BoundaryFace::ComputeFaceNormal()
{
	MAFEM_ASSERT(verts.size() >= 3);

	BoundaryVertex* v0 = verts[0];
	BoundaryVertex* v1 = verts[1];
	Vector3 d1_world = v1->GetWorldPos() - v0->GetWorldPos();
	Vector3 d1_bravais = v1->GetBravaisPos() - v0->GetBravaisPos();

	worldNormal = NULL_VECTOR;
	for(int i = 2; i < verts.size(); i++) {
		BoundaryVertex* v2 = verts[i];
		Vector3 wn = CrossProduct(d1_world, v2->GetWorldPos() - v0->GetWorldPos());
		if(LengthSquared(wn) > FLOATTYPE_EPSILON) {
			worldNormal = Normalize(wn);
			bravaisNormal = Normalize(CrossProduct(d1_bravais, v2->GetBravaisPos() - v0->GetBravaisPos()));
			break;
		}
	}
	MAFEM_ASSERT(worldNormal != NULL_VECTOR);
}
/******************************************************************************
* Creates a new vertex and sets it to the specified point.
******************************************************************************/
BoundaryVertex* GrainBoundary::CreateVertex(const shared_ptr<Grain>& grain, const Point3& p)
{
	CHECK_POINTER(grain);

	// Transform position to lattice space.
	Point3 bravaisPos =  grain->lattice().inverseBravaisVectors() * p;
	Point3 latticePos =  grain->lattice().inverseLatticeVectors() * p;



	// Allocate the new vertex.
	BoundaryVertex* vertex = new BoundaryVertex(p, bravaisPos, latticePos);
	vertices.push_back(vertex);

	// Include vertex in bounding boxes.
	worldBoundingBox.addPoint(vertex->GetWorldPos());
	bravaisBoundingBox.addPoint(Point3(floor(vertex->GetBravaisPos().X+FLOATTYPE_EPSILON), floor(vertex->GetBravaisPos().Y+FLOATTYPE_EPSILON), floor(vertex->GetBravaisPos().Z+FLOATTYPE_EPSILON)));
	bravaisBoundingBox.addPoint(Point3(ceil(vertex->GetBravaisPos().X-FLOATTYPE_EPSILON), ceil(vertex->GetBravaisPos().Y-FLOATTYPE_EPSILON), ceil(vertex->GetBravaisPos().Z-FLOATTYPE_EPSILON)));
	latticeBoundingBox.addPoint(Point3(floor(vertex->GetLatticePos().X+FLOATTYPE_EPSILON), floor(vertex->GetLatticePos().Y+FLOATTYPE_EPSILON), floor(vertex->GetLatticePos().Z+FLOATTYPE_EPSILON)));
	latticeBoundingBox.addPoint(Point3(ceil(vertex->GetLatticePos().X-FLOATTYPE_EPSILON), ceil(vertex->GetLatticePos().Y-FLOATTYPE_EPSILON), ceil(vertex->GetLatticePos().Z-FLOATTYPE_EPSILON)));
	
	
    return vertex;
}

/******************************************************************************
* Create a new triangle face with three vertices given by index.
******************************************************************************/
BoundaryFace* GrainBoundary::CreateTriFace(int a, int b, int c)
{
	QVector<BoundaryVertex*> verts;
	verts.push_back(Vertices()[a]);
	verts.push_back(Vertices()[b]);
	verts.push_back(Vertices()[c]);
	QVector<BoundaryEdge*> fedges;
	fedges.push_back(CreateEdge(a,b));
	fedges.push_back(CreateEdge(b,c));
	fedges.push_back(CreateEdge(c,a));
	BoundaryFace* face = new BoundaryFace(verts, fedges);
	faces.push_back(face);

	return face;
}

/******************************************************************************
* Create a new quad face with four vertices given by index.
******************************************************************************/
BoundaryFace* GrainBoundary::CreateQuadFace(int a, int b, int c, int d)
{
	QVector<BoundaryVertex*> verts;

	verts.push_back(Vertices()[a]);
	verts.push_back(Vertices()[b]);
	verts.push_back(Vertices()[c]);
	verts.push_back(Vertices()[d]);
	QVector<BoundaryEdge*> fedges;
	fedges.push_back(CreateEdge(a,b));
	fedges.push_back(CreateEdge(b,c));
	fedges.push_back(CreateEdge(c,d));
	fedges.push_back(CreateEdge(d,a));

	BoundaryFace* face = new BoundaryFace(verts, fedges);

	faces.push_back(face);

	return face;
}

/******************************************************************************
* Creates an edge that connects the two vertices with the given indices.
* Returns an existing edge if there is one.
******************************************************************************/
BoundaryEdge* GrainBoundary::CreateEdge(int a, int b)
{
	BoundaryVertex* v1 = Vertices()[a];
	BoundaryVertex* v2 = Vertices()[b];

    for(int i= 0; i < edges.size(); i++){
        BoundaryEdge* e = edges.at(i);
		if((e->Vertex1() == v1 && e->Vertex2() == v2) ||
			(e->Vertex2() == v1 && e->Vertex1() == v2))
			return e;
	}

	BoundaryEdge* edge = new BoundaryEdge(v1, v2);
	edges.push_back(edge);
	return edge;
}

/******************************************************************************
* Tests whether a point lies inside the mesh. Returns true if the given point
* is inside the mesh or on the boundary of the mesh.
******************************************************************************/
bool GrainBoundary::LocatePoint(const Point3& point, BoundaryFace** onFace, FloatType epsilon) const
{
	
       // Do a coarse test.
	Box3 bb = GetWorldBoundingBox();
	bb.minc -= Vector3(epsilon);
	bb.maxc += Vector3(epsilon);
	if(!bb.contains(point))
		return false;

	FloatType area = 0.0;


    for(int i= 0; i < faces.size(); i++){

        BoundaryFace* face = faces.at(i);

		const Point3* p2 = &face->Vertices()[face->Vertices().size()-1]->GetWorldPos();
		const Point3* p1 = &face->Vertices()[0]->GetWorldPos();

		Plane3 plane(*p1, face->GetWorldNormal());
		if(plane.classifyPoint(point, epsilon) == 0) {
			// Point is on the face plane. -> Do point in polygon test.
			if(face->PointOnFace(point, epsilon)) {
				if(onFace) *onFace = face;
				return true;
			}
			else continue;
		}

		Vector3 r1;
		FloatType solidAngle = 0.0;
		Vector3 a = *p2 - *p1;

		for(size_t i = 1; i <= face->Vertices().size(); i++) {
			r1 = *p1 - point;
			p2 = &face->Vertices()[i % face->Vertices().size()]->GetWorldPos();
			Vector3 b = *p2 - *p1;
			Vector3 n1 = CrossProduct(a, r1);
			FloatType l1 = Length(n1);
			MAFEM_ASSERT(l1 > 0.0);
			Vector3 n2 = CrossProduct(r1, b);
			FloatType l2 = Length(n2);
			MAFEM_ASSERT(l2 > 0.0);
			FloatType s  = DotProduct(n1, n2) / (l1 * l2);
			FloatType ang = acos(std::max(-1.0, std::min(1.0, s)));
			s = DotProduct(CrossProduct(b, a), plane.normal);
			solidAngle += s > 0.0 ? M_PI - ang : M_PI + ang;
			a = -b;
			p1 = p2;
		}
		solidAngle -= M_PI*(face->Vertices().size()-2);

		area += (DotProduct(plane.normal, r1) > 0.0) ? -solidAngle : solidAngle;
	}

	if(onFace)
		*onFace = NULL;

	return (area > 2*M_PI) || (area < -2*M_PI);
}
/******************************************************************************
* Returns true if the given point is exactly on the face.
******************************************************************************/
bool BoundaryFace::PointOnFace(const Point3& point, FloatType epsilon) const
{
	// Find the best projection plane.
	int zi = MaxAbsComponent(GetWorldNormal());
	int xi = (zi + 1); if(xi == 3) xi = 0;
	int yi = (xi + 1); if(yi == 3) yi = 0;

	int angle = 0;
	const Point3* vertex = &verts[0]->GetWorldPos();
	int quad = (((*vertex)[xi] > point[xi]) ? (((*vertex)[yi] > point[yi]) ? 0 : 3) : (((*vertex)[yi] > point[yi]) ? 1 : 2));
	for(int i = 1; i <= verts.size(); i++) {
		const Point3* next_vertex = &verts[i % verts.size()]->GetWorldPos();

		// Test whether the point is on the polygon edge.
		Vector3 edge = *next_vertex - *vertex;
		FloatType c1 = DotProduct(point - *vertex, edge);
		if(c1 <= 0.0) {
			if(DistanceSquared(point, *vertex) <= epsilon) return true;
		}
		else {
			FloatType c2 = DotProduct(edge, edge);
			if(c2 <= c1) {
				if(DistanceSquared(point, *next_vertex) <= epsilon) return true;
			}
			else if(DistanceSquared(point, *vertex + (c1 / c2) * edge) <= epsilon) return true;
		}

		/// Calculate quadrant and delta from last quadrant.
		int next_quad = (((*next_vertex)[xi] > point[xi]) ? (((*next_vertex)[yi] > point[yi]) ? 0 : 3) : (((*next_vertex)[yi] > point[yi]) ? 1 : 2));
		int delta = next_quad - quad;

		switch(delta) {
			// Make quadrant deltas wrap around.
			case  3: delta = -1; break;
			case -3: delta =  1; break;
			// Check if went around point cw or ccw.
			case  2:
			case -2:
				// Determine x intercept of a polygon edge with a horizontal line at the y value of the test point.
				if(((*next_vertex)[xi] - ( ((*next_vertex)[yi] - point[yi]) * (((*vertex)[xi] - (*next_vertex)[xi]) / ((*vertex)[yi] - (*next_vertex)[yi])))) > point[xi])
					delta = -delta;
				break;
		}
		angle += delta;
		// Increment for next step.
		quad = next_quad;
		vertex = next_vertex;
	}

	// Complete 360 degrees (angle of +4 or -4) means inside.
	return ((angle == +4) || (angle == -4));
}

/******************************************************************************
* Returns true if the given axis-aligned box intersects with any of the polygons of this mesh.
******************************************************************************/
bool GrainBoundary::IntersectsBravaisBox(const Box3I& lbox) const
{
	using namespace std;

	// Do a quick bounding box test first.
	if(!lbox.intersects(GetBravaisBoundingBox()))
		return false;

	Box3 box(lbox.minc, lbox.maxc);
	const Vector3 boxcenter =  (box.center() - ORIGIN);
	Vector3 boxhalfsize = 0.5 * box.size();

	// Test each face individually.

    for(int i = 0; i < faces.size(); i++){
        BoundaryFace* face = faces.at(i);
		Point3 vert = face->Vertices()[0]->GetBravaisPos() - boxcenter;

		// Test each triangle in the triangulation of the face.
        for(int j = 0; j< face->triangulation.size(); j++){
        Point3* triiter = face->triangulation.at(j);
		//QVector<Point3*>::Iterator triiter = face->triangulation.begin();
		//while(triiter != face->triangulation.end()) {
			// AABB-triangle overlap test code
			// by Tomas Akenine-M�ler
			//
			// Use separating axis theorem to test overlap between triangle and box
			// need to test for overlap in these directions:
			//    1) the {x,y,z}-directions (actually, since we use the AABB of the triangle
			//       we do not even need to test these)
			//    2) normal of the triangle
			//    3) crossproduct(edge from tri, {x,y,z}-directin)
			//       this gives 3x3=9 more tests

			// Get the three vertices of the triangle
			// and move everything so that the boxcenter is in (0,0,0).
			Point3 v0 = (*(triiter++)) - (boxcenter);
			Point3 v1 = (*(triiter++)) - (boxcenter);
			Point3 v2 = (*(triiter++)) - (boxcenter);

			// Compute triangle edges.
			Vector3 e0 = v1 - v0;
			Vector3 e1 = v2 - v1;
			Vector3 e2 = v0 - v2;

			/*======================== X-tests ========================*/
            #define AXISTEST_X01(a, b, fa, fb)			   \
        	p0 = a*v0.Y - b*v0.Z;			       	   \
        	p2 = a*v2.Y - b*v2.Z;			       	   \
            if(p0<p2) {min=p0; max=p2;} else {min=p2; max=p0;} \
        	rad = fa * boxhalfsize.Y + fb * boxhalfsize.Z;   \
        	if(min>rad || max<-rad) continue;

            #define AXISTEST_X2(a, b, fa, fb)			   \
            	p0 = a*v0.Y - b*v0.Z;			           \
            	p1 = a*v1.Y - b*v1.Z;			       	   \
            	if(p0<p1) {min=p0; max=p1;} else {min=p1; max=p0;} \
            	rad = fa * boxhalfsize.Y + fb * boxhalfsize.Z;   \
            	if(min>rad || max<-rad) continue;

            /*======================== Y-tests ========================*/
            #define AXISTEST_Y02(a, b, fa, fb)			   \
            	p0 = -a*v0.X + b*v0.Z;		      	   \
            	p2 = -a*v2.X + b*v2.Z;	       	       \
                if(p0<p2) {min=p0; max=p2;} else {min=p2; max=p0;} \
            	rad = fa * boxhalfsize.X + fb * boxhalfsize.Z;   \
            	if(min>rad || max<-rad) continue;

            #define AXISTEST_Y1(a, b, fa, fb)			   \
            	p0 = -a*v0.X + b*v0.Z;		      	   \
            	p1 = -a*v1.X + b*v1.Z;	     	       \
            	if(p0<p1) {min=p0; max=p1;} else {min=p1; max=p0;} \
            	rad = fa * boxhalfsize.X + fb * boxhalfsize.Z;   \
            	if(min>rad || max<-rad) continue;

            			/*======================== Z-tests ========================*/
            #define AXISTEST_Z12(a, b, fa, fb)			   \
            	p1 = a*v1.X - b*v1.Y;			           \
            	p2 = a*v2.X - b*v2.Y;			       	   \
                if(p2<p1) {min=p2; max=p1;} else {min=p1; max=p2;} \
            	rad = fa * boxhalfsize.X + fb * boxhalfsize.Y;   \
            	if(min>rad || max<-rad) continue;

            #define AXISTEST_Z0(a, b, fa, fb)			   \
            	p0 = a*v0.X - b*v0.Y;					   \
            	p1 = a*v1.X - b*v1.Y;			           \
                if(p0<p1) {min=p0; max=p1;} else {min=p1; max=p0;} \
            	rad = fa * boxhalfsize.X + fb * boxhalfsize.Y;   \
            	if(min>rad || max<-rad) continue;

			/// Test the 9 tests first.
			FloatType min,max,p0,p1,p2,rad,fex,fey,fez;
			fex = abs(e0.X); fey = abs(e0.Y); fez = abs(e0.Z);
			AXISTEST_X01(e0.Z, e0.Y, fez, fey);
			AXISTEST_Y02(e0.Z, e0.X, fez, fex);
			AXISTEST_Z12(e0.Y, e0.X, fey, fex);
			fex = abs(e1.X); fey = abs(e1.Y); fez = abs(e1.Z);
			AXISTEST_X01(e1.Z, e1.Y, fez, fey);
			AXISTEST_Y02(e1.Z, e1.X, fez, fex);
			AXISTEST_Z0(e1.Y, e1.X, fey, fex);
			fex = abs(e2.X); fey = abs(e2.Y); fez = abs(e2.Z);
			AXISTEST_X2(e2.Z, e2.Y, fez, fey);
			AXISTEST_Y1(e2.Z, e2.X, fez, fex);
			AXISTEST_Z12(e2.Y, e2.X, fey, fex);

			// First test overlap in the {x,y,z}-directions.
			// find min, max of the triangle each direction, and test for overlap in
			// that direction -- this is equivalent to testing a minimal AABB around
			// the triangle against the AABB.

            #define FINDMINMAX(x0,x1,x2,min,max) \
            	min = max = x0;   \
            	if(x1<min) min=x1;\
            	if(x1>max) max=x1;\
            	if(x2<min) min=x2;\
            	if(x2>max) max=x2;

			// Test in X-direction.
			FINDMINMAX(v0.X,v1.X,v2.X,min,max);
			if(min>=boxhalfsize.X || max<=-boxhalfsize.X) continue;
			// Test in Y-direction.
			FINDMINMAX(v0.Y,v1.Y,v2.Y,min,max);
			if(min>=boxhalfsize.Y || max<=-boxhalfsize.Y) continue;
			// Test in Z-direction.
			FINDMINMAX(v0.Z,v1.Z,v2.Z,min,max);
			if(min>=boxhalfsize.Z || max<=-boxhalfsize.Z) continue;

			break;
		}

		// Compute the plane equation for this face.
		Plane3 plane(vert, face->GetBravaisNormal());

		// Test if the box intersects the plane of the face.
		Vector3 vmin, vmax;
		for(int q=0; q<3; q++) {
			const FloatType v = vert[q];
			if(plane.normal[q] > 0.0) {
				vmin[q] =-boxhalfsize[q] - v;
				vmax[q] = boxhalfsize[q] - v;
			}
			else {
				vmin[q] = boxhalfsize[q] - v;
				vmax[q] =-boxhalfsize[q] - v;
			}
		}
		if(DotProduct(plane.normal, vmin) >= 0.0) continue;
		if(DotProduct(plane.normal, vmax) > FLOATTYPE_EPSILON) return true;
	}

	return false;
}

};

