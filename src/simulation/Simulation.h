///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_SIMULATION_H
#define __MAFEM_SIMULATION_H


#include <MAFEM.h>
#include <grain/Grain.h>
#include <grain/GrainBoundary.h>
#include <mesh/Mesh.h>
#include <material/EAMPotential.h>
#include <atoms/SamplingAtomCollection.h>
#include <boundaryconditions/Group.h>
#include <boundaryconditions/ForceBoundaryCondition.h>
#include <boundaryconditions/DisplacementBoundaryCondition.h>

namespace MAFEM {

/// A list of smart pointers to Grain instances.
typedef QVector< shared_ptr<Grain> > GrainList;

/**
 * \brief Stores the global settings for a simulation.
 * 
 * These flags and values control the behaviour of the simulation.
 * They can be retrieved via the Simulation::settings() method.
 * 
 * \author Alexander Stukowski
 */
class SimulationSettings
{
public:

	/// Enables periodic boundary conditions in X, Y and Z direction (default = false).
	array<bool, NDOF> periodicBC;
	
	/// The radius of the sampling cluster in world units (default = 0).
	FloatType clusterRadius;

	/// The padding to be added to the potential's cutoff radius when
	/// building the sampling clusters. This is used to include more atoms then needed
	/// in the clusters to account for the deformation (default = 0).
	FloatType clusterSkinThickness;

	/// The padding to be added to the potential's cutoff radius when
	/// building the neighbor lists (default = 0). 
	FloatType neighborSkinThickness;
	
	/// Controls whether elements in fully atomistic regions are preserved (default = false).
	bool keepFullyAtomisticElements;
	
	/// This activates the lattice statics without QC (default = false).
	/// If this option is enabled then the mesh is not generated nor are
	/// cluster weight being computed.
	bool latticeStaticsMode;
	
	/// Enables the force-based QC scheme (default = false).
	/// Settings this to true will enable the generation of neighbor lists
	/// also for the passive sampling atoms and increases the passive sampling cluster radius
	/// by an additional cutoff distance.
	bool QCForceMode;

	/**//// The number of octree levels that should be created in the initial
		/// meshing stage.
		unsigned int InitialRefinementLevel;

	/**//// Enables the use of the simple cubic element template for large octree cells.
		bool OptimizeOctreeCells;

	/**//// Enables the qhull mesh
		bool qhullMesh;

	/**//// Enables the qhull mesh
		bool tetgenMesh;

	/**//// Enables the Octree mesh
		bool octreeMesh;

	/**//// Enables the Quadrature method
		bool quadratureMethod;

	/**//// Enables the Quadrature method
		bool octreeElementSearch;

	/**//// Enables the Quadrature method
		bool segmentTreeElementSearch;

	/**//// Enables the Quadrature method
		bool qrVolBasedWeightFactor;

	/**//// sets the maximum number of thread for openMP
		int openMPThread;

	/**//// stores the indentor location
		FloatType indentLocationX;
		FloatType indentLocationY;
		FloatType indentLocationZ;



	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar & periodicBC;
        ar & clusterRadius;
        ar & clusterSkinThickness;
        ar & neighborSkinThickness;
        ar & keepFullyAtomisticElements;
        ar & latticeStaticsMode;
        ar & QCForceMode;
        ar & InitialRefinementLevel;
    	ar & OptimizeOctreeCells;
		ar & qhullMesh;
		ar & tetgenMesh;
		ar & octreeMesh;
		ar & quadratureMethod;
		ar & octreeElementSearch;
		ar & segmentTreeElementSearch;
		ar & qrVolBasedWeightFactor;
		ar & openMPThread;
		ar & indentLocationX;
	ar & indentLocationY;
	ar & indentLocationZ;

    }	
};

/**
 * \brief A container that holds all data for the current simulation.
 * 
 * \author Alexander Stukowski
 */
class Simulation
{
public:

	/// \brief Default constructor.
	/// 
	/// This creates an empty simulation with default settings.
	Simulation();
	
	/// \brief The destructor.
	~Simulation();

	////////////////////////////// Simulation settings ///////////////////////////////

	/// \brief Gets the flags and settings that control the behavior of the simulation.
	/// \return A reference to the object that stores the simulation settings.
	inline SimulationSettings& settings() { return _settings; }
	
	/// \brief Returns the geometry of the initial simulation cell as a matrix.
	inline const AffineTransformation& simulationCell() const { return _simCell; }

	/// \brief Returns the inverse of the initial simulation cell matrix.
	inline const AffineTransformation& inverseSimulationCell() const { return _inverseSimCell; }
	
	/// \brief Sets the initial simulation cell geometry. 
	void setSimulationCell(const AffineTransformation& cell);

	/// \brief Returns the geometry of the current simulation cell as a matrix.
	inline const AffineTransformation& deformedSimulationCell() const { return _deformedSimCell; }

	/// \brief Returns the inverse of the current simulation cell matrix.
	inline const AffineTransformation& inverseDeformedSimulationCell() const { return _inverseDeformedSimCell; }
	
	/// \brief Sets the current simulation cell geometry.
	///
	/// All repatom coordinates are rescaled to the new cell shape based on their current reduced coordinates.
	void setDeformedSimulationCell(const AffineTransformation& cell);

	//////////////////////////////////// Grains /////////////////////////////////////

	/// \brief Returns the number of grains in the simulation.
	inline int grainCount() const { return _grains.size(); }

	/// \brief Returns a reference to the collection of grains in the simulation.
	inline const GrainList& grains() const { return _grains; }

	/// \brief Adds a grain to the simulation.
	void addGrain(const shared_ptr<Grain>& grain);
	
	///////////////////////////////////// Mesh ////////////////////////////////////////
	
	/// \brief Returns the QC mesh.
	inline Mesh& mesh() { return _mesh; }

	///////////////////////////// Interaction potential ////////////////////////////////

	/// \brief Sets the potential used for interaction computations.
	void setPotential(const shared_ptr<EAMPotential>& potential);
	
	/// \brief Returns the potential used for interaction computations.
	inline const shared_ptr<EAMPotential>& potential() const { return _potential; }

	//////////////////////////////////// Groups ////////////////////////////////////////
	
	/// \brief Returns the special predefined group that encompasses all atoms in the simulation.
	Group* allGroup() const { CHECK_POINTER(_groups[0]); return _groups[0]; }
	
	/// \brief Adds a new group of atoms to the simulation. 
	///        All atoms that are located in the given geometric region in the reference configuration are included to the group.
	/// \param region The geometric that specifies which atoms are added to the group.
	/// \return The newly created group.
	Group* createGroup(const shared_ptr<Region>& region);
	
	Group* breakGroup(const Group* group);
	
	/// \brief Returns the group with the given index.
	/// \param The index of the group to return. This must be between 0 and MAX_GROUP_COUNT-1.
	/// \return The group or \c NULL if there is no group with the given index. 
	Group* group(int index) const { MAFEM_ASSERT(index >= 0 && index < MAX_GROUP_COUNT); return _groups[index]; }

	//////////////////////////////////// Boundary conditions ////////////////////////////////////////

	/// \brief Returns the list of force boundary conditions defined for this simulation.
	const ForceBoundaryConditionList& forceBC() const { return _forceBC; }

	/// \brief Returns the list of displacement boundary conditions defined for this simulation.
	const DisplacementBoundaryConditionList& displacementBC() const { return _displacementBC; }

	///////////////////////////////////////// Other //////////////////////////////////////////////

	/// Returns the Simulation object that is currently being deserialized. 
	static Simulation* serializationInstance() { return _serializationInstance; } 
	
	
	void registerDisplacementBoundaryCondition(const shared_ptr<DisplacementBoundaryCondition>& bc){ _displacementBC.push_back(bc); }
	
	void registerForceBoundaryCondition(const shared_ptr<ForceBoundaryCondition>& bc){ _forceBC.push_back(bc); }

private:

	/// The flags and settings that control the behavior of the simulation.
	SimulationSettings _settings;
	
	/// The list of grains in the simulation.
	GrainList _grains;	

	/// The QC mesh.
	Mesh _mesh;
	
	/// The interaction potential
	shared_ptr<EAMPotential> _potential;
	
	/// The geometry of the initial simulation cell.
	AffineTransformation _simCell;
	/// The inverse of the initial simulation cell matrix.
	AffineTransformation _inverseSimCell;

	/// The geometry of the deformed simulation cell.
	AffineTransformation _deformedSimCell;
	/// The inverse of the deformed simulation cell matrix.
	AffineTransformation _inverseDeformedSimCell;
	
	/// The groups defined in the simulation.
	Group* _groups[MAX_GROUP_COUNT];
	
	/// The list of force boundary conditions defined for this simulation.
	ForceBoundaryConditionList _forceBC;

	/// The list of displacement boundary conditions defined for this simulation.
	DisplacementBoundaryConditionList _displacementBC;
	
	/// Pointer to the Simulation object that is currently being deserialized. 
	static Simulation* _serializationInstance;
	
	friend class ForceBoundaryCondition;
	friend class DisplacementBoundaryCondition;
	friend class boost::serialization::access;
	
private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) {
    	Simulation::_serializationInstance = this;
        ar & _settings;
		ar & _potential;		
		ar & _simCell;
		ar & _inverseSimCell;
		ar & _deformedSimCell;
		ar & _inverseDeformedSimCell;
        ar & _grains;
        ar & _mesh;
    }	
};

//////////////////////////////////// Restart files ////////////////////////////////////////
	
/// \brief Save the complete state of the simulation to a file.
void saveSimulation(const Simulation& sim, const QString& filename);
	
/// \brief Loads the state of a simulation from a file.
shared_ptr<Simulation> loadSimulation(const QString& filename);

}; // End of namespace MAFEM


#endif // __MAFEM_SIMULATION_H
