///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_GSL_MINIMIZER_H
#define __MAFEM_GSL_MINIMIZER_H

#include <MAFEM.h>
#include "Minimizer.h"

namespace MAFEM {

/**
 * \brief Wraps the minimizer functions from the GSL library.
 * 
 * \author Alexander Stukowski
 */
class GSLMinimizer : public Minimizer
{
public:
	
	/// \brief Constructor that initializes the minimizer.
	/// \param calculator A calculator that should be used by the minimizer to 
	///                   evaluate the energy function and its gradient.
	/// \param convergenceNorm The norm of the gradient vector that should be reached.
	/// \param type Specifies the minimization algorithm.
	/// \param initialStepSize The size of the first trial step.
	/// \param tolerance The accuracy of the line minimization.
	GSLMinimizer(const shared_ptr<Calculator>& calculator, FloatType convergenceNorm, 
		const gsl_multimin_fdfminimizer_type* type, FloatType initialStepSize, FloatType tolerance);

	/// \brief Destructor.
	virtual ~GSLMinimizer();

	/// Performs one iteration step.
	virtual void iterate();

	/// Tests whether the desired accuracy has been reached.
	virtual bool isConverged();
	
	/// Returns the current norm of the force vector.
	virtual FloatType forceNorm();
	
	/// Returns the value of the energy function and the current point.
	FloatType minimumValue();

	/// Returns the number of gradient evaluations done so far.
	virtual int numberOfForceEvaluations() { return _numForceEvaluations; }

	/// Returns the number of energy evaluations done so far.
	virtual int numberOfEnergyEvaluations() { return _numEnergyEvaluations; }

	/// Allows direct access to the minimizer's internal energy/force calculation results.
	const CalculationResult& calculationOutput() const { return _output; }

private:

	/// The internal GSL minimizer.
	gsl_multimin_fdfminimizer* _gsl_minimizer;

	/// Sepcifies the function to be minimized.	
	gsl_multimin_function_fdf _fdf;
	
	/// This stores the current repatom positions.
	gsl_vector* _x;
	
	/// The output container for the energy/force calculation.
	CalculationResult _output;
	
	/// The norm of the gradient vector that should be reached.
	FloatType _convergenceNorm;
	
	/// Counts the number of gradient evaluations done so far.
	int _numForceEvaluations;

	/// Counts the number of energy evaluations done so far.
	int _numEnergyEvaluations;
	
private:

	static double gsl_f(const gsl_vector* x, void* params) {
		return reinterpret_cast<GSLMinimizer*>(params)->eval_fdf(x, NULL);
	}
	static void gsl_df(const gsl_vector* x, void* params, gsl_vector* g) {
		reinterpret_cast<GSLMinimizer*>(params)->eval_fdf(x, g);
	}
	static void gsl_fdf(const gsl_vector* x, void* params, double* f, gsl_vector* g) {
		*f = reinterpret_cast<GSLMinimizer*>(params)->eval_fdf(x, g);
	}
	/// This calculates the total energy and its gradient for a configuration x.
	double eval_fdf(const gsl_vector* x, gsl_vector* g);
};

/**
 * \brief Wraps the Fletcher-Reeves conjugate gradient algorithm from the GSL library.
 * \author Alexander Stukowski
 */
class CGMinimizer : public GSLMinimizer
{
public:
	/// \brief Constructor that initializes the minimizer.
	/// \param calculator A calculator that should be used by the minimizer to 
	///                   evaluate the energy function and its gradient.
	/// \param convergenceNorm The norm of the gradient vector that should be reached.
	/// \param initialStepSize The size of the first trial step.
	/// \param tolerance The accuracy of the line minimization.
	CGMinimizer(const shared_ptr<Calculator>& calculator, FloatType convergenceNorm, FloatType initialStepSize = 1e-5, FloatType tolerance = 1e-9) :
		GSLMinimizer(calculator, convergenceNorm, gsl_multimin_fdfminimizer_conjugate_fr, initialStepSize, tolerance) {}
};

/**
 * \brief Wraps the steepest descent algorithm from the GSL library.
 * \author Alexander Stukowski
 */
class SteepestDescentMinimizer : public GSLMinimizer
{
public:
	/// \brief Constructor that initializes the minimizer.
	/// \param calculator A calculator that should be used by the minimizer to 
	///                   evaluate the energy function and its gradient.
	/// \param convergenceNorm The norm of the gradient vector that should be reached.
	/// \param initialStepSize The size of the first trial step.
	/// \param tolerance The accuracy of the line minimization.
	SteepestDescentMinimizer(const shared_ptr<Calculator>& calculator, FloatType convergenceNorm, FloatType initialStepSize = 1e-4, FloatType tolerance = 1e-1) :
		GSLMinimizer(calculator, convergenceNorm, gsl_multimin_fdfminimizer_steepest_descent, initialStepSize, tolerance) {}
};

/**
 * \brief Wraps the Broyden-Fletcher-Goldfarb-Shanno (BFGS) algorithm from the GSL library.
 * \author Alexander Stukowski
 */
class BFGSMinimizer : public GSLMinimizer
{
public:
	/// \brief Constructor that initializes the minimizer.
	/// \param calculator A calculator that should be used by the minimizer to 
	///                   evaluate the energy function and its gradient.
	/// \param convergenceNorm The norm of the gradient vector that should be reached.
	/// \param initialStepSize The size of the first trial step.
	/// \param sigma Parameter sigma used by Fletcher. A value of 0.1 is a good choice.
	BFGSMinimizer(const shared_ptr<Calculator>& calculator, FloatType convergenceNorm, FloatType initialStepSize = 1e-5, FloatType sigma = 0.1) :
		GSLMinimizer(calculator, convergenceNorm, gsl_multimin_fdfminimizer_vector_bfgs2, initialStepSize, sigma) {}
};

}; // End of namespace MAFEM

#endif // __MAFEM_GSL_MINIMIZER_H
