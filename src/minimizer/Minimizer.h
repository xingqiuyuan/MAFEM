///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_MINIMIZER_H
#define __MAFEM_MINIMIZER_H

#include <MAFEM.h>
#include <force/Calculator.h>

namespace MAFEM {

/**
 * \brief Base class for minimizers that find the minimum of a multi-dimensional function.
 * 
 * \author Alexander Stukowski
 */
class Minimizer : boost::noncopyable
{
protected:
	
	/// \brief Constructor that initializes the minimizer.
	/// \param calculator A calculator that should be used by the minimizer to 
	///                   evaluate the energy function and its gradient.
	Minimizer(const shared_ptr<Calculator>& calculator) : _calculator(calculator) {}

public:

	/// The virtual destructor.
	virtual ~Minimizer() {}

	/// Returns the calculator used by the minimizer to calculate the energy and the forces.
	const shared_ptr<Calculator>& calculator() const { return _calculator; }

	/// Performs one iteration step.
	virtual void iterate() = 0;
	
	/// Tests whether the desired accuracy has been reached.
	virtual bool isConverged() = 0;
	
	/// Returns the current norm of the force vector.
	virtual FloatType forceNorm() = 0;
	
	/// Returns the number of gradient evaluations done so far.
	virtual int numberOfForceEvaluations() = 0;

	/// Returns the number of energy evaluations done so far.
	virtual int numberOfEnergyEvaluations() = 0;
	
	/// Returns a string that describes the current status of the minimizer.
	virtual QString statusString() { return QString(); }
	
private:

	/// The calculator for the energy and the forces.
	shared_ptr<Calculator> _calculator;
};

}; // End of namespace MAFEM

#endif // __MAFEM_MINIMIZER_H
