///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "GSLMinimizer.h"

namespace MAFEM {

/******************************************************************************
* Constructor that initializes the minimizer.
******************************************************************************/
GSLMinimizer::GSLMinimizer(const shared_ptr<Calculator>& calculator, FloatType convergenceNorm, 
	const gsl_multimin_fdfminimizer_type* type, FloatType initialStepSize, FloatType tolerance) : Minimizer(calculator),
	_gsl_minimizer(NULL), _x(NULL), _convergenceNorm(convergenceNorm),
	_numEnergyEvaluations(0), _numForceEvaluations(0)
{
	MAFEM_ASSERT(_convergenceNorm >= 0);
		
	// The function dimensionality is the number of repatoms times the number of degrees of freedom.
	size_t n = NDOF * calculator->mesh().repatoms().size();

	// Create the internal GSL minimizer.
	_gsl_minimizer = gsl_multimin_fdfminimizer_alloc(type, n);
	if(!_gsl_minimizer) throw OutOfMemoryException();
	
	MsgLogger() << logdate << "Initializing GSL minimizer (type:" << gsl_multimin_fdfminimizer_name(_gsl_minimizer) << ", convergenceNorm:" << 
		convergenceNorm << ", initialStepSize:" << initialStepSize << ", tolerance:" << tolerance << ")" << endl; 
	
	// Allocate vectors.
	_x = gsl_vector_alloc(n);
	if(!_x) throw OutOfMemoryException();
	
	// Copy current repatom positions to GSL vector.
	double* ptr = gsl_vector_ptr(_x, 0);
	Q_FOREACH(const RepAtom* repatom, calculator->mesh().repatoms()) {
		MAFEM_ASSERT(repatom->index() * 3 == ptr - gsl_vector_ptr(_x, 0));
		*ptr++ = repatom->deformedPos().X;
		*ptr++ = repatom->deformedPos().Y;
		*ptr++ = repatom->deformedPos().Z;
	}
	
	// Initialize the function structure.
	_fdf.f = &GSLMinimizer::gsl_f;
	_fdf.df = &GSLMinimizer::gsl_df;
	_fdf.fdf = &GSLMinimizer::gsl_fdf;
	_fdf.n = n;
	_fdf.params = this;
		
	// Initialize minimizer with the initial guess.
	int status = gsl_multimin_fdfminimizer_set(_gsl_minimizer, &_fdf, _x, initialStepSize, tolerance);
	if(status)
		throw Exception(QString("The minimizer could not be initialized: %1").arg(gsl_strerror(status)));
}

/******************************************************************************
* Destructor.
******************************************************************************/
GSLMinimizer::~GSLMinimizer()
{
	// Release minimizer and memory.
	if(_gsl_minimizer) gsl_multimin_fdfminimizer_free(_gsl_minimizer);
	if(_x) gsl_vector_free(_x);
}

/******************************************************************************
* This calculates the total energy and its gradient for a configuration x.
******************************************************************************/
double GSLMinimizer::eval_fdf(const gsl_vector* x, gsl_vector* g)
{
	// The function dimensionality is the number of repatoms times the number of degrees of freedom.
	size_t n = NDOF * calculator()->mesh().repatoms().size();
	MAFEM_ASSERT_MSG(x->size == n, "GSLMinimizer", "The number of repatoms has changed during minimization.");
	
	// Write GSL vector back to repatom positions.
	const double* ptr = gsl_vector_const_ptr(x, 0);
	Q_FOREACH(RepAtom* repatom, calculator()->mesh().repatoms()) {
		MAFEM_ASSERT(repatom->index() * 3 == ptr - gsl_vector_const_ptr(x, 0));
		repatom->setDeformedPosComponent(0, *ptr++);
		repatom->setDeformedPosComponent(1, *ptr++);
		repatom->setDeformedPosComponent(2, *ptr++);
	}
	
	// Let the sampling atoms follow the repatoms.
	calculator()->mesh().deformationUpdate();
	
	if(g) {
		// Calculate total energy and forces.
		calculator()->calculateEnergyAndForces(_output);
		_numForceEvaluations++;
	
		// Copy nodal forces to GSL gradient vector.
		MAFEM_ASSERT(_output.forces().size() == n/NDOF);
		double* ptr = gsl_vector_ptr(g, 0);
		Q_FOREACH(const Vector3& f, _output.forces()) {
			// Don't forget not negate the forces to get the gradient.
			*ptr++ = -f.X;
			*ptr++ = -f.Y;
			*ptr++ = -f.Z;
		}
	}
	else {
		// Calculate only the total energy.
		calculator()->calculateEnergy(_output);
		_numEnergyEvaluations++;
	}

	return _output.totalEnergy();
}

/******************************************************************************
* Performs one iteration step.
******************************************************************************/
void GSLMinimizer::iterate()
{
	int status = gsl_multimin_fdfminimizer_iterate(_gsl_minimizer);
	if(status)
		throw Exception(QString("The minimizer stopped with an error: %1").arg(gsl_strerror(status)));
}

/******************************************************************************
* Tests whether the desired accuracy has been reached.
******************************************************************************/
bool GSLMinimizer::isConverged()
{
	int status = gsl_multimin_test_gradient(_gsl_minimizer->gradient, _convergenceNorm);
	return (status == GSL_SUCCESS);
}

/******************************************************************************
* Returns the value of the energy function and the current point.
******************************************************************************/
FloatType GSLMinimizer::minimumValue()
{
	return gsl_multimin_fdfminimizer_minimum(_gsl_minimizer);
}

/******************************************************************************
* Returns the current norm of the force vector.
******************************************************************************/
FloatType GSLMinimizer::forceNorm()
{
	FloatType sum = 0;
	const double* p = gsl_vector_const_ptr(gsl_multimin_fdfminimizer_gradient(_gsl_minimizer), 0);
	const double* pend = p + _fdf.n;
	while(p != pend) 
		sum += square(*p++);
	return sqrt(sum);
}
	
}; // End of namespace MAFEM

