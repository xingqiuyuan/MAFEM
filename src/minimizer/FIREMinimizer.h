///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_FIRE_MINIMIZER_H
#define __MAFEM_FIRE_MINIMIZER_H

#include <MAFEM.h>
#include "Minimizer.h"

namespace MAFEM {

/**
 * \brief Implements the FIRE algorithm.
 * 
 * \author Alexander Stukowski
 */
class FIREMinimizer : public Minimizer
{
public:
	
	/// \brief Constructor that initializes the minimizer.
	/// \param calculator A calculator that should be used by the minimizer to 
	///                   calculate the nodal forces.
	/// \param convergenceNorm The norm of the gradient vector that should be reached.
	/// \param initialStepSize The initial size of the Verlet step.
	/// \param maxStepSize The maximum size of the Verlet step.
	FIREMinimizer(const shared_ptr<Calculator>& calculator, FloatType convergenceNorm, 
			FloatType initialStepSize = 1e-5, FloatType maxStepSize = 1e-1);

	/// \brief Destructor.
	virtual ~FIREMinimizer();

	/// Performs one iteration step.
	virtual void iterate();

	/// Performs all iteration steps to reach convergence norm.
	virtual int minimize();
	
	/// Tests whether the desired accuracy has been reached.
	virtual bool isConverged();
	
	/// Returns the current norm of the force vector.
	virtual FloatType forceNorm();
	
	/// Returns the value of the energy function and the current point.
	FloatType minimumValue();

	/// Returns the number of gradient evaluations done so far.
	virtual int numberOfForceEvaluations() { return _numForceEvaluations; }

	/// Returns the number of energy evaluations done so far.
	virtual int numberOfEnergyEvaluations() { return 0; }
	
	/// Returns a string that describes the current status of the minimizer.
	virtual QString statusString() { return _statusString; }
	
	/// Allows direct access to the minimizer's internal energy/force calculation results.
	const CalculationResult& calculationOutput() const { return _output; }
	
private:
	
	/// The size of the Velocity Verlet step.
	FloatType _stepSize;

	/// The maximum size of the Velocity Verlet step.
	FloatType _maxStepSize;
	
	/// This stores the current repatom positions.
	ublas::vector<FloatType> _x;

	/// This stores the current repatom velocities.
	ublas::vector<FloatType> _v;

	/// This stores the current gradient.
	ublas::vector<FloatType> _gradient;
	
	/// The output container for the energy/force calculation.
	CalculationResult _output;
	
	/// The norm of the gradient vector that should be reached.
	FloatType _convergenceNorm;
	
	/// Counts the number of gradient evaluations done so far.
	int _numForceEvaluations;
	
	/// The 2-norm of the current gradient.
	FloatType _gradientNorm2;
	
	/// The current alpha value used by the FIRE algorithm.
	FloatType _alpha;

	/// The initial alpha value used by the FIRE algorithm.
	FloatType _alpha0;
	
	/// This counts the number of FIRE steps.
	int _nsteps;	

	/// Describes the current status of the minimizer.
	QString _statusString;
	
	/// This calculates the gradient for the current configuration.
	void computeGradient();

/**//// last total energy
	FloatType _prevTotalEnergy;
};

}; // End of namespace MAFEM

#endif // __MAFEM_FIRE_MINIMIZER_H
